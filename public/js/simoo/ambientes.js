$(document).ready(function () {
    //abrir modal de delete - ambiente
    $('.btn-delete-ambiente').click(function () {
        let fullUrlAmbiente = urlAmbiente + $(this).attr('data-id-ambiente');
        $('.modal-action-ambiente').attr('action', fullUrlAmbiente);
    });

    //abrir modal de delete - sala
    $('.btn-delete-sala').click(function () {
        let fullUrlSala = urlSala + $(this).attr('data-id-sala');
        $('.modal-action-sala').attr('action', fullUrlSala);
    });
});