/* ---------------------- */
/* -- GLOBAL VARIABLES -- */
/* ---------------------- */

let teachers_enrolled = [];
let teacher_value = '';
let teacher_type = '';
let teacher_edit = [];

/* ---------------- */
/* -- ALL EVENTS -- */
/* ---------------- */

//pesquisar instrutores
$('.btn-search-teacher').click(function (event){
    teacher_value = $('.search-teacher').val();
    $('.msg-search-teachers, .all-search-teacher').show();
    get_users(courseid, teacher_value, url_user_search, 'teachers');
});

//inserir instrutor na turma
$('.btn-insert-teacher').click(function(event){
    $('.modal-teacher .modal-title').text('Inserir Instrutor');
    $('.btn-edit-enrol-teacher').hide();
    $('.btn-insert-enrol-teacher').show();

    $('.btn-insert-enrol-teacher').click(function (event) {
        //verifica se existem instrutores selecionados
        if (teachers_enrolled.length > 0) {
            let userid = teachers_enrolled;
            insert_teacher(courseid, userid, url_insert_teacher, teacher_type);
        }
    });
});

//editar instrutor na turma
$('.btn-edit-enrol-teacher').click(function(){
    edit_teachers(courseid, teachers_enrolled, teacher_edit, teacher_type, url_edit_teacher);
});

//fechar modal
$('.close').click(function () {
    $(this).closest('.modal-teacher').find('.all-search-teacher').hide();
    $(this).closest('.modal-teacher').find('.msg-search-teachers').hide();
});


/* ------------------- */
/* -- ALL FUNCTIONS -- */
/* ------------------- */

//montar os instrutores na busca
function show_search_teachers(data) {
    //variaveis
    let teachers = JSON.parse(data);
    let tbody = $('.for-teachers');
    let tr = '';
    let ul_pagination = $('<ul>').addClass('pagination pagination-sm');
    let active_pagination = 'no-active';
    let index = 0;

    //remove
    $('.for-teachers tr').remove();

    //retira o gif loading
    loading_hide();

    //verifica se o valor pesquisado, remete em instrutores
    if (teachers.data.length > 0) {

        $('.pagination-teacher ul').remove();

        //cria uma paginação
        for (let m = 1; m <= teachers.last_page; m++) {
            if (teachers.current_page == m) {
                active_pagination = 'active';
            }
            else {
                active_pagination = 'no-active';
            }

            ul_pagination.append(
                '<li class="' + active_pagination + '"><a href="#" page-teacher-id="' + m + '">' + m + '</a></li>'
            );
        }

        $('.pagination-teacher').append(ul_pagination);
        $('.pagination-teacher').show();

        $('.pagination-teacher li.active a').click(function (event) {
            event.preventDefault();
        });

        $('.pagination-teacher li.no-active a').click(function (event) {
            event.preventDefault();
            ul_pagination.remove();

            let pageid = $(this).attr('page-teacher-id');
        get_users(courseid, teacher_value, url_user_search, 'teachers', parseInt(pageid));
        });

        //cria um for com os usuarios buscados
        for (let i = 0; i < teachers.data.length; i++) {

            //cria a TR com a classe para os usuarios
            tr = $('<tr>').addClass('teacher');

            //cria a TR e adiciona no tbody
            tr.append(
                '<td><input type="radio" name="teacher" class="radio-insert" data-userid="' + teachers.data[i].id + '" /></td>' +
                '<td>' + teachers.data[i].firstname + " " + teachers.data[i].lastname + '</td>' +
                '<td>' + teachers.data[i].username + '</td>' +
                '<td>' + teachers.data[i].email + '</td>'
            );
            tbody.append(tr);
        }

        //inseri a mensagem
        $('.msg-search-teachers').html(teachers.total + ' usuário(s) encontrado(s)');

    }
    else {
        //remove a tabela
        $('.all-search-teacher').hide();
        //inseri a mensagem
        $('.msg-search-teachers').html('Nenhum usuário foi encontrado');
    }

    //verifica o radio
    $('.modal-teacher .radio-insert').change(function (event) {
        let userId = $(this).attr('data-userid');

        //se estiver checado
        if ($(this).is(':checked')) {
            teachers_enrolled = [parseInt(userId)];
        }
    });
}

//buscar os instrutores inseridos na turma
function get_teachers(courseid, page) {
    $.ajax({
        type: 'GET',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid },
        success: function (data) {

            //variaveis
            let users = [];
            let teachers = JSON.parse(data);
            let teacher_role = '';
            let tbody = $('.for-all-teachers');
            let tr = '';

            //preenche os usuarios na turma
            $.each(teachers, function (key, value){
                users.push(value.id);
            });
            add_enrolled(1, users);

            //quantidade de instrutores
            $('.qtt_teachers').html(teachers.length);

            //remove tr
            $('.for-all-teachers tr').remove();

            //verifica se existem instrutores
            if (teachers.length > 0) {

                for (let i = 0; i < teachers.length; i++) {

                    //verifica o tipo do instrutor
                    if (teachers[i].shortname == 'editingteacherone') {
                        teacher_role = 'TITULAR';
                    }
                    else if (teachers[i].shortname == 'editingteachertwo') {
                        teacher_role = 'SECUNDÁRIO';
                    }

                    tr = $('<tr>');
                    tr.append(
                        '<td>' + teachers[i].firstname + " " + teachers[i].lastname + '</td>' +
                        '<td>' + teacher_role + '</td>' +
                        '<td>' +
                        '<button href="#" class="btn btn-primary btn-sm btn-edit-teacher" data-toggle="modal" data-target=".modal-teacher" data-userid-old="' + teachers[i].id + '" data-shortname="' + teachers[i].shortname + '"><i class="fa fa-pencil"></i></button>' +
                        '<button href="#" class="btn btn-danger btn-sm btn-delete-teacher" data-userid="' + teachers[i].id + '"><i class="fa fa-close"></i></button>' +
                        '</td>'
                    );
                    tbody.append(tr);
                }

                //adiciona atributos ao tipo de instrutor a ser cadastrado
                if (teachers.length == 1) {
                    $('.type-teacher').attr('data-teacher-type', 'editingteachertwo');
                    $('.type-teacher').text('Instrutor Secundário');
                    teacher_type = 'editingteachertwo';
                }

                //botao editar instrutor
                $('.btn-edit-teacher').click(function (event) {
                    let userIdOld = $(this).attr('data-userid-old');

                    $('.modal-teacher .modal-title').text('Editar Instrutor');
                    $('.btn-edit-enrol-teacher').show();
                    $('.btn-insert-enrol-teacher').hide();

                    teacher_type = $(this).attr('data-shortname');

                    if (teacher_type == 'editingteacherone'){
                        $('.type-teacher').text('Instrutor Titular');
                    }
                    else if (teacher_type == 'editingteachertwo'){
                        $('.type-teacher').text('Instrutor Secundário');
                    }
                    //inserir id do instrutor no array
                    teacher_edit = [parseInt(userIdOld)];
                });

                //botao remover instrutor
                $('.btn-delete-teacher').click(function (event) {
                    let userId = $(this).attr('data-userid');
                    delete_teachers(courseid, userId, url_remove_teacher);
                });

                $('.table-all-teachers').show();
                $('.no-teachers').hide();
            }
            else {
                //adiciona atributos ao tipo de instrutor a ser cadastrado
                $('.type-teacher').attr('data-teacher-type', 'editingteacherone');
                $('.type-teacher').text('Instrutor Titular');
                teacher_type = 'editingteacherone';

                $('.no-teachers').show();
            }

            if (teachers.length == 2) {
                //fechar os elementos e modal

                $('.box-insert-teacher').hide();
                $('#instrutor .table').css('margin', '0');
                $('#instrutor .table-last-bottom tr').css('border-bottom', 'none');
            }
            else {
                $('.box-insert-teacher').show();
                $('#instrutor .table').css('margin-bottom', '20px');
                $('#instrutor .table-last-bottom tr').css('border-bottom', '1px solid #ddd');
            }
        }
    });
}
get_teachers(courseid, url_all_teachers);

//matricular o instrutor
function insert_teacher(courseid, userid, page, type) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid, userId: userid, typeName: type },
        success: function (data) {
            //remover todos os checked
            $('.modal-teacher .radio-insert').each(function () { this.checked = false; });

            //zerar o array depois de ter matriculado o instrutor
            teachers_enrolled = [];

            //fecha o modal
            if (type == 'editingteachertwo') {
                $('.modal-teacher').modal('hide');
            }

            //chama a nova lista de instrutores
            get_teachers(courseid, url_all_teachers);
        }
    });
}

//editar instrutores
function edit_teachers(courseid, teachers_enrolled, teacher_edit, teacher_type, page) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { 
            _token: CSRF_TOKEN, 
            courseId: courseid, 
            userIdOld: teacher_edit,
            userIdNew: teachers_enrolled,
            roleShortname: teacher_type,        
        },
        success: function (data) {
            
            //remover todos os checked
            $('.modal-teacher .radio-insert').each(function () { this.checked = false; });

            //zerar o array depois de ter matriculado o instrutor
            teachers_enrolled = [];

            //fecha o modal
            $('.modal-teacher').modal('hide');

            //chama a nova lista de instrutores
            get_teachers(courseid, url_all_teachers);
        }
    });
}

//remover instrutores
function delete_teachers(courseid, userid, page) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid, userId: userid },
        success: function (data) {
            get_teachers(courseid, url_all_teachers);
        }
    });
}    