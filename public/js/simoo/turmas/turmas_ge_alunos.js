/* ---------------------- */
/* -- GLOBAL VARIABLES -- */
/* ---------------------- */

let students_enrolled = [];
let student_value = '';


/* ---------------- */
/* -- ALL EVENTS -- */
/* ---------------- */

//pesquisar alunos
$('.btn-search-student').click(function (event) {
    student_value = $('.search-student').val();
    $('.msg-search-students, .all-search-student').show();
    get_users(courseid, student_value, url_user_search, 'students');
});

//inserir aluno na turma
$('.btn-enrol-student').click(function (event) {
    //verifica se existem alunos selecionados
    if (students_enrolled.length > 0) {
        let userid = students_enrolled;
        insert_student(courseid, userid, url_insert_student);
    }
});

//fechar modal
$('.close').click(function(){
    $(this).closest('.modal-student').find('.all-search-student').hide();
    $(this).closest('.modal-student').find('.msg-search-students').hide();
});

//frequencia e nota
$('.mode-course').click(function(){
    let obj = $(this);
    let mode = $(this).hasClass('mode-on');
    mode_students(obj, mode);
});

//Botão salvar alterações
$('.save-frequency-grade').click(function(){
    let manager = [ [ ], [ ], [ ]];

    $('.for-all-students tr').each(function(key){
        
        manager[0].push($(this).find('.checkbox-delete').attr('data-userid'));
        manager[1].push($(this).find('.grade-input').val());
        manager[2].push($(this).find('.frequency-input').val());
    });

    manage_grades(courseid, manager, url_manager_grades);
});


/* ------------------- */
/* -- ALL FUNCTIONS -- */
/* ------------------- */

//montar os alunos na busca
function show_search_students(data) {
    //variaveis
    let students = JSON.parse(data);
    let tbody = $('.for-students');
    let tr = '';
    let ul_pagination = $('<ul>').addClass('pagination pagination-sm');
    let active_pagination = 'no-active';
    let index = 0;

    //remove
    $('.for-students tr').remove();

    //retira o gif loading
    loading_hide();

    //verifica se o valor pesquisado, remete em alunos
    if (students.data.length > 0) {

        $('.pagination-student ul').remove();

        //cria uma paginação
        for (let m = 1; m <= students.last_page; m++) {
            if (students.current_page == m) {
                active_pagination = 'active';
            }
            else {
                active_pagination = 'no-active';
            }

            ul_pagination.append(
                '<li class="' + active_pagination + '"><a href="#" page-student-id="' + m + '">' + m + '</a></li>'
            );
        }

        $('.pagination-student').append(ul_pagination);
        $('.pagination-student').show();

        $('.pagination-student li.active a').click(function (event) {
            event.preventDefault();
        });

        $('.pagination-student li.no-active a').click(function (event) {
            event.preventDefault();
            ul_pagination.remove();

            let pageid = parseInt($(this).attr('page-student-id'));
            get_users(courseid, student_value, url_user_search, 'students', pageid);
        });

        //cria um for com os usuarios buscados
        for (let i = 0; i < students.data.length; i++) {

            //cria a TR com a classe student
            tr = $('<tr>').addClass('student');

            let checked = "";

            //identifica o index do students_enrolled
            index = students_enrolled.indexOf(students.data[i].id);

            //verifica se tem index
            if (index > -1) {
                checked = "checked";
            }

            //cria a TR e adiciona no tbody
            tr.append(
                '<td><input type="checkbox" ' + checked + ' class="checkbox-insert" data-userid="' + students.data[i].id + '" /></td>' +
                '<td>' + students.data[i].firstname + " " + students.data[i].lastname + '</td>' +
                '<td>' + students.data[i].username + '</td>' +
                '<td>' + students.data[i].email + '</td>'
            );
            tbody.append(tr);
        }

        //inseri a mensagem
        $('.msg-search-students').html(students.total + ' usuário(s) encontrado(s)');
    }
    else {
        //remove a tabela
        $('.all-search-student').hide();
        //inseri a mensagem
        $('.msg-search-students').html('Nenhum usuário foi encontrado');
    }

    //verifica o checkbox
    $('.modal-student .checkbox-insert').change(function (event) {
        let userid = parseInt($(this).attr('data-userid'));
        let index = 0;

        //se estiver checado
        if ($(this).is(':checked')) {
            if (students_enrolled.length == 0) {
                students_enrolled.push(userid);
            }
            else {
                index = students_enrolled.indexOf(userid);
                if (index == -1) {
                    students_enrolled.push(userid);
                }
            }

        } else {
            index = students_enrolled.indexOf(userid);
            if (index > -1) {
                students_enrolled.splice(index, 1);
            }
        }
    });
}

//alunos matriculados
function get_students(courseid, page) {
    $.ajax
        ({
            type: 'GET',
            dataType: 'html',
            url: page,
            data: { _token: CSRF_TOKEN, courseId: courseid },
            success: function (data) {
                
                //variaveis
                let users = [];
                let students = JSON.parse(data);
                let tbody = $('.for-all-students');
                let tr = '';
                
                //preenche os usuarios na turma
                $.each(students, function(key,value){
                    users.push(value.id);
                });
                add_enrolled(0, users);
                
                //quantidade de estudantes matriculados
                $('.qtt_students').html(students.length);

                //remove tr
                $('.for-all-students tr').remove();

                //remove o botão remover aluno
                $('.btn-delete-aluno').hide();

                //verifica se existem estudantes
                if (students.length > 0) {
                    
                    console.log(students)

                    let type = '';

                    //varre os estudantes
                    for (let i = 0; i < students.length; i++) {
                        tr = $('<tr>');
                        
                        //verifica o tipo de servidor
                        if ("customfields" in students[i]){

                            for (let m = 0; m < students[i].customfields.length; m++){
                                
                                if ("shortname" in students[i].customfields[m]){
                                    if (students[i].customfields[m].shortname == 'type') {
                                        for (let z = 0; z < invited.length; z++) {
                                            if (students[i].customfields[m].value == invited[z])
                                                type = 'Convidado';
                                            else {
                                                type = students[i].customfields[m].value;
                                            }
                                        }
                                    }
                                    else{
                                        type = 'INDEFINIDO';
                                    }
                                }
                                else{
                                    type = 'INDEFINIDO';
                                }
                                
                            }

                        }
                        else{
                            type = 'INDEFINIDO';
                        }

                        // inseri as informações do aluno na TR
                        tr.append(
                            '<td><input type="checkbox" class="checkbox-delete" data-userid="' + students[i].id + '" name="student_delete[]"/></td>' +
                            '<td>' + students[i].firstname + " " + students[i].lastname + '</td>' +
                            '<td>' + maskCpf(students[i].username) + '</td>' +
                            '<td>' + students[i].email + '</td>' +
                            '<td><span class="span-mode">' + ((students[i].frequency) ? students[i].frequency + 'h - (' + Math.round((((workload - (workload - students[i].frequency))/workload)*100)) + '%)' : '-') + '</span><input type="text" maxlength="2" class="form-control frequency-input mode-on-input" data-userid="' + students[i].id + '" value="' + students[i].frequency + '" /></td>' +
                            '<td><span class="span-mode">' + ((students[i].grade) ? students[i].grade : '-') + '</span><input type="text" maxlength="2" class="form-control grade-input mode-on-input" data-userid="' + students[i].id + '" value="' + students[i].grade + '"/></td>' +
                            '<td>' + type  + '</td>' +
                            '<td>' + situation(students[i].frequency, students[i].grade) + '</td>'
                        );
                        tbody.append(tr);
                    }
                    $('.table-all-students').show();
                    $('.msg-students-enrolled').hide();
                    $('.no-students-enrolled').hide();
                    $('.mode-course').show();
                }
                else {
                    $('.no-students-enrolled').show();
                    $('.msg-students-enrolled').hide();
                    $('.mode-course').hide();
                }

                //verifica o checkbox
                $('.checkbox-delete').change(function (event) {
                    $('.btn-delete-aluno').hide();

                    $('.checkbox-delete:checked').each(function (index) {
                        $('.btn-delete-aluno').show();
                    });
                });

                //botao remover aluno
                $('.btn-delete-aluno').click(function (event) {
                    $('.checkbox-delete:checked').each(function (items) {
                        let userid = $(this).attr('data-userid');
                        delete_students(courseid, userid, url_remove_student);
                    });
                });
            }
        });
}
get_students(courseid, url_all_students);

//matricular o aluno
function insert_student(courseid, userid, page) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid, userId: userid },
        success: function (data) {
            //remover todos os checked
            $('.modal-student .checkbox-insert').each(function () { this.checked = false; });

            //zerar o array depois de ter matriculado o aluno
            students_enrolled = [];
            
            //atualizar os estudantes matriculados
            get_students(courseid, url_all_students);
        }
    });
}

//remover estudantes
function delete_students(courseid, userid, page) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid, userId: userid },
        success: function (data) {
            get_students(courseid, url_all_students);
        }
    });
}

//gerenciar notas
function manage_grades(courseid, manager, page){
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid, manager: manager },
        beforeSend: function(){
            $('.loading-grade').show();
        },
        success: function (data) {
            //remove o loading
            $('.loading-grade').hide();

            //atualizar os estudantes matriculados
            get_students(courseid, url_all_students);

            //atualizar o mode
            mode_students($('.mode-course'), $('.mode-course').hasClass('mode-on'));
        }
    });
}

//frequencia e notas
function mode_students(obj, mode){
    if (mode) {
        //habilita o mode-off
        obj.removeClass('mode-on');
        obj.addClass('mode-off');

        //desabilita os inputs
        $('.mode-on-input').hide();

        //desabilita o span de notas
        $('.span-mode').show();

        //desabilita o save-frequency-grade
        $('.save-frequency-grade').hide();
    }
    else {
        //habilita o mode-on
        obj.removeClass('mode-off');
        obj.addClass('mode-on');

        //habilita os inputs
        $('.mode-on-input').show();

        //desabilita o span de notas
        $('.span-mode').hide();

        //habilita o save-frequency-grade
        $('.save-frequency-grade').show();
    }
}

//Situação do aluno
function situation(frequency, grade){
    //Tranforma em porcento
    let fcy = Math.round((((workload - (workload - frequency)) / workload) * 100));

    //Verifica a situação do aluno
    if (fcy == 0 && grade == '') {
        return 'EM ANDAMENTO';
    }
    else if (fcy < defaultfrequency || grade < defaultgrade){
        return 'REPROVADO';
    }
    else{
        return 'APROVADO';
    }
}

