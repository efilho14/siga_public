$(document).ready(function () {
    //abrir modal de delete
    $('.btn-delete').click(function () {
        let fullUrl = url + $(this).attr('data-id');
        let nomeTurma = $(this).closest('tr').find('.event-fullname').text();
        let numeroTurma = $(this).closest('tr').find('.event-shortname').text();

        $('.modal-action').attr('action', fullUrl);
        $('.modal-fullname').text(nomeTurma);
        $('.modal-shortname').text(numeroTurma);
    });
});