jQuery(document).ready(function () {
    //select2
    $("#location").select2({
        placeholder: "Selecione uma opção"
    });

    //nota e frequencia
    $('.frequency-group').hide();
    $('.fe-default').change(function () {
        if ($(this).val() == 0) {
            $('.frequency-group').show();
        } else if ($(this).val() == 1) {
            $('.frequency-group').hide();
        }
    });

    //daterangepicker
    function datapicker(){

        $('.hourinit').inputmask("99:99");
        $('.hourend').inputmask("99:99");

        $(".dates").each(function (key, value){

            $('.dates').daterangepicker({
                locale: {
                    format: "DD/MM/YYYY",
                    daysOfWeek: ["D", "S", "T", "Q", "Q", "S", "S"],
                    monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                    applyLabel: "Aplicar",
                    cancelLabel: "Cancelar",
                },
            });

            $(this).addClass('dt-' + key);
        });
        reorder();
    } datapicker();

    //adicionar novo período
    $('.new-date').click(function () {
        let datesDefaultFirst = $('.dates-default:first')
        
        $('.dates-group').append('<div class="form-group dates-default">' + datesDefaultFirst.html() + '</div>');

        if($('.dates-default').length > 1){
            $('.dates-default:last').find('.btn-delete').removeAttr('disabled');
            $('.dates-default:last').find('.hourinit').val('');
            $('.dates-default:last').find('.hourend').val('');
        }
        datapicker();
    });
    
    //remover período
    jQuery(document.body).on('click', '.btn-delete', function (){
        $(this).closest('.dates-default').remove();
        reorder();
    });

    //reordenar período
    function reorder(){

        //remover a class
        for (let m = 0; m <= ($(".dates").length + 1); m++){
            $(".dates").removeClass('dt-' + m);
        }
        
        //adicionar a class
        $('.dates').each(function(key, value){
            $(this).addClass('dt-' + key);
            $(this).closest('.dates-default').find('.block').val(key);
            //$(this).attr('data-block', key);
        });
    }

    //tiny
    tinymce.init({
        menubar: false,
        selector: 'textarea',
        plugins: ["advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern"
        ]
    });
});