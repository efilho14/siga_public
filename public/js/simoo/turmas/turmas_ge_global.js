/* ---------------------- */
/* -- GLOBAL VARIABLES -- */
/* ---------------------- */

let enrolled_all = [ ['student',[]],['teacher',[]],['coordinator',[]] ];

/* ---------------------- */
/* -- GLOBAL FUNCTIONS -- */
/* ---------------------- */

//get users for search
function get_users(course, name, page, type, pagination=null) {
    let result_page = page;

    if(pagination){
        result_page = page + '?page=' + pagination;
    }

    $.ajax
    ({
        type: 'GET',
        dataType: 'html',
        url: result_page,
        data: { _token: CSRF_TOKEN, courseId: course, nameUser: name, users: enrolled_all },
        success: function(data){

            if(type=='students'){
                return show_search_students(data);
            }
            else if(type=='teachers'){
                return show_search_teachers(data);
            }
            else if(type=='coordinators'){
                return show_search_coordinators(data);
            }
        }
    });
}

function add_enrolled(index,userid){
    if (userid.length == 0){
        enrolled_all[index][1] = [0];
    }
    else{
        enrolled_all[index][1] = userid;
    }
}

function loading_show(){
    $('.loading').fadeIn('fast');
}

function loading_hide(){
    $('.loading').fadeOut('fast');
} 

//Mascara para o CPF 
function maskCpf(value) {
    return value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");
}