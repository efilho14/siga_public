/* ---------------------- */
/* -- GLOBAL VARIABLES -- */
/* ---------------------- */

let coordinators_enrolled = [];
let coordinator_value = '';
let coordinator_edit = [];

/* ---------------- */
/* -- ALL EVENTS -- */
/* ---------------- */

//pesquisar coordenadores
$('.btn-search-coordinator').click(function (event) {
    coordinator_value = $('.search-coordinator').val();
    $('.msg-search-coordinators, .all-search-coordinator').show();
    get_users(courseid, coordinator_value, url_user_search, 'coordinators');
});

//inserir coordenador na turma
$('.btn-insert-coordinator').click(function (event) {
    $('.modal-coordinator .modal-title').text('Inserir Coordenador');
    $('.btn-edit-enrol-coordinator').hide();

    $('.btn-insert-enrol-coordinator').click(function (event) {
        //verifica se existem coordenadores selecionados
        if (coordinators_enrolled.length > 0) {
            let userid = coordinators_enrolled;
            insert_coordinator(courseid, userid, url_insert_coordinator);
        }
    });
});

//editar instrutor na turma
$('.btn-edit-enrol-coordinator').click(function () {
    edit_coordinators(courseid, coordinators_enrolled, coordinator_edit, url_edit_coordinator);
});

//fechar modal
$('.close').click(function () {
    $(this).closest('.modal-coordinator').find('.all-search-coordinator').hide();
    $(this).closest('.modal-coordinator').find('.msg-search-coordinators').hide();
});

/* ------------------- */
/* -- ALL FUNCTIONS -- */
/* ------------------- */

//montar os coordenadores na busca
function show_search_coordinators(data) {
    //variaveis
    let coordinators = JSON.parse(data);
    let tbody = $('.for-coordinators');
    let tr = '';
    let ul_pagination = $('<ul>').addClass('pagination pagination-sm');
    let active_pagination = 'no-active';
    let index = 0;

    //remove
    $('.for-coordinators tr').remove();

    //retira o gif loading
    loading_hide();

    //verifica se o valor pesquisado, remete em coordenadores
    if (coordinators.data.length > 0) {

        $('.pagination-coordinators ul').remove();

        //cria uma paginação
        for (let m = 1; m <= coordinators.last_page; m++) {
            if (coordinators.current_page == m) {
                active_pagination = 'active';
            }
            else {
                active_pagination = 'no-active';
            }

            ul_pagination.append(
                '<li class="' + active_pagination + '"><a href="#" page-coordinators-id="' + m + '">' + m + '</a></li>'
            );
        }

        $('.pagination-coordinator').append(ul_pagination);
        $('.pagination-coordinator').show();

        $('.pagination-coordinator li.active a').click(function (event) {
            event.preventDefault();
        });
        
        $('.pagination-coordinator li.no-active a').click(function (event) {
            event.preventDefault();
            ul_pagination.remove();

            let pageid = $(this).attr('page-coordinators-id');
            get_users(courseid, coordinator_value, url_user_search, 'coordinators', parseInt(pageid));
        });


        //cria um for com os usuarios buscados
        for (let i = 0; i < coordinators.data.length; i++) {

            //cria a TR com a classe para os usuarios
            tr = $('<tr>').addClass('coordinator');

            //cria a TR e adiciona no tbody
            tr.append(
                '<td><input type="radio" name="coordinator" class="radio-insert" data-userid="' + coordinators.data[i].id + '" /></td>' +
                '<td>' + coordinators.data[i].firstname + " " + coordinators.data[i].lastname + '</td>' +
                '<td>' + coordinators.data[i].username + '</td>' +
                '<td>' + coordinators.data[i].email + '</td>'
            );
            tbody.append(tr);
        }

        //inseri a mensagem
        $('.msg-search-coordinators').html(coordinators.total + ' usuário(s) encontrado(s)');
    }
    else{
        //remove a tabela
        $('.all-search-coordinator').hide();

        //inseri a mensagem
        $('.msg-search-coordinators').html('Nenhum usuário foi encontrado');
    }

    //verifica o radio
    $('.modal-coordinator .radio-insert').change(function (event) {
        let userId = $(this).attr('data-userid');

        //se estiver checado
        if ($(this).is(':checked')) {
            coordinators_enrolled = [parseInt(userId)];
        }
    });
}

//buscar os coordenadores inseridos na turma
function get_coordinators(courseid, page) {
    $.ajax({
        type: 'GET',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid },
        success: function (data){

            //variaveis
            let users = [];
            let coordinators = JSON.parse(data);
            let coordinator_role = '';
            let tbody = $('.for-all-coordinators');
            let tr = '';

            //preenche os usuarios na turma
            $.each(coordinators, function (key, value) {
                users.push(value.id);
            });
            add_enrolled(2, users);

            //quantidade de coordenadores
            $('.qtt_coordinators').html(coordinators.length);

            //remove tr
            $('.for-all-coordinators tr').remove();

            //verifica se existem coordenadores
            if(coordinators.length > 0){

                for (let i = 0; i < coordinators.length; i++) {

                    tr = $('<tr>');
                    tr.append(
                        '<td>' + coordinators[i].firstname + " " + coordinators[i].lastname + '</td>' +
                        '<td>' +
                        '<button href="#" class="btn btn-primary btn-sm btn-edit-coordinator" data-toggle="modal" data-target=".modal-coordinator" data-userid-old="' + coordinators[i].id + '" data-shortname="' + coordinators[i].shortname + '"><i class="fa fa-pencil"></i></button>' +
                        '<button href="#" class="btn btn-danger btn-sm btn-delete-coordinator" data-userid="' + coordinators[i].id + '"><i class="fa fa-close"></i></button>' +
                        '</td>'
                    );
                    tbody.append(tr);
                }

                //botao editar coordenador
                $('.btn-edit-coordinator').click(function (event) {
                    let userIdOld = $(this).attr('data-userid-old');

                    $('.modal-coordinator .modal-title').text('Editar Coordenador');
                    $('.btn-edit-enrol-coordinator').show();
                    $('.btn-insert-enrol-coordinator').hide();

                    //inserir id do coordenador no array
                    coordinator_edit = [parseInt(userIdOld)];
                });

                //botao remover coordenador
                $('.btn-delete-coordinator').click(function (event) {
                    let userId = $(this).attr('data-userid');
                    delete_coordinators(courseid, userId, url_remove_coordinator);
                });

                $('.table-all-coordinators').show();
                $('.no-coordinators').hide();
                $('#coordenador .top_search').hide();
            }
            else {
                $('.table-all-coordinators').hide();
                $('.no-coordinators').show();
                $('#coordenador .top_search').show();
            }
        }   
    });
}
get_coordinators(courseid, url_all_coordinators);

//matricular o coordenador
function insert_coordinator(courseid, userid, page) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid, userId: userid },
        success: function (data) {
            //remover todos os checked
            $('.modal-coordinator .radio-insert').each(function () { this.checked = false; });

            //zerar o array depois de ter matriculado o coordenador
            coordinators_enrolled = [];

            //fecha o modal
            $('.modal-coordinator').modal('hide');

            //chama a nova lista de coordenadores
            get_coordinators(courseid, url_all_coordinators);
        }
    });
}

//editar coordenadores
function edit_coordinators(courseid, coordinators_enrolled, coordinator_edit, page) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: {
            _token: CSRF_TOKEN,
            courseId: courseid,
            userIdOld: coordinator_edit,
            userIdNew: coordinators_enrolled
        },
        success: function (data){
            //remover todos os checked
            $('.modal-coordinator .radio-insert').each(function () { this.checked = false; });

            //zerar o array depois de ter matriculado o coordenador
            coordinators_enrolled = [];

            //fecha o modal
            $('.modal-coordinator').modal('hide');

            //chama a nova lista de coordenadores
            get_coordinators(courseid, url_all_coordinators);
        }
    });
}

//remover coordenador
function delete_coordinators(courseid, userid, page) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: page,
        data: { _token: CSRF_TOKEN, courseId: courseid, userId: userid },
        success: function (data) {
            get_coordinators(courseid, url_all_coordinators);
        }
    });
}    