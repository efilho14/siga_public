$(document).ready(function () {
    //abrir modal de delete - public
    $('.btn-delete-public').click(function () {
        let fullUrl = urlPublic + $(this).attr('data-id');
        let nome = $(this).closest('tr').find('.public-name').text();

        $('.modal-action').attr('action', fullUrl);
        $('.modal-event').text(nome);
    });

    //abrir modal de delete - group
    $('.btn-delete-group').click(function () {
        let fullUrl = urlGroup + $(this).attr('data-id');
        let nome = $(this).closest('tr').find('.group-name').text();

        $('.modal-action').attr('action', fullUrl);
        $('.modal-event').text(nome);
    });

    //abrir modal de delete - type
    $('.btn-delete-type').click(function () {
        let fullUrl = urlType + $(this).attr('data-id');
        let nome = $(this).closest('tr').find('.type-name').text();

        $('.modal-action').attr('action', fullUrl);
        $('.modal-event').text(nome);
    });
});