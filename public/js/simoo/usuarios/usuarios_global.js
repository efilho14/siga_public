/* ---------------
----- EVENTS -----
--------------- */

//abrir modal de delete
$('.btn-delete').click(function (){
    let fullUrl = url + $(this).attr('data-id');
    let nomeUsuario = $(this).closest('tr').find('.user-name').text();
    $('.modal-action').attr('action', fullUrl);
    $('.modal-event').text(nomeUsuario);
});

//Criar e editar usuário - opção de selecionar o publico, grupo e tipo
$('#public').change(function(){
    ajaxPublic(this.value);
});

//Criar e editar usuário - opção de selecionar o publico, grupo e tipo
$('#group').change(function () {
    ajaxGroup(this.value);
});

/* --------------
--- FUNCTIONS ---
--------------- */

// Buscar os grupos pelo público pesquisado
function ajaxPublic(name, edit=false)
{
    let publicName = name;

    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: url_user_public,
        data: {
            _token: CSRF_TOKEN,
            publicName: publicName
        },
        success: function (data) {
            var result = JSON.parse(data);

            $('.loading_group').text('Selecione uma opção:');
            $('#group option.el').remove();

            $('.loading_type').text('Aguardando o grupo...');
            $('#type option.el').remove();
            $.each(result, function (key, value) {
                if(edit == false){
                    $('#group').append($("<option></option>").attr("class", 'el').attr("value", value.name).text(value.name));
                }
                else{
                    if (value.name == group){
                        $('#group').append($("<option></option>").attr("class", 'el').attr("value", value.name).attr("selected","selected").text(value.name));
                    }
                    else{
                        $('#group').append($("<option></option>").attr("class", 'el').attr("value", value.name).text(value.name));
                    }
                    
                }
            });
        }
    });
}

//Buscar os tipos pelos grupos pesquisados
function ajaxGroup(name, edit=false)
{
    let groupName = name;
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: url_user_group,
        data: {
            _token: CSRF_TOKEN,
            groupName: groupName
        },
        success: function (data) {
            //console.log(data);
            var result = JSON.parse(data);

            $('.loading_type').text('Selecione uma opção:');
            $('#type option.el').remove();
            $.each(result, function (key, value) {
                if (edit == false) {
                    $('#type').append($("<option></option>").attr("class", 'el').attr("value", value.name).text(value.name));
                }
                else {
                    if (value.name == type) {
                        $('#type').append($("<option></option>").attr("class", 'el').attr("value", value.name).attr("selected", "selected").text(value.name));
                    }
                    else {
                        $('#type').append($("<option></option>").attr("class", 'el').attr("value", value.name).text(value.name));
                    }

                }
            });
        }
    });
}
