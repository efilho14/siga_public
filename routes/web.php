<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|---------------------------------------------
| SIGA - SISTEMA DE GERENCIAMENTO ACADÊMICO
|---------------------------------------------
 */

## Início
Route::get('/', function () {
    return redirect()->route('login');
});

## Autenticação
Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

## Registro
Route::get('auth/register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('auth/register', ['as' => 'register', 'uses' => 'Auth\RegisterController@register']);

## Resetar Senha
Route::get('password/change', ['as' => 'change_password', 'uses' => 'Auth\ChangePasswordController@showChangePasswordForm']);
Route::post('password/change', ['as' => 'change_password', 'uses' => 'Auth\ChangePasswordController@changePassword']);
Route::get('password/reset', ['as' => 'password.request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

## Módulos
Route::get('modules', ['as' => 'modules.index', 'uses' => 'Siga\ModulesController@index']);
Route::get('modules/create',['as' => 'modules.create', 'uses' => 'Siga\ModulesController@create']);
Route::post('modules', ['as' => 'modules.store', 'uses' => 'Siga\ModulesController@store']);
Route::delete('modules/{id}', ['as' => 'modules.destroy', 'uses' => 'Siga\ModulesController@destroy'])->where('id', '[0-9]+');
Route::get('modules/{id}/edit', ['as' => 'modules.edit', 'uses' => 'Siga\ModulesController@edit'])->where('id', '[0-9]+');
Route::put('modules/{id}', ['as' => 'modules.update', 'uses' => 'Siga\ModulesController@update'])->where('id', '[0-9]+');

## Tipos de Usuário
Route::get('roles', ['as' => 'roles.index', 'uses' => 'Siga\RolesController@index']);
Route::get('roles/create', ['as' => 'roles.create', 'uses' => 'Siga\RolesController@create']);
Route::post('roles', ['as' => 'roles.store', 'uses' => 'Siga\RolesController@store']);
Route::delete('roles/{id}', ['as' => 'roles.destroy', 'uses' => 'Siga\RolesController@destroy'])->where('id', '[0-9]+');
Route::get('roles/{id}/edit', ['as' => 'roles.edit', 'uses' => 'Siga\RolesController@edit'])->where('id', '[0-9]+');
Route::put('roles/{id}', ['as' => 'roles.update', 'uses' => 'Siga\RolesController@update'])->where('id', '[0-9]+');

## Usuários do Sistema
Route::get('users', ['as' => 'users.index', 'uses' => 'Siga\UsersController@index']);
Route::get('users/create', ['as' => 'users.create', 'uses' => 'Siga\UsersController@create']);
Route::post('users', ['as' => 'users.store', 'uses' => 'Siga\UsersController@store']);
Route::delete('users/{id}', ['as' => 'users.destroy', 'uses' => 'Siga\UsersController@destroy'])->where('id', '[0-9]+');
Route::get('users/{id}/edit', ['as' => 'users.edit', 'uses' => 'Siga\UsersController@edit'])->where('id', '[0-9]+');
Route::put('users/{id}', ['as' => 'users.update', 'uses' => 'Siga\UsersController@update'])->where('id', '[0-9]+');

## Permissão dos Usuários do Sistema
Route::get('permissions', ['as' => 'permissions.index', 'uses' => 'Siga\PermissionsController@index']);
Route::get('permissions/create', ['as' => 'permissions.create', 'uses' => 'Siga\PermissionsController@create']);
Route::post('permissions', ['as' => 'permissions.store', 'uses' => 'Siga\PermissionsController@store']);
Route::delete('permissions/{id}', ['as' => 'permissions.destroy', 'uses' => 'Siga\PermissionsController@destroy'])->where('id', '[0-9]+');
Route::get('permissions/{id}/edit', ['as' => 'permissions.edit', 'uses' => 'Siga\PermissionsController@edit'])->where('id', '[0-9]+');
Route::put('permissions/{id}', ['as' => 'permissions.update', 'uses' => 'Siga\PermissionsController@update'])->where('id', '[0-9]+');


/*
|---------------------------------------------
| SIMOO - SISTEMA DE INTEGRAÇÃO PARA O MOODLE
|---------------------------------------------
 */

Route::group(['prefix' => 'simoo'], function()
{
    ## Turmas
    Route::get('turmas', function(){ 
        return redirect()->route('simoo.turmas.index'); 
    });
    Route::get('turmas/list/{year?}/{qtt?}',['as' => 'simoo.turmas.index', 'uses' => 'Simoo\Course\CourseController@index'])->where('year', '[0-9]+');
    Route::post('turmas', ['as' => 'simoo.turmas.store', 'uses' => 'Simoo\Course\CourseController@store']);
    Route::get('turmas/create', ['as' => 'simoo.turmas.create', 'uses' => 'Simoo\Course\CourseController@create']);
    Route::get('turmas/{id}', ['as' => 'simoo.turmas.show', 'uses' => 'Simoo\Course\CourseController@show'])->where('id', '[0-9]+');
    Route::put('turmas/{id}', ['as' => 'simoo.turmas.update', 'uses' => 'Simoo\Course\CourseController@update'])->where('id', '[0-9]+');
    Route::delete('turmas/{id}', ['as' => 'simoo.turmas.destroy', 'uses' => 'Simoo\Course\CourseController@destroy'])->where('id', '[0-9]+');
    Route::get('turmas/{id}/edit', ['as' => 'simoo.turmas.edit', 'uses' => 'Simoo\Course\CourseController@edit'])->where('id', '[0-9]+');
    
    ## Turmas - Config
    Route::get('turmas/config', ['as' => 'simoo.turmas.config', 'uses' => 'Simoo\Course\CourseConfigController@index']);
    Route::put('turmas/config/{id}', ['as' => 'simoo.turmas.config.update', 'uses' => 'Simoo\Course\CourseConfigController@update']);

    ## Turmas - Relatórios
    Route::get('turmas/{id}/frequency/{numberPage}', ['as' => 'simoo.turmas.frequency', 'uses' => 'Simoo\Course\CourseController@frequency'])->where('id', '[0-9]+')->where('pageNumber', '[0-9]+');
    Route::get('turmas/{id}/student_general', ['as' => 'simoo.turmas.student_general', 'uses' => 'Simoo\Course\CourseController@student_general'])->where('id', '[0-9]+');
    Route::get('turmas/{id}/student_grade', ['as' => 'simoo.turmas.student_grade', 'uses' => 'Simoo\Course\CourseController@student_grade'])->where('id', '[0-9]+');
    Route::get('turmas/{id}/student_frequency', ['as' => 'simoo.turmas.student_frequency', 'uses' => 'Simoo\Course\CourseController@frequency'])->where('id', '[0-9]+');
    Route::get('turmas/{id}/student_grade_frequency', ['as' => 'simoo.turmas.student_grade_frequency', 'uses' => 'Simoo\Course\CourseController@student_grade_frequency'])->where('id', '[0-9]+');

    ## Turmas - Avaliação de Reação
    Route::post('turmas/{id}/rate', ['as' => 'simoo.turmas.rate', 'uses' => 'Simoo\Course\CourseController@rate'])->where('id', '[0-9]+');

    ## Eventos
    Route::resource('eventos','Simoo\Course\CourseCategoryController')->names([ 
            'index' => 'simoo.eventos.index', 'store' => 'simoo.eventos.store', 
            'create' => 'simoo.eventos.create', 'show' => 'simoo.eventos.show', 
            'update' => 'simoo.eventos.update', 'destroy' => 'simoo.eventos.destroy', 
            'edit' => 'simoo.eventos.edit'
    ]);

    ## Usuários
    Route::get('usuarios',['as' => 'simoo.usuarios.index', 'uses' => 'Simoo\Users\UserController@index']);
    Route::post('usuarios',['as' => 'simoo.usuarios.store', 'uses' => 'Simoo\Users\UserController@store']);
    Route::get('usuarios/{id}',['as' => 'simoo.usuarios.show', 'uses' => 'Simoo\Users\UserController@show'])->where('id', '[0-9]+');
    Route::get('usuarios/{id}/edit',['as' => 'simoo.usuarios.edit', 'uses' => 'Simoo\Users\UserController@edit'])->where('id', '[0-9]+');
    Route::put('usuarios/{id}',['as' => 'simoo.usuarios.update', 'uses' => 'Simoo\Users\UserController@update'])->where('id', '[0-9]+');
    Route::delete('usuarios/{id}',['as' => 'simoo.usuarios.destroy', 'uses' => 'Simoo\Users\UserController@destroy'])->where('id', '[0-9]+');
    Route::get('usuarios/create',['as' => 'simoo.usuarios.create', 'uses' => 'Simoo\Users\UserController@create']);
    Route::get('usuarios/search',['as' => 'simoo.usuarios.search', 'uses' => 'Simoo\Users\UserController@search']);

    ## Usuários - Público-alvo
    Route::get('usuarios/target', ['as' => 'simoo.usuarios.target.index', 'uses' => 'Simoo\Users\UserTargetController@index']);

    ## Usuários - Público-alvo - Público
    Route::get('usuarios/target/public/create', ['as' => 'simoo.usuarios.target.public.create', 'uses' => 'Simoo\Users\UserPublicController@create']);
    Route::post('usuarios/target/public', ['as' => 'simoo.usuarios.target.public.store', 'uses' => 'Simoo\Users\UserPublicController@store']);
    Route::get('usuarios/target/public/{id}/edit', ['as' => 'simoo.usuarios.target.public.edit', 'uses' => 'Simoo\Users\UserPublicController@edit'])->where('id', '[0-9]+');
    Route::put('usuarios/target/public/{id}', ['as' => 'simoo.usuarios.target.public.update', 'uses' => 'Simoo\Users\UserPublicController@update'])->where('id', '[0-9]+');
    Route::delete('usuarios/target/public/{id}', ['as' => 'simoo.usuarios.target.public.destroy', 'uses' => 'Simoo\Users\UserPublicController@destroy'])->where('id', '[0-9]+');
    Route::post('usuarios/target/public/getId', ['as' => 'simoo.usuarios.target.public.getName', 'uses' => 'Simoo\Users\UserPublicController@getName']);

    ## Usuários - Público-alvo - Grupo
    Route::get('usuarios/target/group/create', ['as' => 'simoo.usuarios.target.group.create', 'uses' => 'Simoo\Users\UserGroupController@create']);
    Route::post('usuarios/target/group', ['as' => 'simoo.usuarios.target.group.store', 'uses' => 'Simoo\Users\UserGroupController@store']);
    Route::get('usuarios/target/group/{id}/edit', ['as' => 'simoo.usuarios.target.group.edit', 'uses' => 'Simoo\Users\UserGroupController@edit'])->where('id', '[0-9]+');
    Route::put('usuarios/target/group/{id}', ['as' => 'simoo.usuarios.target.group.update', 'uses' => 'Simoo\Users\UserGroupController@update'])->where('id', '[0-9]+');
    Route::delete('usuarios/target/group/{id}', ['as' => 'simoo.usuarios.target.group.destroy', 'uses' => 'Simoo\Users\UserGroupController@destroy'])->where('id', '[0-9]+');
    Route::post('usuarios/target/group/getId', ['as' => 'simoo.usuarios.target.group.getName', 'uses' => 'Simoo\Users\UserGroupController@getName']);

    ## Usuários - Público-alvo - Tipo
    Route::get('usuarios/target/type/create', ['as' => 'simoo.usuarios.target.type.create', 'uses' => 'Simoo\Users\UserTypeController@create']);
    Route::post('usuarios/target/type', ['as' => 'simoo.usuarios.target.type.store', 'uses' => 'Simoo\Users\UserTypeController@store']);
    Route::get('usuarios/target/type/{id}/edit', ['as' => 'simoo.usuarios.target.type.edit', 'uses' => 'Simoo\Users\UserTypeController@edit'])->where('id', '[0-9]+');
    Route::put('usuarios/target/type/{id}', ['as' => 'simoo.usuarios.target.type.update', 'uses' => 'Simoo\Users\UserTypeController@update'])->where('id', '[0-9]+');
    Route::delete('usuarios/target/type/{id}', ['as' => 'simoo.usuarios.target.type.destroy', 'uses' => 'Simoo\Users\UserTypeController@destroy'])->where('id', '[0-9]+');

    ## Gerenciar alunos, instrutores e coordenadores
    Route::get('alunos/all_students',['as' => 'simoo.alunos.all_students', 'uses' => 'Simoo\Users\UserEnrolledController@all_students']);
    Route::post('alunos/insert_student',['as' => 'simoo.alunos.insert_student', 'uses' => 'Simoo\Users\UserEnrolledController@insert_student']);
    Route::post('alunos/remove_student',['as' => 'simoo.alunos.remove_student', 'uses' => 'Simoo\Users\UserEnrolledController@remove_student']);
    Route::post('alunos/manager_grades',['as' => 'simoo.alunos.manager_grades', 'uses' => 'Simoo\Users\UserEnrolledController@manager_grades']);
    Route::get('instrutores/all_teachers',['as' => 'simoo.instrutores.all_teachers', 'uses' => 'Simoo\Users\UserEnrolledController@all_teachers']);
    Route::post('instrutores/insert_student',['as' => 'simoo.instrutores.insert_teacher', 'uses' => 'Simoo\Users\UserEnrolledController@insert_teacher']);
    Route::post('instrutores/edit_student',['as' => 'simoo.instrutores.edit_teacher', 'uses' => 'Simoo\Users\UserEnrolledController@edit_teacher']);
    Route::post('instrutores/remove_student',['as' => 'simoo.instrutores.remove_teacher', 'uses' => 'Simoo\Users\UserEnrolledController@remove_teacher']);
    Route::get('coordenadores/all_coordinators',['as' => 'simoo.coordenadores.all_coordinators', 'uses' => 'Simoo\Users\UserEnrolledController@all_coordinators']);
    Route::post('coordenadores/insert_coordinator',['as' => 'simoo.coordenadores.insert_coordinator', 'uses' => 'Simoo\Users\UserEnrolledController@insert_coordinator']);
    Route::post('coordenadores/edit_coordinator',['as' => 'simoo.coordenadores.edit_coordinator', 'uses' => 'Simoo\Users\UserEnrolledController@edit_coordinator']);
    Route::post('coordenadores/remove_coordinator',['as' => 'simoo.coordenadores.remove_coordinator', 'uses' => 'Simoo\Users\UserEnrolledController@remove_coordinator']);

    ## Agenda
    Route::get('calendario', ['as' => 'simoo.calendario.index', 'uses' => 'Simoo\Calendar\CalendarCourse@index']);

    ## Ambientes
    Route::get('ambientes',['as' => 'simoo.ambientes.index', 'uses' => 'Simoo\Environment\EnvironmentController@index']);
    Route::post('ambientes', ['as' => 'simoo.ambientes.store', 'uses' => 'Simoo\Environment\EnvironmentController@store']);
    Route::get('ambientes/create',['as' => 'simoo.ambientes.create', 'uses' => 'Simoo\Environment\EnvironmentController@create']);
    Route::put('ambientes/{id}', ['as' => 'simoo.ambientes.update', 'uses' => 'Simoo\Environment\EnvironmentController@update'])->where('id', '[0-9]+');
    Route::delete('ambientes/{id}', ['as' => 'simoo.ambientes.destroy', 'uses' => 'Simoo\Environment\EnvironmentController@destroy'])->where('id', '[0-9]+');
    Route::get('ambientes/{id}/edit', ['as' => 'simoo.ambientes.edit', 'uses' => 'Simoo\Environment\EnvironmentController@edit'])->where('id', '[0-9]+');

    ## Salas de Aula
    Route::post('salas', ['as' => 'simoo.salas.store', 'uses' => 'Simoo\Environment\RoomController@store']);
    Route::get('salas/create/{id}',['as' => 'simoo.salas.create', 'uses' => 'Simoo\Environment\RoomController@create'])->where('id', '[0-9]+');
    Route::put('salas/{id}', ['as' => 'simoo.salas.update', 'uses' => 'Simoo\Environment\RoomController@update'])->where('id', '[0-9]+');
    Route::delete('salas/{id}', ['as' => 'simoo.salas.destroy', 'uses' => 'Simoo\Environment\RoomController@destroy'])->where('id', '[0-9]+');
    Route::get('salas/{id}/edit', ['as' => 'simoo.salas.edit', 'uses' => 'Simoo\Environment\RoomController@edit'])->where('id', '[0-9]+');

    ## Relatórios

    ## Admin
});