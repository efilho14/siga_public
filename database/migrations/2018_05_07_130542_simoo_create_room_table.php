<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::connection('simoo')->hasTable('room')):
            Schema::connection('simoo')->create('room', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('environmentid')->unsigned();
                $table->string('name');
                $table->timestamps();

                $table->foreign('environmentid')
                    ->references('id')
                    ->on('environment')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('room');
    }
}
