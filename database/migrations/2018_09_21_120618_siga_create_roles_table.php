<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SigaCreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('siga')->hasTable('roles')) :
            Schema::connection('siga')->create('roles', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('name', 50);
                $table->string('label', 200);
                $table->integer('moduleid')->unsigned();
                $table->timestamps();

                $table->foreign('moduleid')
                    ->references('id')
                    ->on('modules')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siga')->dropIfExists('roles');
    }
}
