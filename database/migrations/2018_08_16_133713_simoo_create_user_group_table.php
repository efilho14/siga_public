<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateUserGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('simoo')->hasTable('user_group')):
            Schema::connection('simoo')->create('user_group', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('publicid')->unsigned();
                $table->string('name');
                $table->timestamps();

                $table->foreign('publicid')
                    ->references('id')
                    ->on('user_public')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('user_group');
    }
}
