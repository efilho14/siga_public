<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SigaCreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('siga')->hasTable('users')) :
            Schema::connection('siga')->create('users', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('name');
                $table->string('username',30)->unique();
                $table->string('roleid');
                $table->string('email');
                $table->string('password');
                $table->rememberToken();
                $table->timestamps();
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siga')->dropIfExists('users');
    }
}
