<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateUserPublicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('simoo')->hasTable('user_public')) :
            Schema::connection('simoo')->create('user_public', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('user_public');
    }
}
