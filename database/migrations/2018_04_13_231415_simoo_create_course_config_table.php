<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateCourseConfigTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('simoo')->hasTable('course_config')):
            Schema::connection('simoo')->create('course_config', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('frequency');
                $table->integer('grade');
                $table->timestamps();
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('course_config');
    }
}
