<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateStudentFrequencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::connection('simoo')->hasTable('student_frequency')):
            Schema::connection('simoo')->create('student_frequency', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('courseid')->unsigned();
                $table->integer('userid')->unsigned();
                $table->integer('frequency')->unsigned();
                $table->timestamps();

                $table->foreign('courseid')
                    ->references('id')
                    ->on('course')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('student_frequency');
    }
}
