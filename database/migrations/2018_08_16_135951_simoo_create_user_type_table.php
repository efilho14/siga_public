<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateUserTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('simoo')->hasTable('user_type')):
            Schema::connection('simoo')->create('user_type', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('groupid')->unsigned();
                $table->string('name');
                $table->timestamps();

                $table->foreign('groupid')
                    ->references('id')
                    ->on('user_group')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('user_type');
    }
}
