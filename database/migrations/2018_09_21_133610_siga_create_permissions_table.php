<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SigaCreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('siga')->hasTable('permissions')) :
            Schema::connection('siga')->create('permissions', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('name', 50);
                $table->string('label', 200);
                $table->timestamps();
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siga')->dropIfExists('permissions');
    }
}
