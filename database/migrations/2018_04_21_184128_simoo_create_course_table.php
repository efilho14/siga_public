<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::connection('simoo')->hasTable('course')):
            Schema::connection('simoo')->create('course', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('instanceid');
                $table->integer('location')->nullable();
                $table->integer('workload')->unsigned();
                $table->boolean('config');
                $table->integer('frequency')->nullable();
                $table->integer('grade')->nullable();
                $table->timestamps();
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('course');
    }
}
