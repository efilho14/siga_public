<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimooCreateCourseDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::connection('simoo')->hasTable('course_dates')):
            Schema::connection('simoo')->create('course_dates', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('courseid')->unsigned();
                $table->char('date',10);
                $table->char('hourinit',5);
                $table->char('hourend',5);
                $table->integer('block')->unsigned();

                $table->foreign('courseid')
                    ->references('id')
                    ->on('course')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('simoo')->dropIfExists('course_dates');
    }
}
