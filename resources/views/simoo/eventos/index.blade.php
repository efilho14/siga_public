@extends('main')

@section('title', 'Evento - Todos os Eventos')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Todos os Eventos</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Pesquisar Evento...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Buscar</button>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Lista dos Eventos</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if($category)
                        <table id="datatable" class="table table-striped table-last-bottom">
                          <thead>
                                <tr>
                                    <th>Evento/Curso</th>
                                    <th>Descrição</th>
                                    <th>Ativo</th>
                                    @if(Gate::check('courseCategory.edit') OR Gate::check('courseCategory.delete'))
                                        <th>Opções</th>
                                    @endif
                                </tr>
                          </thead>
                          <tbody>
                                @foreach($category as $categories)
                                <tr>
                                    <td class="event-name" style="{{ ToolsMoodleCategory::marginOrderEvents($categories->depth) }}">{{ $categories->name }}</td>
                                    <td>{{ $categories->description }}</td>
                                    <td>{{ ToolsGeneral::visible($categories->visible) }}</td>
                                    @if(Gate::check('courseCategory.edit') OR Gate::check('courseCategory.delete'))
                                        <td>
                                            @can('courseCategory.edit')
                                                <a href="{{ route('simoo.eventos.edit',$categories->categoryid) }}" class="btn btn-primary btn-sm">Editar</a>
                                            @endcan

                                            @can('courseCategory.edit')
                                                <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="{{ $categories->categoryid }}" data-toggle="modal" data-target=".modal-delete">Excluir</a>
                                            @endcan
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                          </tbody>
                        </table>
                    @else
                        <p>Não existem eventos cadastrados.</p>
                        <a href="{{ route('simoo.eventos.create') }}" class="btn btn-primary">Inserir Novo Evento</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Evento</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir o evento <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este evento.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let url = '{{ url()->full() }}/';
    </script>
    <!-- Custom -->
    {{ Html::script("js/simoo/eventos.js") }}
@endsection