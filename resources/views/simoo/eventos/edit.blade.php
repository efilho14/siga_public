@extends('main')

@section('title', 'Ambiente - Editar Evento')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Editar Evento</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Evento/Curso</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    {!! Form::model($category, ['route' => ['simoo.eventos.update',$category->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            {{ Form::label('name','Nome', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('name',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'name'], $category->name) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('description','Descrição', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('description', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'description'], $category->description) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('parent','Categoria-Pai', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="form-control" name="parent">
                                      <option value="0">-- Raíz --</option>
                                  @foreach($parents as $parent)
                                      <option {{ ($category->parent == $parent['id']) ? 'selected' : '' }} value="{{ $parent['id'] }}">{{ $parent['name'] }}</option>
                                  @endforeach
                              </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{ Form::submit('Editar Evento', ['class' => 'btn btn-success btn-lg']) }}
                                <a href="{{ route('simoo.eventos.index') }}" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- Switchery -->
    {{ Html::script("gentelella/vendors/switchery/dist/switchery.min.js") }}
@endsection
