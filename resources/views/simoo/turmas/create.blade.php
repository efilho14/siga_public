@extends('main')

@section('title', 'Turmas - Inserir Nova Turma')

@section('stylesheets')
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
    <!-- Select2 -->
    {!! Html::style('gentelella/vendors/select2/dist/css/select2.css') !!}
    <!-- bootstrap-daterangepicker -->
    {!! Html::style('gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css') !!}
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Inserir Nova Turma</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            {!! Form::open(['route' => 'simoo.turmas.store', 'data-parsley-validate' => '', 'class' => 'form-horizontal']) !!}

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Geral</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                    @if (count($errors) > 0)
                    <div class = "alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                        <br />

                        <div class="form-group">
                            {{ Form::label('category','Categoria', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="category" required='required'>
                                    @foreach($category as $categories)
                                        <option value="{{ $categories['id'] }}">{{ $categories['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('idnumber','Número da Turma', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('idnumber', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'idnumber', 'required' => '', 'placeholder' => 'NNO 01/18I']) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {{ Form::label('visible','Ativo', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::checkbox('visible',1, null, ['class' => 'js-switch', 'checked']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('workload','Carga Horária', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('workload', null, ['class' => 'form-control col-md-7 col-xs-12', 'required' => '', 'id' => 'workload']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('summary','Sumário', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::textarea('summary', null, array('class' => 'form-control col-md-7 col-xs-12')) }}
                            </div>
                        </div>

                    </div>
                </div>
            
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Outras Informações</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <br />

                        <div class="form-group">
                            {{ Form::label('location','Local', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="location" id="location" class="form-control">
                                    <option></option>
                                    @foreach($environment as $keyEnvironments => $environments)
                                        <optgroup label="{{ $environments['environmentName'] }}">
                                            @foreach($environments['rooms'] as $keyRoom => $room)
                                                <option value="{{ $room['roomId'] }}">{{ $room['roomName'] }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="dates-group">
                            <div class="form-group dates-default">
                                <div class="col-md-3 col-sm-2 col-xs-12 col-md-offset-3">
                                    {{ Form::label('dates','Período', ['class' => 'control-label']) }}
                                    {{ Form::text('dates[]', null, ['class' => 'form-control dates', 'required' => '', 'id' => 'dates']) }}
                                    {{ Form::hidden('block[]', 0, ['class' => 'block']) }}
                                    <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                    
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-12">
                                    {{ Form::label('hourinit','Início', ['class' => 'control-label']) }}
                                    {{ Form::text('hourinit[]', null, ['class' => 'form-control hourinit', 'maxlength' => '5', 'required' => '', 'id' => 'hourinit', 'placeholder' => '08:00']) }}
                                    <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-12">
                                    {{ Form::label('hourend','Término', ['class' => 'control-label']) }}
                                    {{ Form::text('hourend[]', null, ['class' => 'form-control hourend', 'maxlength' => '5', 'required' => '', 'id' => 'hourend', 'placeholder' => '12:00']) }}
                                    <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-12">
                                    {{ Form::button('Remover', array('class' => 'btn btn-danger btn-block btn-delete mt-5', 'disabled')) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="button" class="btn btn-dark new-date">Acrescentar novo período</button>
                            </div>
                        </div>

                        <div class="form-group mb-0">
                            <div class="col-md-6 col-sm-6 col-md-offset-3">    
                                <div class="ln_solid"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Nota e Frequência
                                <br>
                                <small class="text-navy">Deseja manter as configurações padrões?</small>
                            </label>

                            <div class="col-md-1 col-sm-6 col-xs-12 mt-2">
                                <div class="checkbox-course">
                                    <select name="fe_default" class="form-control fe-default" required="required">
                                        <option value="1">Sim</option>
                                        <option value="0">Não</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group frequency-group">
                            <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-3">
                                {{ Form::label('frequency','Frequência mínima', ['class' => 'control-label']) }}
                                {{ Form::text('frequency', $config->frequency, ['class' => 'form-control', 'maxlength' => '3', 'id' => 'frequency']) }}
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                {{ Form::label('grade','Nota mínima', ['class' => 'control-label']) }}
                                {{ Form::text('grade', $config->grade, ['class' => 'form-control', 'maxlength' => '2', 'id' => 'grade']) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success btn-lg">Criar Nova Turma</button>
                                <a href="{{ route('simoo.turmas.index') }}" class="btn btn-primary btn-lg">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>

@endsection

@section('scripts')
    <!-- Switchery -->
    {{ Html::script("gentelella/vendors/switchery/dist/switchery.min.js") }}
    <!-- Select2 -->
    {{ Html::script("gentelella/vendors/select2/dist/js/select2.full.min.js") }}
    <!-- bootstrap-daterangepicker -->
    {{ Html::script("gentelella/vendors/moment/min/moment.min.js") }}
    {{ Html::script("gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js") }}
    <!-- Tinymce -->
    {{ Html::script('//cdn.tinymce.com/4/tinymce.min.js') }}
    <!-- jquery.inputmask -->
    {{ Html::script('gentelella/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}
    <!-- Custom -->
    {{ Html::script('js/simoo/turmas/turmas_edit.js') }}
@endsection
