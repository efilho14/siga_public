@extends('main')

@section('title', 'Mostrar Turma')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>{{ $courseMoodle->fullname }} - {{ $courseMoodle->idnumber }}</h3>
        </div>

        <div class="title_right">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
                <div class="input-group input-group-right">
                    @can('course.edit') <a href="{{ route('simoo.turmas.edit',$courseMoodle->id) }}" class="btn btn-primary">Editar Turma</a> @endcan
                    @can('userEnrolled.gradeFrequency') <a href="#" class="btn btn-default mode-course mode-off"><i class="fa fa-cog"></i> </a> @endcan
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                        <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-print black"></i></a>
                        <ul class="dropdown-menu dropdown-menu-custom">
                            <li><a target="_blank" href="{{ route('simoo.turmas.frequency', [$courseMoodle->id, 1]) }}">Frequência não Preenchida</a></li>
                            <li>
                                @if( date('Y/m/d') >= $courseLastDate['date'] AND (count($questionnaire)>0) )
                                    <a target="_blank" href="#">Avaliação de Reação</a>
                                @endif
                            <li>
                            <li><a target="_blank" href="{{ route('simoo.turmas.student_grade', $courseMoodle->id) }}">Relação sem Nota </a></li>
                            <li><a target="_blank" href="{{ route('simoo.turmas.student_grade_frequency', $courseMoodle->id) }}">Relação com Nota e Frequência </a></li>
                            <li><a target="_blank" href="{{ route('simoo.turmas.student_general', $courseMoodle->id) }}">Relação Geral dos Alunos</a></li>
                        </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Informações sobre a turma</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#aluno" role="tab" data-toggle="tab" aria-expanded="true">Aluno (<span class="qtt_students">0</span>)</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#instrutor" role="tab" data-toggle="tab" aria-expanded="false">Instrutor (<span class="qtt_teachers">0</span>)</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#coordenador" role="tab" data-toggle="tab" aria-expanded="false">Coordenador (<span class="qtt_coordinators">0</span>)</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">

                            <div role="tabpanel" class="tab-pane fade active in" id="aluno" aria-labelledby="home-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped table-last-bottom table-all-students">
                                        <thead>
                                        <tr>
                                            <th width="2%"></th>
                                            <th width="30%">Nome</th>
                                            <th>CPF</th>
                                            <th>E-mail</th>
                                            <th>Frequência (h)</th>
                                            <th>Nota</th>
                                            <th>Público</th>
                                            <th>Situação</th>
                                        </tr>
                                        </thead>
                                        <tbody class="for-all-students"></tbody>
                                    </table>
                                    <p class="msg-students-enrolled">Carregando alunos...</p>
                                    <p class="no-students-enrolled">Não existem alunos inscritos nesta turma.</p>
                                    <hr class="no-students-enrolled" style="margin-bottom:15px">
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group input-group-left">
                                        @can('userEnrolled.insertStudent')<button class="btn btn-primary btn-insert-aluno" data-toggle="modal" data-target=".modal-student">Matricular Aluno</button>@endcan
                                        @can('userEnrolled.deleteStudent')<button class="btn btn-danger btn-delete-aluno">Remover Aluno</button>@endcan
                                        @if(date('Y/m/d') >= $courseLastDate['date'])
                                            {!! Form::open(['route' => ['simoo.turmas.rate',$courseMoodle->id], 'data-parsley-validate' => '', 'style' => 'float:right']) !!}
                                                <button class="btn {{ (count($questionnaire)>0) ? 'btn-success' : 'btn-warning' }} btn-rate-service"><i class="fa fa-check-square-o"></i></button>
                                            {!! Form::close() !!}
                                        @endif
                                    </div>
                                    <div class="input-group input-group-right">
                                        <button href="{{ route('simoo.turmas.edit',$courseMoodle->id) }}" class="btn btn-info save-frequency-grade">Salvar Alteração</button>
                                        <div class="loading-grade"></div>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="instrutor" aria-labelledby="profile-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped table-last-bottom table-all-teachers">
                                        <thead>
                                        <tr>
                                            <th width="60%">Nome</th>
                                            <th>Tipo</th>
                                            <th width="15%">Opções</th>
                                        </tr>
                                        </thead>
                                        <tbody class="for-all-teachers"></tbody>
                                    </table>
                                    <p class="no-teachers">Não existem instrutores inscritos nesta turma.</p>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search box-insert-teacher">
                                    <div class="input-group">
                                        @can('userEnrolled.insertTeacher')<button class="btn btn-primary btn-insert-teacher" data-toggle="modal" data-target=".modal-teacher">Inserir Instrutor</button>@endcan
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="coordenador" aria-labelledby="profile-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped table-last-bottom table-all-coordinators">
                                        <thead>
                                        <tr>
                                            <th width="85%">Nome</th>
                                            <th>Opções</th>
                                        </tr>
                                        </thead>
                                        <tbody class="for-all-coordinators"></tbody>
                                    </table>
                                    <p class="no-coordinators">Não existem coordenadores inscritos nesta turma.</p>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        @can('userEnrolled.insertCoordinator')<button class="btn btn-primary btn-insert-coordinator" data-toggle="modal" data-target=".modal-coordinator">Inserir Coordenador</button>@endcan
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
    </div>

    @can('userEnrolled.insertStudent')
        <!-- modal student -->
        <div class="modal fade modal-student" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Matricular Aluno</h4>
                    </div>
                    <div id="search" class="modal-body">
                        <div class="form-group">
                            <p>Pesquisar por (Nome / CPF)</p>
                            <div class="input-group">
                                <input type="text" class="form-control search-student" name="name_user" style="text-transform:uppercase"/>
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-search-student" type="button">Buscar</button>
                                </span>
                            </div>
                            <hr>
                            <p class="msg-search-students">Pesquisando...</p>
                        </div>
                        <div class="form-group all-search-student">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nome</th>
                                        <th>CPF</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                <tbody class="for-students"></tbody>
                            </table>

                            <div class="pages pagination-student"></div>
                            
                            <div class="contentLoading">
                                <div class="loading"><img src="/../images/loading_icon.gif" alt="loading"/></div>
                            </div>
                            <hr>
                            <button class="btn btn-success btn-block btn-enrol-student">Matricular</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @if(Gate::check('userEnrolled.editTeacher') OR Gate::check('userEnrolled.insertTeacher'))
        <!-- modal teacher -->
        <div class="modal fade modal-teacher" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Inserir Instrutor</h4>
                    </div>
                    <div id="search" class="modal-body">
                        <div class="form-group">
                            <p>Pesquisar por (Nome / CPF)</p>
                            <div class="input-group">
                                <input type="text" class="form-control search-teacher" name="name_user" style="text-transform:uppercase"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-default type-teacher" data-teacher-type="">Instrutor Titular</button>
                                    <button href="#" class="btn btn-primary btn-search-teacher">Buscar</button>
                                </div>
                            </div>
                            <hr>
                            <p class="msg-search-teachers">Pesquisando...</p>
                        </div>
                        <div class="form-group all-search-teacher">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nome</th>
                                        <th>CPF</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                <tbody class="for-teachers"></tbody>
                            </table>
                            
                            <div class="pages pagination-teacher"></div>

                            <div class="contentLoading">
                                <div class="loading"><img src="/../images/loading_icon.gif" alt="loading"/></div>
                            </div>
                            <hr>
                            <button class="btn btn-success btn-block btn-insert-enrol-teacher">Matricular</button>
                            <button class="btn btn-success btn-block btn-edit-enrol-teacher">Editar Instrutor</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(Gate::check('userEnrolled.editCoordinator') OR Gate::check('userEnrolled.deleteCoordinator'))
        <!-- modal coordinator -->
        <div class="modal fade modal-coordinator" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Inserir Coordenador</h4>
                    </div>
                    <div id="search" class="modal-body">
                        <div class="form-group">
                            <p>Pesquisar por (Nome / CPF)</p>
                            <div class="input-group">
                                <input type="text" class="form-control search-coordinator" name="name_user" style="text-transform:uppercase"/>
                                <div class="input-group-btn">
                                    <button href="#" class="btn btn-primary btn-search-coordinator">Buscar</button>
                                </div>
                            </div>
                            <hr>
                            <p class="msg-search-coordinators">Pesquisando...</p>
                        </div>
                        <div class="form-group all-search-coordinator">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nome</th>
                                        <th>CPF</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                <tbody class="for-coordinators"></tbody>
                            </table>
                            
                            <div class="pages pagination-coordinator"></div>

                            <div class="contentLoading">
                                <div class="loading"><img src="/../images/loading_icon.gif" alt="loading"/></div>
                            </div>
                            <hr>
                            <button class="btn btn-success btn-block btn-insert-enrol-coordinator">Matricular</button>
                            <button class="btn btn-success btn-block btn-edit-enrol-coordinator">Editar Coordenador</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('scripts')
    <script>
        //variavel global
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let courseid = {{ $courseMoodle->id }};
        let workload = {{ $courseSimoo->workload }};
        let defaultfrequency = {{ $config->frequency }};
        let defaultgrade = {{ $config->grade }};

        //usuários que serão convidados
        let invited = ['Terceirizado'];

        //usuarios
        let url_user_search = "{!! route('simoo.usuarios.search') !!}"

        //alunos
        let url_all_students = "{!! route('simoo.alunos.all_students') !!}";
        let url_insert_student = "{!! route('simoo.alunos.insert_student') !!}";
        let url_remove_student = "{!! route('simoo.alunos.remove_student') !!}";
        let url_manager_grades = "{!! route('simoo.alunos.manager_grades') !!}";

        //instrutor
        let url_all_teachers = "{!! route('simoo.instrutores.all_teachers') !!}";
        let url_insert_teacher = "{!! route('simoo.instrutores.insert_teacher') !!}";
        let url_edit_teacher = "{!! route('simoo.instrutores.edit_teacher') !!}";
        let url_remove_teacher = "{!! route('simoo.instrutores.remove_teacher') !!}";

        //coordenador
        let url_all_coordinators = "{!! route('simoo.coordenadores.all_coordinators') !!}";
        let url_insert_coordinator = "{!! route('simoo.coordenadores.insert_coordinator') !!}";
        let url_edit_coordinator = "{!! route('simoo.coordenadores.edit_coordinator') !!}";
        let url_remove_coordinator = "{!! route('simoo.coordenadores.remove_coordinator') !!}";
    </script>
    {{ Html::script("js/simoo/turmas/turmas_ge_global.js") }}
    {{ Html::script("js/simoo/turmas/turmas_ge_alunos.js") }}
    {{ Html::script("js/simoo/turmas/turmas_ge_instrutores.js") }}
    {{ Html::script("js/simoo/turmas/turmas_ge_coordenadores.js") }}
@endsection