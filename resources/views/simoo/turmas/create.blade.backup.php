@extends('main')

@section('title', '| All Posts')

@section('stylesheets')
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
    <!-- Select2 -->
    {!! Html::style('gentelella/vendors/select2/dist/css/select2.css') !!}
    <!-- bootstrap-daterangepicker -->
    {!! Html::style('gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css') !!}
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Inserir Nova Turma</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            {!! Form::open(['route' => 'simoo.turmas.store', 'data-parsley-validate', 'class' => 'form-horizontal']) !!}

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Geral</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <br />
                            
                        <div class="form-group">
                            {{ Form::label('fullname','Nome completo do Curso', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('fullname', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'fullname', 'placeholder' => 'Desenvolvimento Pessoal']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('shortname','Nome abreviado do Curso', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('shortname', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'shortname', 'placeholder' => 'Desenv. Pessoal']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('idnumber','Número da Turma', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('idnumber', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'idnumber', 'placeholder' => '01/18I']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('category','Categoria', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="parent">
                                    @foreach($category as $categories)
                                        <option value="{{ $categories['id'] }}">{{ $categories['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('visible','Ativo', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::checkbox('visible',1, null, ['class' => 'js-switch', 'checked']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('workload','Carga Horária', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('workload', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'workload']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('sumary','Sumário', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::textarea('sumary', null, array('class' => 'form-control col-md-7 col-xs-12')) }}
                            </div>
                        </div>

                    </div>
                </div>
            
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Outras Informações</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <br />

                        <div class="form-group">
                            {{ Form::label('location','Local', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="location" id="location" class="form-control">
                                    <option></option>
                                    <optgroup label="Edf. San Rafael">
                                        <option value="1">Laboratório 01</option>
                                        <option value="2">Laboratório 02</option>
                                    </optgroup>
                                    <optgroup label="Cruz Cabugá">
                                        <option value="3">Sala 01</option>
                                        <option value="4">Auditório</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class="dates-group">
                            <div class="form-group dates-default">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-3">
                                    {{ Form::label('dates','Período', ['class' => 'control-label']) }}
                                    {{ Form::text('dates[]', null, ['class' => 'form-control dates', 'id' => 'dates']) }}
                                </div>
                                <div class="col-md-2 col-sm-3 col-xs-12">
                                    {{ Form::label('hourinit','Início', ['class' => 'control-label']) }}
                                    {{ Form::text('hourinit', null, ['class' => 'form-control hourinit', 'maxlength' => '5', 'id' => 'hourinit', 'placeholder' => '08:00']) }}
                                </div>
                                <div class="col-md-2 col-sm-3 col-xs-12">
                                    {{ Form::label('hourend','Término', ['class' => 'control-label']) }}
                                    {{ Form::text('hourend', null, ['class' => 'form-control hourend', 'maxlength' => '5', 'id' => 'hourend', 'placeholder' => '12:00']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="button" class="btn btn-dark btn-sm new-date">Acrescentar novo período</button>
                            </div>
                        </div>

                        <div class="form-group mb-0">
                            <div class="col-md-6 col-sm-6 col-md-offset-3">    
                                <div class="ln_solid"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Nota e Frequência
                                <br>
                                <small class="text-navy">Deseja manter as configurações padrões?</small>
                            </label>

                            <div class="col-md-1 col-sm-6 col-xs-12 mt-2">
                                <div class="checkbox-course">
                                    <select name="fe-default" class="form-control fe-default">
                                        <option value="1">Sim</option>
                                        <option value="0">Não</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group frequency-group">
                            <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-3">
                                {{ Form::label('frequency','Frequência mínima', ['class' => 'control-label']) }}
                                {{ Form::text('frequency', null, ['class' => 'form-control', 'maxlength' => '3', 'id' => 'frequency']) }}
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                {{ Form::label('grade','Nota mínima', ['class' => 'control-label']) }}
                                {{ Form::text('grade', null, ['class' => 'form-control', 'maxlength' => '2', 'id' => 'grade']) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success btn-lg">Criar Nova Turma</button>
                                <a href="{{ route('simoo.turmas.index') }}" class="btn btn-primary btn-lg">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>

@endsection

@section('scripts')
    <!-- Switchery -->
    {{ Html::script("gentelella/vendors/switchery/dist/switchery.min.js") }}
    <!-- Select2 -->
    {{ Html::script("gentelella/vendors/select2/dist/js/select2.full.min.js") }}
    <!-- bootstrap-daterangepicker -->
    {{ Html::script("gentelella/vendors/moment/min/moment.min.js") }}
    {{ Html::script("gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js") }}
    <!-- Tinymce -->
    {{ Html::script('//cdn.tinymce.com/4/tinymce.min.js') }}
    <!-- jquery.inputmask -->
    {{ Html::script('gentelella/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}
    <!-- Custom -->
    {{ Html::script('js/simoo/turmas.js') }}
    <script>
        $(document).ready(function () {
            //select2
            $("#location").select2({
                placeholder: "Selecione uma opção"
            });

            //nota e frequencia
            $('.frequency-group').hide();
            $('.fe-default').change(function () {
                if ($(this).val() == 0) {
                    $('.frequency-group').show();
                } else if ($(this).val() == 1) {
                    $('.frequency-group').hide();
                }
            });

            //daterangepicker
            let noDates = ['16-04-2018', '17-04-2018', '18-04-2018', '19-04-2018', '20-04-2018'];
            let newDate = moment(
                noDates[noDates.length - 1], "DD-MM-YYYY"
            ).add(1, 'days');

            function datapicker() {

                $('.hourinit').inputmask("99:99");
                $('.hourend').inputmask("99:99");

                $(".dates").each(function () {
                    
                    $('.dates').daterangepicker({
                        startDate: newDate,
                        endDate: newDate,
                        isInvalidDate: function (date){
                            for (let i = 0; i < noDates.length; i++){
                                if (date.format('DD-MM-YYYY') == noDates[i]){
                                    return true;
                                }
                            }
                        },

                        locale: {
                            format: "DD/MM/YYYY",
                            daysOfWeek: ["D", "S", "T", "Q", "Q", "S", "S"],
                            monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                            applyLabel: "Aplicar",
                            cancelLabel: "Cancelar",
                        },
                    });

                });
            } datapicker();

            //adicionar novo período
            $('.new-date').click(function () {
                let datesDefault = $('.dates-default').html();
                $('.dates-group').append('<div class="form-group dates-default">' + datesDefault + '</div>');
                datapicker();
            });
            
            //tiny
            tinymce.init({
                menubar: false,
                selector: 'textarea',
                plugins: ["advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
                ]
            });
        });
    </script>
@endsection
