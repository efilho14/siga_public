@extends('main')

@section('title', 'Evento - Todas as Turmas')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Todas as Turmas</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Pesquisar Turma...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Buscar</button>
                    </span>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">{{ $yearCurrent }} <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu" style="position:absolute; top:45px; left:22px; min-width:65px !important">
                        @foreach($years as $year => $value)
                            <li class="{{ ($value['actual']=='Y') ? 'choose_year' : '' }}" ><a href="{{ route('simoo.turmas.index', $year) }}">{{ $year }}</a></li>
                        @endforeach                        
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                    <div class="col-xs-12">
                        @if($courses)

                            @foreach($courses as $month => $course)
                                
                                <h3 id="{{ $month }}" class="green">{{ $month }}</h3>

                                @if($course)
                                    <table class="table table-striped table-last-bottom">
                                        <thead>
                                            <tr>
                                                <th style="width:20%">Evento</th>
                                                <th>Turma</th>
                                                <th>Período</th>
                                                <th>Horário</th>
                                                <th>Inscritos</th>
                                                @if(Gate::check('course.edit') OR Gate::check('course.delete'))
                                                    <th>Opções</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($course as $info)
                                                <tr>
                                                    <td class="event-fullname"><a target="_blank" href="{{ route('simoo.turmas.show', $info['courseid']) }}">{{ $info['fullname'] }}</a></td>
                                                    <td class="event-shortname">{{ $info['shortname'] }}</td>
                                                    <td>
                                                        @foreach($info['dates']['dateParcial'] as $key => $date)
                                                            <p class="{{ ( (count($info['dates']['dateParcial'])-1) == $key) ? 'no-margin' : '' }}">{{ $date }}</p>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        @foreach($info['dates']['hourParcial'] as $key => $date)    
                                                            <p class="{{ ( (count($info['dates']['hourParcial'])-1) == $key) ? 'no-margin' : '' }}">{{ $info['dates']['hourParcial'][0] }}</p>
                                                        @endforeach
                                                    </td>
                                                    <td>{{ $info['enrolled'] }}</td>
                                                    @if(Gate::check('course.edit') OR Gate::check('course.delete'))
                                                        <td>
                                                            @can('course.edit')
                                                                <a href="{{ route('simoo.turmas.edit', $info['courseid']) }}" class="btn btn-primary btn-sm">Editar</a>
                                                            @endcan

                                                            @can('course.delete')
                                                                <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="{{ $info['courseid'] }}" data-toggle="modal" data-target=".modal-delete">Excluir</a>
                                                            @endcan
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <table class="table table-striped table-last-bottom">
                                        <tbody>
                                            <td>Não existem turmas cadastradas para este mês.</td>
                                        </tbody>
                                    </table>
                                @endif

                            @endforeach

                            @if($qtyMouth == null)
                                <div class="show_courses">
                                    <a href="{{ route('simoo.turmas.index') }}/{{ $yearCurrent }}/12" class="btn btn-success btn-lg">Mostrar mais</a>
                                </div>
                            @else
                                <div class="show_courses">
                                    <a href="{{ route('simoo.turmas.index') }}" class="btn btn-success btn-lg">Mostrar menos</a>
                                </div>
                            @endif

                        @else

                            <p>Não existem turmas cadastradas para este ano.</p>
                            <a href="{{ route('simoo.turmas.create') }}" class="btn btn-primary">Inserir Nova Turma</a>

                        @endif
                    </div>                    

                </div>
            </div>
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Turma</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir a turma <br><strong>"<span class="modal-fullname"></span> - <span class="modal-shortname"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este evento.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let url = "{{ route('simoo.turmas.store') }}/";
    </script>
    <!-- Custom -->
    {{ Html::script("js/simoo/turmas/turmas_delete.js") }}
@endsection