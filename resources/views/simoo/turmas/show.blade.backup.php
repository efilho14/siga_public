@extends('main')

@section('title', '| Mostrar Turma')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>{{ $course->fullname }} - {{ $course->idnumber }}</h3>
        </div>

        <div class="title_right">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
                <div class="input-group input-group-right">
                    <a href="{{ route('simoo.turmas.edit',$course->id) }}" class="btn btn-primary">Editar Turma</a>
                    <a href="#" class="btn btn-default"><i class="fa fa-cog"></i> </a>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                        <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-print black"></i></a>
                        <ul class="dropdown-menu dropdown-menu-custom">
                            <li><a href="#">Imprimir Frequência</a></li>
                            <li><a href="#">Imprimir Nota</a></li>
                            <li><a href="#">Imprimir Relação</a></li>
                        </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Informações sobre a turma</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#aluno" role="tab" data-toggle="tab" aria-expanded="true">Aluno</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#instrutor" role="tab" data-toggle="tab" aria-expanded="false">Instrutor</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#coordenador" role="tab" data-toggle="tab" aria-expanded="false">Coordenador</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="aluno" aria-labelledby="home-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped table-last-bottom">
                                        <thead>
                                        <tr>
                                            <th width="30%">Nome</th>
                                            <th>CPF</th>
                                            <th>Frequência (h)</th>
                                            <th>Nota</th>
                                            <th>Público</th>
                                            <th width="10%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>EDUARDO GOMES DE ALMEIDA FILHO</td>
                                                <td>06719957406</td>
                                                <td><input type="number" name="frequency[]"  class="form-control input-sm input-40" min="1" max="999" maxlength="3"></td>
                                                <td><input type="number" name="grade[]" class="form-control input-sm input-40" min="1" max="99" maxlength="2"></td>
                                                <td>Convidado</td>
                                                <td class="align-right">
                                                    <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>KATHARINE DE MELO CAVALCANTI SILVA</td>
                                                <td>15798457526</td>
                                                <td>18h - 90%</td>
                                                <td>9</td>
                                                <td>Administrativos</td>
                                                <td class="align-right">
                                                    <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>PAULO KAZUO KATO</td>
                                                <td>57798457526</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>Convidado</td>
                                                <td class="align-right">
                                                    <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group input-group-left">
                                        <a href="#" class="btn btn-primary btn-insert-aluno" data-toggle="modal" data-target=".modal-insert-aluno">Matricular Aluno</a>
                                    </div>
                                    <div class="input-group input-group-right">
                                        <a href="{{ route('simoo.turmas.edit',$course->id) }}" class="btn btn-success">Salvar Alteração</a>
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="instrutor" aria-labelledby="profile-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped table-last-bottom">
                                        <thead>
                                        <tr>
                                            <th width="60%">Nome</th>
                                            <th>Tipo</th>
                                            <th width="15%">Opções</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>FERNANDO ANTONIO ALVIM</td>
                                                <td>TITULAR</td>
                                                <td>
                                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>FERNANDO ANTONIO VILAROUCA MOREIRA</td>
                                                <td>SECUNDÁRIO</td>
                                                <td>
                                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <a href="{{ route('simoo.turmas.edit',$course->id) }}" class="btn btn-primary">Inserir Instrutor</a>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="coordenador" aria-labelledby="profile-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped table-last-bottom">
                                        <thead>
                                        <tr>
                                            <th width="85%">Nome</th>
                                            <th>Opções</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>ELIZABETH MARIA RABELLO</td>
                                                <td>
                                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <a href="{{ route('simoo.turmas.edit',$course->id) }}" class="btn btn-primary">Inserir Coordenador</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
    </div>

    <!-- modal insert aluno -->
    <div class="modal fade modal-insert-aluno" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Matricular Aluno</h4>
                </div>
                <div id="search" class="modal-body">
                    <div class="form-group">
                        <p>Pesquisar por (Nome / CPF)</p>
                        <form method="POST" action="{{ route('simoo.alunos.search') }}" accept-charset="UTF-8">
                            {!! csrf_field() !!}
                            <input v-on:keyup="pesquisar" v-model="apesquisar" type="text" class="form-control search" style="text-transform:uppercase"/>
                        </form>

                        <hr>
                        <p style="text-align:center;color: #26b99a; font-weight:bold">@{{msg}}</p>
                    </div>

                    <div class="form-group" v-if="usuarios.length != 0">
                        
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nome</th>
                                    <th>CPF</th>
                                    <th>E-mail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="item in usuarios">
                                    <td><input type="checkbox" name="student_check"/></td>
                                    <td>@{{item.firstname}} @{{item.lastname}}</td>
                                    <td>@{{item.username}}</td>
                                    <td>@{{item.email}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <input class="btn btn-success btn-block" type="submit" value="Matricular">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{ Html::script("vuejs/vue.min.js") }}
    {{ Html::script("vuejs/vue-resource.min.js") }}
    <script>
        var app = new Vue({
            el: '#search',
            data: {
                msg:'',
                apesquisar:'',
                usuarios: []
            },
            methods: {
                pesquisar: function(){
                    if(this.apesquisar.length>=3){
                        this.msg = 'Pesquisando...';
                        this.$http.get("{!! route('simoo.alunos.search') !!}",
                                {params:{search:this.apesquisar,}}).then(response => {
                                    
                            this.usuarios = response.body;
                            if(this.usuarios.length != 0){
                                this.msg = this.usuarios.length + ' alunos encontrados';
                            }else{
                                this.msg = 'Nenhum aluno encontrado';
                                this.usuarios = [];
                            }
                        });

                    }else if(this.apesquisar.length < 3){
                        this.msg = '';
                        this.usuarios = [];
                    }
                }
            }
        });
    </script>
@endsection