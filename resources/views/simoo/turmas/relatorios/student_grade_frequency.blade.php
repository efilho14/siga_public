@extends('plain')

@section('title', 'Turmas - Frequência e Notas dos Participantes')

@section('stylesheets')
    <!-- print -->
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/print.css') }}">
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
@endsection

@section('content')

    <div class="full_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h2 class="text-center">Frequência e Notas dos Participantes</h2>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel x_panel_frequency">
                        <div class="x_title">
                            <h3 class="m-bottom-1">{{ $course['fullname'] }} - {{ $course['idnumber'] }}</h3>
                        </div>
                        <div class="x_content x_content_frequency">
                            <table class="b-top-1">
                                <tbody>
                                    <tr>
                                        <td class="p-top-1 p-bottom-1" style="width:50%">
                                            <div class="block_frequency m-bottom-1">
                                                <p><strong>Instrutor(es):</strong></p>
                                                @foreach($course['teacher'] as $teacher)
                                                    <p>{{ $teacher['firstname'] }} {{ $teacher['lastname'] }}</p>
                                                @endforeach
                                            </div>
                                            <div class="block_frequency">
                                                <p><strong>Coordenação:</strong></p>
                                                @foreach($course['coordinator'] as $coordinator)
                                                    <p>{{ $coordinator['firstname'] }} {{ $coordinator['lastname'] }}</p>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="p-top-1 p-bottom-1">
                                            <div class="block_frequency m-bottom-1">
                                                <p><strong>Carga Horária:</strong></p>
                                                <p>{{ $course['workload'] }} hora(s)</p>
                                            </div>
                                            <div class="block_frequency m-bottom-1">
                                                <p><strong>Frequência Mínima:</strong></p>
                                                <p>{{ $course['frequency'] }}% por capacitação</p>
                                            </div>
                                            <div class="block_frequency">
                                                <p><strong>Nota Mínima:</strong></p>
                                                <p>{{ $course['grade'] }},00 por capacitação</p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="m-bottom-1">
                                <thead>
                                    <tr>
                                        <th class="b-right-1" width="50%">Nome</th>
                                        <th class="b-right-1 text-center">Frequência</th>
                                        <th class="b-right-1 text-center">Nota</th>
                                        <th class="b-right-1 text-center">Situação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($course['student'] as $students)
                                        <tr>
                                            <td class="b-full-1 p-full-1">{{ $students['firstname'] }} {{ $students['lastname'] }}</td>
                                            <td class="text-center b-full-1 p-full-1">
                                                {{ ( ($students['frequency']) ? $students['frequency'] . 'h - (' . round((($course['workload'] - ($course['workload'] - $students['frequency'])) / $course['workload']) * 100) . '%)' : '-' ) }}
                                            </td>
                                            <td class="text-center b-full-1 p-full-1">{{ $students['grade'] }}</td>
                                            <td class="text-center b-full-1 p-full-1">{{ $students['situation'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <p class="no-margin m-left-1"><strong>Total de participantes:</strong> {{ count($course['student']) }}</p>
                            <p class="no-margin m-left-1"><strong>Total de aprovados:</strong> {{ $course['approved']}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

