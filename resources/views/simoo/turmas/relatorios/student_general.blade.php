@extends('plain')

@section('title', 'Turmas - Relação Geral dos Alunos')

@section('stylesheets')
    <!-- print -->
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/print.css') }}">
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
@endsection

@section('content')

    <div class="full_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h2 class="text-center">Relação Geral dos Alunos</h2>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel x_panel_frequency">
                        <div class="x_title">
                            <h3 class="m-bottom-1">{{ $course['fullname'] }} - {{ $course['idnumber'] }}</h3>
                        </div>
                        <div class="x_content x_content_frequency">
                            <table class="b-top-1">
                                <tbody>
                                    <tr>
                                        <td class="p-top-1 p-bottom-1">
                                            <div class="block_frequency m-bottom-1">
                                                <p><strong>Instrutor(es):</strong></p>
                                                @foreach($course['teacher'] as $teacher)
                                                    <p>{{ $teacher['firstname'] }} {{ $teacher['lastname'] }}</p>
                                                @endforeach
                                            </div>
                                            <div class="block_frequency">
                                                <p><strong>Coordenação:</strong></p>
                                                @foreach($course['coordinator'] as $coordinator)
                                                    <p>{{ $coordinator['firstname'] }} {{ $coordinator['lastname'] }}</p>
                                                @endforeach
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="m-bottom-1">
                                <thead>
                                    <tr>
                                        <th class="b-right-1" width="50%">Nome</th>
                                        <th class="b-right-1 text-center">Matrícula</th>
                                        <th class="b-right-1 text-center">CPF</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($course['student'] as $students)
                                        <tr>
                                            <td class="b-full-1 p-full-1">{{ $students['firstname'] }} {{ $students['lastname'] }}</td>
                                            <td class="text-center b-full-1 p-full-1">
                                                @if(isset($students['customfields']))
                                                    
                                                    @foreach($students['customfields'] as $custom)

                                                        @if($custom['shortname'] == 'registry')

                                                            {{ $custom['value'] }}

                                                        @endif

                                                    @endforeach
                                                    
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                            <td class="text-center b-full-1 p-full-1">{{ ToolsGeneral::mask($students['username'],'###.###.###-##') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <p class="no-margin m-left-1"><strong>Total:</strong> {{ count($course['student']) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

