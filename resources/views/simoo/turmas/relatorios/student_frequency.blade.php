@extends('plain')

@section('title', 'Turmas - Frequência não Preenchida')

@section('stylesheets')
    <!-- print -->
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/print.css') }}">
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
@endsection

@section('content')

    <div class="full_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h2 class="text-center">Lista de Frequência</h2>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel x_panel_frequency">
                        <div class="x_title">
                            <h3 class="m-bottom-1">{{ $course['fullname'] }} - {{ $course['idnumber'] }}</h3>
                        </div>
                        <div class="x_content x_content_frequency">
                            <table class="b-top-1">
                                <tbody>
                                    <tr>
                                        <td class="p-top-1 p-bottom-1">
                                            <div class="block_frequency m-bottom-1">
                                                <p><strong>Instrutor(es):</strong></p>
                                                @foreach($course['teacher'] as $teacher)
                                                    <p>{{ $teacher['firstname'] }} {{ $teacher['lastname'] }}</p>
                                                @endforeach
                                            </div>
                                            <div class="block_frequency">
                                                <p><strong>Coordenação:</strong></p>
                                                @foreach($course['coordinator'] as $coordinator)
                                                    <p>{{ $coordinator['firstname'] }} {{ $coordinator['lastname'] }}</p>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="p-top-1 p-bottom-1 p-left-1 ">
                                            <div class="block_frequency m-bottom-1">
                                                <p><strong>Período:</strong></p>
                                                @foreach($course['dates']['dateParcial'] as $key => $date)
                                                    <p>@if(count($course['dates']['dateParcial']) > 1) {{ ($key+1) }}º) @endif {{ $date }}</p>
                                                @endforeach
                                            </div>
                                            <div class="block_frequency">
                                                <p><strong>Horário:</strong></p>
                                                @foreach($course['dates']['hourParcial'] as $key => $hour)
                                                    <p>@if(count($course['dates']['dateParcial']) > 1) {{ ($key+1) }}º) @endif {{ $hour }}</p>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="p-top-1 p-bottom-1 p-left-1">
                                            <div class="block_frequency m-bottom-1">
                                                <p><strong>Local:</strong></p>
                                                <p>{{ $course['environment'] }}</p>
                                            </div>
                                            <div class="block_frequency">
                                                <p><strong>Sala:</strong></p>
                                                <p>{{ $course['room'] }}</p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="m-bottom-1">
                                <thead>
                                    <tr>
                                        <th class="b-right-1" width="25%">Nome</th>
                                        <th class="b-right-1 text-center">Matrícula</th>
                                        <th class="b-right-1 text-center">CPF</th>
                                        <td class="no-padding">
                                            <table class="w-100-percent">
                                                <tr>
                                                    <th colspan="5" class="text-center w-100-percent p-top-0 p-bottom-0">Assinatura / Data</th>
                                                </tr>
                                                <tr class="b-top-1">
                                                    @foreach($course['dates']['dateFull'][$page] as $dates)
                                                        <th width="20%" style="text-align:center; padding:5px 0; border-right: 1px solid #fff">{{ $dates }} ({{ ToolsGeneral::week( ToolsGeneral::convertDatePtToEn($dates) )  }})</th>
                                                    @endforeach
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($course['student'] as $students)
                                        <tr>
                                            <td class="b-full-1 p-full-1">{{ $students['firstname'] }} {{ $students['lastname'] }}</td>
                                            <td class="text-center b-full-1 p-full-1">
                                                @if(isset($students['customfields']))
                                                    
                                                    @foreach($students['customfields'] as $custom)

                                                        @if($custom['shortname'] == 'registry')

                                                            {{ $custom['value'] }}

                                                        @endif

                                                    @endforeach
                                                    
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                            <td class="text-center b-full-1 p-full-1">{{ ToolsGeneral::mask($students['username'],'###.###.###-##') }}</td>
                                            <td class="p-none b-full-1">
                                                <table class="no-border w-100-percent">
                                                    <tr class="no-border">
                                                        @foreach($course['dates']['dateFull'][$page] as $key => $dates)
                                                            <td width="20%" @if($key < (count($course['dates']['dateFull'][$page] ) -1)) class="b-right-1" @endif>&nbsp;</td>
                                                        @endforeach
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if( $page < (count($course['dates']['dateFull'])-1) ) 
        <a href="{{ route('simoo.turmas.frequency', ['id' => 9, 'numberPage' => count($course['dates']['dateFull'])]) }}" class="btn btn-primary btn-lg btn-page">Próximo</a>
    @elseif( $page == (count($course['dates']['dateFull'])-1) )
        <a href="{{ route('simoo.turmas.frequency', ['id' => 9, 'numberPage' => 1]) }}" class="btn btn-primary btn-lg btn-page">Voltar</a>
    @endif

@endsection

