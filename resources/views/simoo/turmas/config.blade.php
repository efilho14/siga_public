@extends('main')

@section('title', 'Turmas - Configurações')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Configurações</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Padrão para as turmas</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    {!! Form::model($config, ['route' => ['simoo.turmas.config.update',$config->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        {{ Form::label('frequency','Frequência mínima (%) dos participantes', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('frequency',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'frequency'], $config->frequency) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('grade','Nota mínima dos participantes', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {{ Form::text('grade', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'grade'], $config->grade) }}
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            {{ Form::submit('Editar Configurações', ['class' => 'btn btn-success btn-lg']) }}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- Switchery -->
    {{ Html::script("gentelella/vendors/switchery/dist/switchery.min.js") }}
@endsection
