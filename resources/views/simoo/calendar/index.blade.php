@extends('main')

@section('title', 'Calendário')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}

    <!-- FullCalendar -->
    {!! Html::style('gentelella/vendors/fullcalendar/dist/fullcalendar.min.css') !!}
    {!! Html::style('gentelella/vendors/fullcalendar/dist/fullcalendar.print.css', array('media' => 'print')) !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Calendário</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- FullCalendar -->
    {{ Html::script("gentelella/vendors/moment/min/moment.min.js") }}
    {{ Html::script("gentelella/vendors/fullcalendar/dist/fullcalendar.min.js") }}
    {{ Html::script("gentelella/vendors/fullcalendar/dist/locale-all.js") }}

    <script>
        $(document).ready(function() {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listMonth'
                },
                locale: 'pt-br',
                events: [
                    {
                    title: 'All Day Event',
                    start: '2018-03-01'
                    },
                    {
                    title: 'Long Event',
                    start: '2018-03-07',
                    end: '2018-03-10'
                    },
                    {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2018-03-09T16:00:00'
                    },
                    {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2018-03-16T16:00:00'
                    },
                    {
                    title: 'Conference',
                    start: '2018-03-11',
                    end: '2018-03-13'
                    },
                    {
                    title: 'Meeting',
                    start: '2018-03-12T10:30:00',
                    end: '2018-03-12T12:30:00'
                    },
                    {
                    title: 'Lunch',
                    start: '2018-03-12T12:00:00'
                    },
                    {
                    title: 'Meeting',
                    start: '2018-03-12T14:30:00'
                    },
                    {
                    title: 'Happy Hour',
                    start: '2018-03-12T17:30:00'
                    },
                    {
                    title: 'Dinner',
                    start: '2018-03-12T20:00:00'
                    },
                    {
                    title: 'Birthday Party',
                    start: '2018-03-13T07:00:00'
                    },
                    {
                    title: 'Click for Google',
                    url: 'http://google.com/',
                    start: '2018-03-28'
                    }
                ]
            });
        });
    </script>
@endsection