@extends('main')

@section('title', 'Usuários - Criar Usuário')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Criar Usuário</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Edite abaixo as informações:</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    
                    {!! Form::open(['route' => 'simoo.usuarios.store', 'data-parsley-validate' => '', 'class' => 'form-horizontal']) !!}

                        @if(count($errors) > 0)
                        <div class = "alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <br />

                        <div class="form-group">
                            {{ Form::label('firstname','Nome', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('firstname',  null, ['class' => 'form-control col-md-7 col-xs-12 text-uppercase', 'id' => 'firstname'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('lastname','Sobrenome', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('lastname',  null, ['class' => 'form-control col-md-7 col-xs-12 text-uppercase', 'id' => 'lastname'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('username','CPF', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('username',  null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'username', 'data-inputmask' => "'mask': '999.999.999-99'"] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('registry','Matrícula', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('registry',  null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'registry'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('email','E-mail', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('email',  null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'email'] ) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('cellphone','Celular', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('cellphone',  null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'cellphone', 'data-inputmask' => "'mask': '99 9 9999-9999'", 'placeholder' => '81 9 9999-9999'] ) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('public','Público', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="public" name="public">
                                    <option value="">Selecione uma opção:</option>
                                    @foreach($publics as $public)
                                        <option value="{{ $public->name }}">{{ $public->name }}</option>
                                    @endforeach
                                </select>
                            </div>                            
                        </div>

                        <div class="form-group">
                            {{ Form::label('group','Grupo', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="group" name="group">
                                    <option class="loading_group" value="">Aguardando o público...</option>
                                </select>
                            </div>                            
                        </div>

                        <div class="form-group">
                            {{ Form::label('type','Tipo', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="type" name="type">
                                    <option class="loading_type" value="">Aguardando o grupo...</option>
                                </select>
                            </div>                            
                        </div>

                        <div class="form-group">
                            {{ Form::label('password','Senha', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('password',  null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'password'] ) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success btn-lg">Criar Usuário</button>
                                <a href="{{ route('simoo.usuarios.index') }}" class="btn btn-primary btn-lg">Cancelar</a>
                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        //variavel global
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        //usuarios
        let url_user_public = "{!! route('simoo.usuarios.target.public.getName') !!}";
        let url_user_group = "{!! route('simoo.usuarios.target.group.getName') !!}";
    </script>

    <!-- jquery.inputmask -->
    {{ Html::script("gentelella/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js") }}
    <!-- usuarios -->
    {{ Html::script('js/simoo/usuarios/usuarios_global.js') }}
@endsection
