@extends('main')

@section('title', 'Evento - Todos os Usuários')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Todos os Usuários</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Pesquisar Usuário...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Buscar</button>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                  <h2>Lista dos Usuários</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(count($users)>0)
                        <table id="datatable" class="table table-striped">
                          <thead>
                            <tr>
                                <th>Nome</th>
                                <th>CPF</th>
                                @if(Gate::check('user.edit') OR Gate::check('user.delete'))
                                    <th>Opções</th>
                                @endif
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td class="user-name"><a href="{{ route('simoo.usuarios.show', $user->id) }}">{{ $user->firstname }} {{ $user->lastname }}</a></td>
                                <td>{{ ToolsGeneral::mask($user['username'],'###.###.###-##') }}</td>
                                @if(Gate::check('user.edit') OR Gate::check('user.delete'))
                                    <td>
                                        @can('user.edit')
                                            <a href="{{ route('simoo.usuarios.edit', $user->id) }}" class="btn btn-primary btn-sm">Editar</a>
                                        @endcan

                                        @can('user.delete')
                                            <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="{{ $user->id }}" data-toggle="modal" data-target=".modal-delete">Excluir</a>
                                        @endcan
                                    </td>
                                @endif
                            </tr>
                            @endforeach
                          </tbody>
                        </table>

                        <div class="pagination-custom">
                            {{ $users->links() }}
                        </div>

                    @else
                        <p>Não existem usuários cadastrados.</p>
                        <a href="{{ route('simoo.usuarios.create') }}" class="btn btn-primary">Inserir Novo Usuário</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Small modal -->
    <div class="modal fade modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Usuário</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir o usuário <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este usuário.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let url = '{{ url()->full() }}/';
    </script>
    <!-- Custom -->
    {{ Html::script("js/simoo/usuarios/usuarios.js") }}
@endsection