@extends('main')

@section('title', 'Evento - Todos os Usuários')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Visualizar Usuário</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                  <h2>Detalhes</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        <div class="profile_img">
                            <div id="crop-avatar">
                            
                            <img class="img-responsive avatar-view" src="{{ $users['profileimageurl'] }}" alt="Avatar" title="Change the avatar">
                            </div>
                        </div>
                        <h3>{{ $users['firstname'] }} {{ $users['lastname'] }}</h3>

                        <ul class="list-unstyled user_data">
                            <li><strong style="display:block">CPF </strong> {{ ToolsGeneral::mask($users['username'],'###.###.###-##') }}</li>

                            @foreach($users['customfields'] as $customfields)
                                <li><strong style="display:block">{{ $customfields['name'] }} </strong> {{ $customfields['value'] }}</li>
                            @endforeach
                        </ul>

                        @can('user.edit')
                            <a href="{{ route('simoo.usuarios.edit',$users['id']) }}" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i> Editar</a>
                        @endcan

                    </div>


                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab1" role="tab" data-toggle="tab" aria-expanded="true">Aluno ({{ count($users['courses']['student']) }})</a></li>
                                <li role="presentation"><a href="#tab_content2" id="home-tab2" role="tab" data-toggle="tab" aria-expanded="true">Instrutor ({{ count($users['courses']['teacher']) }})</a></li>
                                <li role="presentation"><a href="#tab_content3" id="home-tab3" role="tab" data-toggle="tab" aria-expanded="true">Coordenador ({{ ( count($users['courses']['editingteacherone']) + count($users['courses']['editingteachertwo']) ) }})</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="profile-tab">
                                    <!-- start user projects -->
                                    @if($users['courses']['student'])
                                        <table class="data table table-striped no-margin">
                                            <thead>
                                                <tr>
                                                <th>#</th>
                                                <th>Curso</th>
                                                <th>Período</th>
                                                <th>Carga Horária</th>
                                                <th>Situação</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users['courses']['student'] as $key => $course)
                                                    <tr>
                                                        <td>{{ $course['courseid'] }}</td>
                                                        <td><a target="_blank" href="{{ route('simoo.turmas.show',$course['courseid']) }}">{{ $course['courseName'] }}</a></td>
                                                        <td>
                                                            @foreach($course['period']['dateParcial'] as $key => $parcial)
                                                                <span style="display:block; line-height: 18px;">{{ $parcial }}</span>
                                                            @endforeach
                                                        </td>
                                                        <td>{{ $course['workload'] }}</td>
                                                        <td>{{ $course['situation'] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <p class="box-tab-user">Não existem registros deste usuário como aluno.</p>
                                    @endif
                                    <!-- end user projects -->
                                </div>

                                <div role="tabpanel" class="tab-pane fade in" id="tab_content2" aria-labelledby="profile-tab">
                                    <!-- start user projects -->
                                    @if($users['courses']['editingteacherone'])
                                        <table class="data table table-striped no-margin">
                                            <thead>
                                                <tr>
                                                <th>#</th>
                                                <th>Curso</th>
                                                <th>Período</th>
                                                <th>Tipo</th>
                                                <th>Carga Horária</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- Instrutor Titular -->
                                                @foreach($users['courses']['editingteacherone'] as $key => $course)
                                                    <tr>
                                                        <td>{{ $course['courseid'] }}</td>
                                                        <td><a target="_blank" href="{{ route('simoo.turmas.show',$course['courseid']) }}">{{ $course['courseName'] }}</a></td>
                                                        <td>
                                                            @foreach($course['period']['dateParcial'] as $key => $parcial)
                                                                <span style="display:block; line-height: 18px;">{{ $parcial }}</span>
                                                            @endforeach
                                                        </td>
                                                        <td>Instrutor Titular</td>
                                                        <td>{{ $course['workload'] }}</td>
                                                    </tr>
                                                @endforeach

                                                <!-- Instrutor Secundário -->
                                                @foreach($users['courses']['editingteachertwo'] as $key => $course)
                                                    <tr>
                                                        <td>{{ $course['courseid'] }}</td>
                                                        <td><a target="_blank" href="{{ route('simoo.turmas.show',$course['courseid']) }}">{{ $course['courseName'] }}</a></td>
                                                        <td>
                                                            @foreach($course['period']['dateParcial'] as $key => $parcial)
                                                                <span style="display:block; line-height: 18px;">{{ $parcial }}</span>
                                                            @endforeach
                                                        </td>
                                                        <td>Instrutor Secundário</td>
                                                        <td>{{ $course['workload'] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <p class="box-tab-user">Não existem registros deste usuário como instrutor.</p>
                                    @endif
                                    <!-- end user projects -->
                                </div>

                                <div role="tabpanel" class="tab-pane fade in" id="tab_content3" aria-labelledby="profile-tab">
                                    <!-- start user projects -->
                                    @if($users['courses']['teacher'])
                                        <table class="data table table-striped no-margin">
                                            <thead>
                                                <tr>
                                                <th>#</th>
                                                <th>Curso</th>
                                                <th>Período</th>
                                                <th>Carga Horária</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users['courses']['teacher'] as $key => $course)
                                                    <tr>
                                                        <td>{{ $course['courseid'] }}</td>
                                                        <td><a target="_blank" href="{{ route('simoo.turmas.show',$course['courseid']) }}">{{ $course['courseName'] }}</a></td>
                                                        <td>
                                                            @foreach($course['period']['dateParcial'] as $key => $parcial)
                                                                <span style="display:block; line-height: 18px;">{{ $parcial }}</span>
                                                            @endforeach
                                                        </td>
                                                        <td>{{ $course['workload'] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <p class="box-tab-user">Não existem registros deste usuário como coordenador.</p>
                                    @endif
                                    <!-- end user projects -->
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    
@endsection