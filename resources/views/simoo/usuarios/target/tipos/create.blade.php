@extends('main')

@section('title', 'Usuários - Inserir Novo Tipo')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
    <!-- Switchery -->
    {!! Html::style('gentelella/vendors/switchery/dist/switchery.min.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Inserir Novo Tipo</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tipo</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    {!! Form::open(['route' => 'simoo.usuarios.target.type.store', 'data-parsley-validate', 'class' => 'form-horizontal']) !!}

                        @if(count($errors) > 0)
                            <div class = "alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <br />

                        <div class="form-group">
                            {{ Form::label('groupid','Grupo', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="groupid" required='required'>
                                    @foreach($groups as $group)
                                        <option value="{{ $group['id'] }}">{{ $group['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('name','Nome', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'name']) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success btn-lg">Criar Novo Tipo</button>
                                <a href="{{ route('simoo.usuarios.target.index') }}" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- Switchery -->
    {{ Html::script("gentelella/vendors/switchery/dist/switchery.min.js") }}
@endsection
