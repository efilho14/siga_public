@extends('main')

@section('title', 'Público-alvo')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Todos os Públicos-alvo</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Lista dos Públicos-alvo</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(count($target) > 0)
                        <table id="datatable" class="table table-bordered jambo_table table-hover">
                            <thead>
                                <tr>
                                    <th>Público</th>
                                    @if(Gate::check('userTargetPublic.edit') OR Gate::check('userTargetPublic.delete') OR
                                        Gate::check('userTargetGroup.edit') OR Gate::check('userTargetGroup.delete') OR
                                        Gate::check('userTargetType.edit') OR Gate::check('userTargetType.delete'))

                                        <th>Opções</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($target as $namePublic => $public)
                                    <tr>
                                        <td class="public-name" style="padding-left:10px">{{ $namePublic }}</td>

                                            @if(Gate::check('userTargetPublic.edit') OR Gate::check('userTargetPublic.delete') OR
                                                Gate::check('userTargetGroup.edit') OR Gate::check('userTargetGroup.delete') OR
                                                Gate::check('userTargetType.edit') OR Gate::check('userTargetType.delete'))
                                            <td>
                                                @can('userTargetPublic.edit') <a href="{{ route('simoo.usuarios.target.public.edit',$target[$namePublic]['info']['id']) }}" class="btn btn-primary btn-sm">Editar</a> @endcan
                                                @can('userTargetPublic.delete') <a href="#" class="btn btn-danger btn-sm btn-delete-public" data-id="{{ $target[$namePublic]['info']['id'] }}" data-toggle="modal" data-target=".modal-delete-1">Excluir</a> @endcan
                                            </td>
                                        @endif
                                    </tr>

                                    @foreach($public['groups'] as $nameGroup => $group)
                                        <tr>
                                            <td class="group-name" style="padding-left:25px">{{ $nameGroup }}</td>

                                            @if(Gate::check('userTargetPublic.edit') OR Gate::check('userTargetPublic.delete') OR
                                                    Gate::check('userTargetGroup.edit') OR Gate::check('userTargetGroup.delete') OR
                                                    Gate::check('userTargetType.edit') OR Gate::check('userTargetType.delete'))
                                                <td>
                                                    @can('userTargetGroup.edit') <a href="{{ route('simoo.usuarios.target.group.edit',$public['groups'][$nameGroup]['info']['id']) }}" class="btn btn-primary btn-sm">Editar</a> @endcan
                                                    @can('userTargetGroup.delete') <a href="#" class="btn btn-danger btn-sm btn-delete-group" data-id="{{ $public['groups'][$nameGroup]['info']['id'] }}" data-toggle="modal" data-target=".modal-delete-2">Excluir</a> @endcan
                                                </td>
                                            @endif
                                        </tr>

                                        @foreach($group['types'] as $nameType => $type)

                                            <tr>
                                                <td class="type-name" style="padding-left:45px">{{ $nameType }}</td>

                                                @if(Gate::check('userTargetPublic.edit') OR Gate::check('userTargetPublic.delete') OR
                                                    Gate::check('userTargetGroup.edit') OR Gate::check('userTargetGroup.delete') OR
                                                    Gate::check('userTargetType.edit') OR Gate::check('userTargetType.delete'))
                                                    <td>
                                                        @can('userTargetType.edit') <a href="{{ route('simoo.usuarios.target.type.edit',$group['types'][$nameType]['info']['id']) }}" class="btn btn-primary btn-sm">Editar</a> @endcan
                                                        @can('userTargetType.delete') <a href="#" class="btn btn-danger btn-sm btn-delete-type" data-id="{{ $group['types'][$nameType]['info']['id'] }}" data-toggle="modal" data-target=".modal-delete-3">Excluir</a> @endcan
                                                    </td>
                                                @endif
                                            </tr>

                                        @endforeach

                                    @endforeach

                                @endforeach
                            </tbody>
                        </table>

                        @can('userTargetPublic.create')<a href="{{ route('simoo.usuarios.target.public.create') }}" class="btn btn-primary">Novo Público</a> @endcan
                        @can('userTargetGroup.create')<a href="{{ route('simoo.usuarios.target.group.create') }}" class="btn btn-success">Novo Grupo</a> @endcan
                        @can('userTargetType.create')<a href="{{ route('simoo.usuarios.target.type.create') }}" class="btn btn-info">Novo Tipo</a> @endcan
                    @else
                        <p>Não existem públicos cadastrados.</p>
                        <a href="{{ route('simoo.usuarios.target.public.create') }}" class="btn btn-primary">Inserir Novo Público</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- público - modal -->
    <div class="modal fade modal-delete-1" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Público</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir o público <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este público.">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- grupo - modal -->
    <div class="modal fade modal-delete-2" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Grupo</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir o grupo <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este grupo.">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- público - modal -->
    <div class="modal fade modal-delete-3" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Tipo</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir o tipo <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este tipo.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let urlPublic = '{{ url()->full() }}/public/';
        let urlGroup = '{{ url()->full() }}/group/';
        let urlType = '{{ url()->full() }}/type/';
    </script>
    <!-- Custom -->
    {{ Html::script("js/simoo/usuarios/publico_alvo.js") }}
@endsection