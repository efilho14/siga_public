@extends('main')

@section('title', 'Todos os Ambientes')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Todos os Ambientes</h3>
        </div>

        @if($environment)
        <div class="title_right">
            <div class="col-md-2 col-sm-2 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <a href="{{ route('simoo.ambientes.create') }}" class="btn btn-primary">Inserir Novo Ambiente</a>
                </div>
            </div>
        </div>
        @endif
    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Lista dos Ambientes</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                
                @if($environment)
                    
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        
                        @foreach($environment as $keyEnvironments => $environments)
                            <div class="panel">
                                <a class="panel-heading" role="tab" id="headingOne{{ $keyEnvironments }}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{ $keyEnvironments }}" aria-expanded="false">
                                    <h4 class="panel-title">{{ $environments['environmentName'] }}</h4>
                                </a>
                                <div id="collapseOne{{ $keyEnvironments }}" class="panel-collapse collapse {{ ( session('id') == $environments['environmentId'] ) ? 'in' : '' }}" role="tabpanel">
                                    <div class="panel-body">
                                        @if(!$environment)
                                            <p>Não existem ambientes cadastrados para este ambiente!</p>
                                        @endif
                                        
                                        @if($environments['rooms'])
                                            <table class="table table-striped table-last-bottom">
                                                <thead>
                                                <tr>
                                                    <th width="80%">Nome</th>
                                                    <th>Opções</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($environments['rooms'] as $keyRoom => $room)
                                                        <tr>
                                                            <td>{{ $room['roomName'] }}</td>
                                                            <td>
                                                                <a href="{{ route('simoo.salas.edit',$room['roomId']) }}" class="btn btn-primary btn-sm">Editar</a>
                                                                <a href="#" class="btn btn-danger btn-sm btn-delete-sala" data-id-sala="{{ $room['roomId'] }}" data-toggle="modal" data-target=".modal-delete-sala">Excluir</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>Não existem salas cadastradas!</p>
                                        @endif

                                        <a href="{{ route('simoo.salas.create',$environments['environmentId']) }}" class="btn btn-success">Inserir Nova Sala</a>

                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button" aria-expanded="false">Ambiente <span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="{{ route('simoo.ambientes.edit',$environments['environmentId']) }}">Editar</a></li>
                                                <li><a href="#" class="btn-delete-ambiente" data-toggle="modal" data-target=".modal-delete-ambiente" data-id-ambiente="{{ $environments['environmentId'] }}">Excluir</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                @else
                    <p>Não existem ambientes cadastrados.</p>
                    <a href="{{ route('simoo.ambientes.create') }}" class="btn btn-primary">Inserir Novo Ambiente</a>
                @endif

                </div>
            </div>
        </div>
        
    </div>

    <!-- modal delete ambiente -->
    <div class="modal fade modal-delete-ambiente" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Ambiente</h4>
                </div>
                <div class="modal-body" style="text-align:center">
                    <p>Você tem certeza que deseja excluir este ambiente?</p>
                    <p class="alert-color"><strong>Lembre-se que ao excluí-lo, as salas ao qual ela pertence, também serão excluídas.</strong></p>
                </div>
                <div class="modal-footer">
                    <form class="modal-action-ambiente" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este ambiente.">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- modal delete sala -->
    <div class="modal fade modal-delete-sala" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Sala</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir está sala?</div>
                <div class="modal-footer">
                    <form class="modal-action-sala" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir está sala.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let urlAmbiente = "{{ url('simoo/ambientes') }}/";
        let urlSala = "{{ url('simoo/salas') }}/";
    </script>
    <!-- Custom -->
    {{ Html::script("js/simoo/ambientes.js") }}
@endsection