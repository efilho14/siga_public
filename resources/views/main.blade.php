<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials._head')
        @yield('stylesheets')
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                @include('partials._nav')

                <div class="right_col" role="main">
                    @yield('content')
                </div>

                @include('partials._footer')
            </div>
        </div>

        @include('partials._javascript')
        @yield('scripts')

        <!-- Custom Theme Scripts -->
        {!! Html::script("gentelella/build/js/custom.min.js") !!}

        <script>
            $(document).ready(function(){
                $('.ui-pnotify').remove();
            });
        </script>

        {!! Notify::render() !!}
    </body>
</html>
