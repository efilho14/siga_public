<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials._head_plain')
        @yield('stylesheets')
    </head>

    <body class="login">
        <div class="container body">
            <div class="main_container">
                @yield('content')
            </div>
        </div>

        @include('partials._javascript')
        @yield('scripts')
    </body>
</html>
