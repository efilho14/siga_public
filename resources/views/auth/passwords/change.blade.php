@extends('main')

@section('title', 'Alterar Senha')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Alterar a Senha</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Preencha abaixo as informações:</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <br />
                    <form class="form-horizontal" method="POST" action="{{ route('change_password') }}">
                        @csrf
                        <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                            <label for="new-password" class="col-md-4 control-label">Senha Atual</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="current-password" type="password" class="form-control" name="current-password" required>

                                @if ($errors->has('current-password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('current-password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                            <label for="new-password" class="col-md-4 control-label">Nova Senha</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="new-password" type="password" class="form-control" name="new-password" required>

                                @if ($errors->has('new-password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('new-password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                            <label for="new-password-confirm" class="col-md-4 control-label">Confirmar Nova Senha</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success btn-lg">Editar Senha</button>
                                <a href="/" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

