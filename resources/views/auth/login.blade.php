@extends('auth')

@section('title', 'Login')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="form login_form">
        <section class="login_content">
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}"><form>
                @csrf
                <h1>{{ HTML::image('images/logo-siga.png', 'Sistema de Gerenciamento Acadêmico', array('style' => 'margin-top:-20px')) }}</h1>
                <div>
                    @if ($errors->has('username'))
                        <span class="invalid-feedback red" role="alert">
                            <strong>{{ $errors->first('username') }}.</strong>
                        </span>
                    @endif
                    <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Nome de Usuário" name="username" value="{{ old('username') }}" required autofocus>
                </div>
                <div>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Senha" name="password" required>
                </div>
                <div>
                    <button type="submit" class="btn btn-default submit">Logar</button>
                    <a class="reset_pass" href="{{ route('password.request') }}">Esqueceu a senha?</a>  
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    @if(count($user)==0)
                        <p class="change_link"><a href="{{ route('register') }}">Criar Novo Registro</a></p>
                    @endif

                    <div class="clearfix"></div>
                    <br />

                    <div>
                        <p>Portal Esafaz © {{ date('Y') }}. Todos os direitos reservados.</p>
                    </div>
                </div>
            </form>
        </section>
    </div>

@endsection