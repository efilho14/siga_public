@extends('auth')

@section('title', 'Novo Registro')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="form">
        <section class="login_content">
            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                @csrf
                <h1>{{ HTML::image('images/logo-siga.png', 'Sistema de Gerenciamento Acadêmico', array('style' => 'margin-top:-20px')) }}</h1>
                <div>
                    <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Nome de Usuário" name="username" value="admin" required>

                    @if ($errors->has('username'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Nome" name="name" value="Administrator" required>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Senha" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                    <input id="password-confirm" type="password" class="form-control" placeholder="Confirmar Senha" name="password_confirmation" required>
                </div>
                <div>
                    <button type="submit" class="btn btn-default">Registrar</button>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link"><a href="{{ route('login') }}" class="to_register">Login</a></p>

                    <div class="clearfix"></div>
                    <br />

                    <div>
                        <p>Portal Esafaz © {{ date('Y') }}. Todos os direitos reservados.</p>
                    </div>
                </div>
            </form>
        </section>
    </div>

@endsection