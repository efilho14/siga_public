<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Siga | @yield('title')</title>

<!-- Font Awesome -->
{!! Html::style("gentelella/vendors/font-awesome/css/font-awesome.min.css") !!}

<!--[if lt IE 9]>
    {!! Html::script("gentelella/assets/js/ie8-responsive-file-warning.js") !!}
<![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js") !!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js") !!}
<![endif]-->
