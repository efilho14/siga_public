<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0; margin-top:15px">
			<span class="site_title">{{ HTML::image('images/logo-siga-white.png', 'Sistema de Gerenciamento Acadêmico', array('style' => 'margin-top:-20px')) }}</span>
		</div>

		<div class="clearfix"></div>

		<br>

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

			@can('moduleSystem.simoo')
			<div class="menu_section">
				<h3>SIMOO - PRESENCIAL</h3>
				<ul class="nav side-menu" style="">
					@can('dashboard.index')
						<li>
							<a><i class="fa fa-bar-chart"></i> Dashboard</a>
						</li>
					@endcan

					<li>
						<a><i class="fa fa-folder-open"></i> Turmas <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							@can('course.index')
								<li><a href="{{ route('simoo.turmas.index') }}">Listar Todas as Turmas</a></li>
							@endcan

							@can('course.create')
								<li><a href="{{ route('simoo.turmas.create') }}">Inserir Nova Turma</a></li>
							@endcan

							@can('course.config.index')
								<li><a href="{{ route('simoo.turmas.config') }}">Configurações</a></li>
							@endcan
						</ul>
					</li>

					<li>
						<a><i class="fa fa-server"></i> Eventos <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							@can('courseCategory.index')
								<li><a href="{{ route('simoo.eventos.index') }}">Listar Todos os Eventos</a></li>
							@endcan

							@can('courseCategory.create')
								<li><a href="{{ route('simoo.eventos.create') }}">Inserir Novo Evento</a></li>
							@endcan
						</ul>
					</li>

					<li>
						<a><i class="fa fa-users"></i> Usuários <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							@can('user.index')
								<li><a href="{{ route('simoo.usuarios.index') }}">Listar Todos os Usuários</a></li>
							@endcan
							
							@can('user.create')
								<li><a href="{{ route('simoo.usuarios.create') }}">Inserir Novo Usuário</a></li>
							@endcan
							
							@can('userTarget.index')
								<li><a href="{{ route('simoo.usuarios.target.index') }}">Gerenciar Público-alvo</a></li>
							@endcan
						</ul>
					</li>

					<li>
						<a><i class="fa fa-table"></i> Calendário <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							@can('calendar.index')
								<li><a href="{{ route('simoo.calendario.index') }}">Mostrar o calendário</a></li>
							@endcan
						</ul>
					</li>

					@if( Gate::check('environment.index') )
						<li>
							<a><i class="fa fa-building"></i> Ambientes <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								@can('environment.index')
									<li><a href="{{ route('simoo.ambientes.index') }}">Listar Todos os Ambientes</a></li>
								@endcan
							</ul>
						</li>
					@endif

					@if( Gate::check('relatory.index') )
						<li>
							<a><i class="fa fa-pie-chart"></i> Relatórios <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								@can('relatory.index')
									<li><a href="#">Partipação por aluno</a></li>
								@endcan
							</ul>
						</li>
					@endif
					
				</ul>
			</div>
			@endcan

			@can('moduleSystem.admin')
			<div class="menu_section">
				<h3>ADMINISTRAÇÃO</h3>
				<ul class="nav side-menu" style="">
					<li>
						<a><i class="fa fa-cog"></i> Configurações <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							@if( Gate::check('user.index') OR Gate::check('role.index') )
								<li>
									<a>Usuários<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										@can('user.index')
											<li><a href="{{ route('users.index') }}">Lista de Usuários</a></li>
										@endcan

										@can('role.index')
											<li><a href="{{ route('roles.index') }}">Perfil de Usuário</a></li>
										@endcan
									</ul>
								</li>
							@endif

							@if( Gate::check('module.index') )
								<li>
									<a>Módulos<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										@can('module.index')
											<li><a href="{{ route('modules.index') }}">Lista de Módulos</a></li>
										@endcan
									</ul>
								</li>
							@endif

							@if( Gate::check('permission.index') )
								<li>
									<a>Permissão<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										@can('permission.index')
											<li><a href="{{ route('permissions.index') }}">Lista de permissões</a></li>
										@endcan
									</ul>
								</li>
							@endif
						</ul>						
					</li>
				</ul>
			</div>
			@endcan

		</div>

	</div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ route('change_password') }}">Alterar a senha</a></li>
                        <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out pull-right"></i> Sair</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
