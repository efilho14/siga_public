<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials._head')
        @yield('stylesheets')
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @yield('content')

            </div>
        </div>

        @include('partials._javascript')
        @yield('scripts')

        <!-- Custom Theme Scripts -->
        {!! Html::script("gentelella/build/js/custom.min.js") !!}
    </body>
</html>
