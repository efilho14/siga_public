@extends('main')

@section('title', 'Editar Perfil de Usuário')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Editar Perfil de Usuário</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Perfil de Usuário</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    {!! Form::model($role, ['route' => ['roles.update',$role->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            {{ Form::label('name','Nome', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('name',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'name'], $role->name) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('label','Descrição', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('label',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'label'], $role->label) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('modules','Módulo', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="moduleid" id="module" required='required'>
                                    @foreach($module as $modules)
                                        <option {{ ($modules->id == $role['moduleid']) ? "selected='selected'" : '' }} value="{{ $modules->id }}">{{ $modules->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{ Form::submit('Editar Perfil', ['class' => 'btn btn-success btn-lg']) }}
                                <a href="{{ route('roles.index') }}" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
