@extends('main')

@section('title', 'Usuários do Sistema')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Todos os Usuários</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            @foreach($user as $module => $modules)

                <div class="x_panel">
                    <div class="x_title">
                    <h2>{{ $module }}</h2>
                    <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if(count($user)>0)
                            
                            <table id="datatable" class="table table-striped table-last-bottom">
                            <thead>
                                <tr>
                                <th style="width:20%">Nome</th>
                                <th>Nome de Usuário</th>
                                <th>Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($modules as $id => $name)
                                    
                                    <tr>
                                        <td class="name">{{ $name['name'] }}</td>
                                        <td>{{ $name['username'] }}</td>
                                        <td>
                                            <a href="{{ route('users.edit', $id) }}" class="btn btn-primary btn-sm">Editar</a>
                                            <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="{{ $id }}" data-toggle="modal" data-target=".modal-delete">Excluir</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                        @else
                            <p>Não existem usuários cadastrados.</p>
                            <a href="{{ route('users.create') }}" class="btn btn-primary">Inserir Usuário</a>
                        @endif
                    </div>
                    
                </div>

            @endforeach

            @if(count($user)>0)
            <a href="{{ route('users.create') }}" class="btn btn-primary">Inserir Usuário</a>
            @endif
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Usuário</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir o usuário <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este usuário.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let url = '{{ url()->full() }}/';
    </script>
    <!-- Custom -->
    {{ Html::script("js/siga/ambiente.js") }}
@endsection