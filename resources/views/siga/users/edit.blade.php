@extends('main')

@section('title', 'Editar Usuário')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Editar Usuário</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Usuário</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    @if(count($errors) > 0)

                    <div class="alert alert-danger" role="alert">
                        <strong>Errors:</strong>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }} </li>
                        @endforeach
                        </ul>
                    </div>

                    @endif

                    {!! Form::model($user, ['route' => ['users.update',$user->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            {{ Form::label('name','Nome', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('name',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'name'], $user->name) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('username','Nome de Usuário', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('username',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'username'], $user->username) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('email','Email', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('email',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'email'], $user->email) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('roles','Perfil', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="roleid" id="role" required='required'>
                                    <option>Escolha uma opção:</option>
                                    @foreach($role as $roles)
                                        <option {{ ($roles->id == $user['roleid']) ? "selected='selected'" : '' }} value="{{ $roles->id }}">{{ $roles->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('password','Senha', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::password('password', ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'password']) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{ Form::submit('Editar Usuário', ['class' => 'btn btn-success btn-lg']) }}
                                <a href="{{ route('users.index') }}" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
