@extends('main')

@section('title', 'Inserir Usuário')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Inserir Usuário</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Usuário</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    {!! Form::open(['route' => 'users.store', 'data-parsley-validate', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            {{ Form::label('name','Nome', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'name', 'required']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('username','Nome de Usuário', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('username', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'username', 'required']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('email','Email', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('email', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'email', 'required']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('roleid','Perfil', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="roleid" id="role" required='required'>
                                    <option>Escolha uma opção:</option>
                                    @foreach($role as $roles)
                                        <option value="{{ $roles->id }}">{{ $roles->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('password','Senha', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::password('password', ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'password', 'required']) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success btn-lg">Criar Usuário</button>
                                <a href="{{ route('users.index') }}" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
