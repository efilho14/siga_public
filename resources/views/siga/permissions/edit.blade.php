@extends('main')

@section('title', 'Editar Perfil de Usuário')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Editar Perfil de Usuário</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Perfil de Usuário</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    {!! Form::model($permission, ['route' => ['permissions.update',$permission->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            {{ Form::label('name','Nome', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('name',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'name'], $permission->name) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('label','Descrição', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('label',null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'label'], $permission->label) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('role','Perfil', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="roleid" id="role" required='required'>
                                    @foreach($role as $roles)
                                        <option {{ ($roles->id == $permission['roleid']) ? "selected='selected'" : '' }} value="{{ $roles->id }}">{{ $roles->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{ Form::submit('Editar Permissão', ['class' => 'btn btn-success btn-lg']) }}
                                <a href="{{ route('permissions.index') }}" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
