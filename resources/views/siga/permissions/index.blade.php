@extends('main')

@section('title', 'Permissões')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Permissões</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @foreach($permission as $module => $permissions)

                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ $module }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        @foreach($permissions as $role => $list)
                            <h6><strong>{{ $role }}</strong></h6>
                        
                            <table class="table table-bordered">
                                @foreach($list as $name)
                                <tr>
                                    <td style="width:30%">{{ $name['name'] }}</td>
                                    <td>{{ $name['label'] }}</td>
                                </tr>
                                @endforeach
                            </table>

                        @endforeach

                    </div>
                    
                </div>

            @endforeach

            @if(count($permission)>0)
                <a href="{{ route('permissions.create') }}" class="btn btn-primary">Inserir Permissão</a>
            @endif
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Permissão</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir a permissão <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir esta permissão.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let url = '{{ url()->full() }}/';
    </script>
    <!-- Custom -->
    {{ Html::script("js/siga/ambiente.js") }}
@endsection