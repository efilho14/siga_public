@extends('main')

@section('title', 'Inserir Novo Módulo')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Inserir Módulo</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Módulo</h2>
                <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    {!! Form::open(['route' => 'modules.store', 'data-parsley-validate', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            {{ Form::label('name','Nome', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'name']) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success btn-lg">Criar Módulo</button>
                                <a href="{{ route('modules.index') }}" class="btn btn-primary btn-lg">Cancelar</a>

                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
