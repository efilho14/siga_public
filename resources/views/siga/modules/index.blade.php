@extends('main')

@section('title', 'Todos os Módulos')

@section('stylesheets')
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Todos os Módulos</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                  <h2>Listagem</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(count($module)>0)
                        <table id="datatable" class="table table-striped table-last-bottom">
                          <thead>
                            <tr>
                              <th>Nome</th>
                              <th>Opções</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($module as $modules)
                            <tr>
                              <td class="name">{{ $modules->name }}</td>
                              <td>
                                  <a href="{{ route('modules.edit',$modules->id) }}" class="btn btn-primary btn-sm">Editar</a>
                                  <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="{{ $modules->id }}" data-toggle="modal" data-target=".modal-delete">Excluir</a>
                               </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    @else
                        <p>Não existem módulos cadastrados.</p>
                        <a href="{{ route('modules.create') }}" class="btn btn-primary">Inserir Módulo</a>
                    @endif
                </div>
                @if(count($module)>0)
                <a href="{{ route('modules.create') }}" class="btn btn-primary">Inserir Módulo</a>
                @endif
            </div>
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Evento</h4>
                </div>
                <div class="modal-body" style="text-align:center">Você tem certeza que deseja excluir o módulo <br><strong>"<span class="modal-event"></span>"</strong>?</div>
                <div class="modal-footer">
                    <form class="modal-action" method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        {!! csrf_field() !!}
                        <input class="btn btn-danger btn-block" type="submit" value="Sim! Eu desejo excluir este módulo.">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let url = '{{ url()->full() }}/';
    </script>
    <!-- Custom -->
    {{ Html::script("js/siga/ambiente.js") }}
@endsection