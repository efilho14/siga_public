@extends('error')

@section('title', 'Erro - Página não encontrada')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Desculpe, mas não conseguimos encontrar esta página.</h2>
              <p>Esta página que você está procurando não existe.
              </p>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection