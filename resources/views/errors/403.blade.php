@extends('error')

@section('title', 'Erro - Acesso negado')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">403</h1>
              <h2>Acesso Negado</h2>
              <p>Sua autenticação não tem permissão para acessar está página.
              </p>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection