@extends('error')

@section('title', 'Erro - Acesso negado')

@section('stylesheets')
    <!-- style -->
    {!! Html::style('css/style.css') !!}
@endsection

@section('content')

    <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">500</h1>
              <h2>Erro interno no Servidor</h2>
              <p>A página não pode ser exibida, pois algum erro de execução no servidor ou em scripts do site. <br>Se o problema persistir, não hesite em nos contactar. <br>Enquanto isso, tente atualizar novamente.
              </p>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection