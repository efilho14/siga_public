<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials._head')
        @yield('stylesheets')
    </head>

    <body class="login">
        <div class="login_wrapper">

            @yield('content')

        </div>

        @include('partials._javascript')
        @yield('scripts')

        <!-- Custom Theme Scripts -->
        {!! Html::script("gentelella/build/js/custom.min.js") !!}
    </body>
</html>
