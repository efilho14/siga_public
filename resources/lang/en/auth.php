<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'O seu Nome de Usuário ou Senha estão incorretos',
    'throttle' => 'Você expirou o seu números de tentativas. Por favor, tente novamente em :seconds segundos.',

];
