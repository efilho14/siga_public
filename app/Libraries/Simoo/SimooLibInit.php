<?php

namespace App\Libraries\Simoo;

## Models
use App\Model\Moodle\MoodleCourseCategory;

## Libraries
use App\Libraries\Moodle\MoodleLibCourseCategories;

## Others
use Session;

class SimooLibInit
{
    public static function presential()
    {
        if(!Session::has('presential')):
            $course = MoodleCourseCategory::GetIdNumber('presencial');
            if(empty($course)):
                MoodleLibCourseCategories::create([
                    'name'              => 'CURSOS PRESENCIAIS',
                    'parent'            => 0,
                    'idnumber'          => 'presencial',
                    'description'       => '',
                    'descriptionformat' => 1
                ]);

                $course = MoodleCourseCategory::GetIdNumber('presencial');
            endif;

            session()->put('presential', $course->id);
        endif;
    }
}