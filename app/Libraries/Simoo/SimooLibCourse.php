<?php

namespace App\Libraries\Simoo;

## Models
use App\Model\Simoo\SimooCourseConfig;
use App\Model\Simoo\SimooCourse;
use App\Model\Simoo\SimooCourseDates;

## Helpers
use App\Helpers\Simoo\ToolsSimooCourse;
use App\Helpers\General\ToolsGeneral;

class SimooLibCourse
{
    public static function get_date(int $id)
    {
        ## block
        $block = 0;

        ## inicia array
        $period = [
            'dateFull' => ['0' => []],
            'hourFull' => ['0' => []],
            'dateParcial' => ['0' => []],
            'hourParcial' => ['0' => []],
        ];

        ## buscar na tabela 'course_dates'
        $dates = SimooCourseDates::select('courseid','date','hourinit','hourend','block')
                                  ->where('courseid', $id)->get();

        ## varrer os dados para o 'full'
        foreach($dates as $date):
            if($date->block != $block):
                $block = $date->block;
                $period['dateFull'] += [$date->block => []];
                $period['hourFull'] += [$date->block => []];
                $period['dateParcial'] += [$date->block => []];
                $period['hourParcial'] += [$date->block => []];

                //inserir date e hour
                array_push($period['dateFull'][$block], ToolsGeneral::convertDatePtToEn($date->date));
                array_push($period['hourFull'][$block], ($date->hourinit.' às '.$date->hourend));
            else:                
                //inserir date e hour
                array_push($period['dateFull'][$block], ToolsGeneral::convertDatePtToEn($date->date));
                array_push($period['hourFull'][$block], ($date->hourinit.' às '.$date->hourend));
            endif;
        endforeach;

        ## varrer os dados do date_full para o 'parcial'
        foreach($period['dateFull'] as $key => $dates):
            
            $unionDates = '';

            foreach($dates as $key => $value):
                if($key == 0 AND (count($dates) > 1)):
                    $unionDates .= $value . " a ";
                elseif($key == 0 AND (count($dates) == 1)):
                    $unionDates .= $value;
                elseif($key == (count($dates)-1)):
                    $unionDates .= $value;
                endif;
            endforeach;

            $period['dateParcial'][key($period['dateFull'])] = $unionDates;
            next($period['dateFull']);
        endforeach;
        
        ## varrer os dados do hour_full para o 'parcial'
        foreach($period['hourFull'] as $key => $dates):

            $unionHour = '';

            foreach($dates as $key => $value):
                if($key == 0 AND (count($dates) == 1)):
                    $unionHour .= $value;
                elseif($key == (count($dates)-1)):
                    $unionHour .= $value;
                endif;
            endforeach;

            $period['hourParcial'][key($period['hourFull'])] = $unionHour;
            next($period['hourFull']);
        endforeach;

        return $period;
    }

    public static function create($data, $dates, $hourInit, $hourEnd, $block)
    {
        ## inseri na tabela 'course'
        $courseSimooId = SimooCourse::insertGetId($data);

        ## inseri na tabela 'course_dates'
        $toolsDates = ToolsSimooCourse::Period($courseSimooId, $dates, $hourInit, $hourEnd, $block);
        SimooCourseDates::where('courseid', $courseSimooId)->insert($toolsDates);
    }

    public static function update($data, $id, $dates, $hourInit, $hourEnd, $block)
    {
        ## atualizar na tabela 'course'
        SimooCourse::where('id', $id)->update($data);

        ## atualizar na tabela 'course_dates'
        $toolsDates = ToolsSimooCourse::Period($id, $dates, $hourInit, $hourEnd, $block);
        SimooCourseDates::where('courseid', $id)->delete();
        SimooCourseDates::where('courseid', $id)->insert($toolsDates);
    }

    public static function delete(int $id)
    {

    }
}