<?php

namespace App\Libraries\Simoo;

## Models
use App\Model\Simoo\SimooUserPublic;
use App\Model\Simoo\SimooUserGroup;
use App\Model\Simoo\SimooUserType;

class SimooLibTarget
{
    public static function Full()
    {
        $target = [];

        ## Busca os públicos, grupos e tipos
        $publics = SimooUserPublic::orderBy('name', 'asc')->get();
        $groups = SimooUserGroup::orderBy('publicid', 'asc')->orderBy('name', 'asc')->get();
        $types = SimooUserType::orderBy('groupid', 'asc')->orderBy('name', 'asc')->get();

        ## Adiciona os resultados no array $target
        foreach($publics as $public):
            $target += [ $public->name => [ 
                    'groups' => [],
                    'info' => [
                        'id' => $public->id
                    ]
                ]
            ];

            foreach($groups as $group):

                if($public->id == $group->publicid):
                    if(isset($target[$public->name])):
                        $target[$public->name]['groups'] += [ 
                            $group->name => [  
                                'types' => [],
                                'info' => [
                                    'id' => $group->id
                                ]
                            ] 
                        ];
                    endif;
                endif;

                foreach($types as $type):

                    if($group->id == $type->groupid):
                        if(isset($target[$public->name]['groups'][$group->name])):
                            $target[$public->name]['groups'][$group->name]['types'] += [
                                $type->name => [  
                                    'info' => [
                                        'id' => $type->id
                                    ]
                                ] 
                            ];
                        endif;
                    endif;

                endforeach;

            endforeach;
            
        endforeach;

        return ($target);
    }
}