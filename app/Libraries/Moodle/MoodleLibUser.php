<?php

namespace App\Libraries\Moodle;

## WebService
use App\Libraries\Moodle\WebService\RestCall;

## Models
use App\Model\Moodle\MoodleUser;
use App\Model\Moodle\MoodleUserInfoData;
use App\Model\Moodle\MoodleUserInfoField;

## Libraries

## Helpers

class MoodleLibUser
{
    public static function GetAll()
    {
        ## busca todos os usuários que estão no moodle
        $users = MoodleUser::whereNotIn('id', [1,2])->where('deleted', 0)
                    ->orderBy('firstname')->orderBy('lastname')->paginate(4);
        return $users;
    }

    public static function GetId(array $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_user_get_users';

        ## Inseri o parâmetro recebido
        $params = array('criteria' => [$data]);

        ## Faz a chamada com a API
        $user = RestCall::call($functionName, $params)['users'][0];
        $user['fields'] = [
            'registry' => '', 'cellphone' => '', 'public' => '', 'group' => '', 'type' => ''
        ];

        if(isset($user['customfields'])):
            foreach($user['customfields'] as $field):
                if($field['shortname']== 'registry'):
                    $user['fields']['registry'] = $field['value'];

                elseif($field['shortname'] == 'cellphone'):
                    $user['fields']['cellphone'] = $field['value'];
                    
                elseif($field['shortname'] == 'public'):
                    $user['fields']['public'] = $field['value'];

                elseif($field['shortname'] == 'group'):
                    $user['fields']['group'] = $field['value'];

                elseif($field['shortname'] == 'type'):
                    $user['fields']['type'] = $field['value'];

                endif;
            endforeach;
        endif;

        return ($user);
    }

    public static function GetUserIdCourse(int $userId)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_user_get_users';

        ## Inseri o parâmetro recebido
        $params = array('criteria' => [['key' => 'id', 'value' => $userId]]);

        ## Faz a chamada com a API
        $user = RestCall::call($functionName, $params)['users'][0];
        $course = MoodleLibCourse::GetIdUser($userId);

        ## Inserção dos cursos que o usuário participou
        $user['courses'] = $course;

        return ($user);
    }

    public static function AuthConfirmUser($username, $password)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_auth_confirm_user';

        ## Inseri o parâmetro recebido
        $params = array(
            'username' => $username,
            'secret' => $password
        );

        ## Faz a chamada com a API
        return RestCall::call($functionName, $params);
    }

    public static function Create(array $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_user_create_users';

        ## Inseri o parâmetro recebido
        $params = array('users' => [$data]);

        ## Faz a chamada com a API
        return RestCall::call($functionName, $params);
    }

    public static function Update(array $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_user_update_users';

        ## Inseri o parâmetro recebido
        $params = array('users' => [$data]);

        ## Faz a chamada com a API
        return RestCall::call($functionName, $params);
    }

    public static function UpdateInfoData(array $shortName, array $userId, array $data)
    {
        foreach($shortName as $key => $name):
            $infoField = MoodleUserInfoField::where('shortname', $name)->first();
            MoodleUserInfoData::where('userid',$userId[$key])->where('fieldid', $infoField->id)->update(['data' => $data[$key]]);
        endforeach;
    }  

    public static function Delete(int $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_user_delete_users';

        ## Inseri o parâmetro recebido
        $params = array('userids' => [$data]);

        ## Faz a chamada com a API
        return RestCall::call($functionName, $params);
    }
}