<?php

namespace App\Libraries\Moodle\WebService;

//Webservice
use App\Libraries\Moodle\WebService\Curl;

class RestCall{

    public static function call(string $functionname, $params)
    {
        $domainname = env('APP_URL_MOODLE');
        $token = env('TOKEN_MOODLE');
        $restformat = 'json';

        /// REST CALL
        $serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;

        $curl = new Curl;

        //if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
        $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
        $resp = $curl->post($serverurl . $restformat, $params);

        return json_decode($resp, true);
    }
}