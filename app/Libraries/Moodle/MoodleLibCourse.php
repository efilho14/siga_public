<?php

namespace App\Libraries\Moodle;

## WebService
use App\Libraries\Moodle\WebService\RestCall;

## Models
use App\Model\Moodle\MoodleCourse;
use App\Model\Moodle\MoodleUser;
use App\Model\Moodle\MoodleContext;
use App\Model\Moodle\MoodleGradeItems;
use App\Model\Simoo\SimooCourse;
use App\Model\Simoo\SimooStudentFrequency;
use App\Model\Simoo\SimooCourseConfig;
use App\Model\Simoo\SimooCourseDates;
use App\Model\Simoo\SimooEnvironment;
use App\Model\Simoo\SimooRoom;

## Libraries
use App\Libraries\Moodle\MoodleLibStudent;
use App\Libraries\Moodle\MoodleLibTeacher;
use App\Libraries\Moodle\MoodleLibCoordinator;
use App\Libraries\Simoo\SimooLibCourse;
use App\Libraries\Simoo\SimooLibInit;

## Helpers
use App\Helpers\General\ToolsGeneral;

class MoodleLibCourse
{

    public static function GetAll(array $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_course_get_courses';

        ## Inseri o parâmetro recebido
        $params = array('options' => $data);

        ## Faz a chamada com a API
        return RestCall::call($functionName, $params);
    }

    public static function GetIdUser(int $idUser)
    {
        ## Criar Array
        $coursesFull = [
            'student' => [], 
            'teacher' => [], 
            'editingteacherone' => [], 
            'editingteachertwo' => []
        ];

        ## Buscar todos os cursos presenciais
        $courses = MoodleCourse::select('mdl_course.id', 'mdl_course.fullname')
                        ->join('mdl_course_categories', 'mdl_course.category', '=', 'mdl_course_categories.id')
                        ->where('mdl_course_categories.path', 'like', '/'. session()->get('presential').'/%')
                        ->where('mdl_course.category', '<>', 0)->get();

        foreach($courses as $course):                        
            ## Buscar context
            $context = MoodleContext::where('instanceid', $course->id)
                        ->where('contextlevel', '50')->first();

            ## Buscar no curso o 'Role' do Usuário
            $courseMoodle = MoodleUser::select('mdl_user.id', 'mdl_course.id as courseid', 'mdl_course.fullname', 'mdl_course.idnumber', 'mdl_user.username', 'mdl_user.firstname', 'mdl_user.lastname', 'mdl_user.email', 'mdl_role.shortname')
                ->join('mdl_user_enrolments', 'mdl_user.id', '=', 'mdl_user_enrolments.userid')
                ->join('mdl_role_assignments', 'mdl_role_assignments.userid', '=', 'mdl_user.id')
                ->join('mdl_role', 'mdl_role_assignments.roleid', '=', 'mdl_role.id')
                ->join('mdl_enrol', 'mdl_user_enrolments.enrolid', '=', 'mdl_enrol.id')
                ->join('mdl_course', 'mdl_enrol.courseid', '=', 'mdl_course.id')
                ->where('mdl_user.confirmed', 1)
                ->where('mdl_user.id', $idUser)
                ->where('mdl_course.id', $course->id)
                ->where('mdl_role_assignments.contextid', $context->id)
                ->orderBy('mdl_role.shortname', 'ASC')->first();

            if(!empty($courseMoodle)):
                
                ## Buscar as informações adicionais no Simoo
                $courseSimoo = SimooCourse::select('id', 'workload', 'config', 'frequency', 'grade')
                ->where('instanceid', $course->id)->first();

                ## Buscar o período do curso
                $courseDates = SimooLibCourse::get_date($courseSimoo->id);

                ## Buscar as configurações iniciais do curso
                $courseConfig = SimooCourseConfig::select('*')->first();

                ## Verficar a situação do usuário, se é estudante, instrutor ou coordenador na turma
                if($courseMoodle['shortname'] == 'student'):

                    $grade = MoodleGradeItems::select('mdl_grade_grades.finalgrade')
                    ->join('mdl_grade_grades', 'mdl_grade_items.id', '=', 'mdl_grade_grades.itemid')
                    ->where('mdl_grade_items.courseid', $course->id)
                    ->where('mdl_grade_items.itemtype', 'mod')
                    ->where('mdl_grade_grades.userid', $idUser)
                    ->where('mdl_grade_items.grademax', '10.00000')->first();

                    // Buscar a frequência do usuário
                    $frequency = SimooStudentFrequency::select('student_frequency.frequency')
                        ->join('course', 'student_frequency.courseid', '=', 'course.id')
                        ->where('instanceid', $course->id)
                        ->where('userid', $idUser)->first();

                    // Calcula a frequência do usuário na turma
                    $frequencyTotal = round(((($courseSimoo['workload'] - ($courseSimoo['workload'] - $frequency['frequency'])) / $courseSimoo['workload']) * 100));

                    // Se a turma selecionou as configurações padrões ao ser criada
                    if ($courseSimoo->config == 1):

                        if($frequencyTotal >= $courseConfig->frequency AND $grade->finalgrade >= $courseConfig->grade) :
                            $situation = 'Aprovado';
                        elseif($frequency == 0 AND $grade == ''):
                            $situation = 'Em andamento';
                        else:
                            $situation = 'Reprovado';
                        endif;
                        
                    else:

                        if($frequencyTotal >= $courseSimoo->frequency AND $grade->finalgrade >= $courseSimoo->grade) :
                            $situation = 'Aprovado';
                        elseif($frequency == 0 AND $grade == ''):
                            $situation = 'Em andamento';
                        else:
                            $situation = 'Reprovado';
                        endif;

                    endif;

                else:

                    $situation = '';

                endif;

                //Inseri no array as informações da turma coletadas acima
                array_push($coursesFull[$courseMoodle['shortname']], [
                    'userid' => $courseMoodle['id'],
                    'courseid' => $courseMoodle['courseid'],
                    'courseName' => $courseMoodle['fullname'],
                    'idnumber' => $courseMoodle['idnumber'],
                    'period' => $courseDates,
                    'workload' => $courseSimoo['workload'],
                    'role' => $courseMoodle['shortname'],
                    'situation' => $situation
                ]);

            endif;

        endforeach;

        return $coursesFull;
    }

    public static function GetCourseMouth(int $numberMonths, int $yearSelected)
    {
        ## Meses com os cursos
        $monthsWithCourses = [];

        ## Se o ano selecionado for igual ao ano atual, o mesmo poderá mostrar o 3 primeiros meses ou o ano inteiro, mas se for outro ano, mostra-se todo o ano
        if($yearSelected == date('Y')):

            if($numberMonths==3):

                ## Data início
                $dateCurrent = strtotime(date('Y-m-01'));
                $dateGetLastDay = date('t', mktime(0, 0, 0, date('m', strtotime(date('Y-m') . '+' . $numberMonths . ' month')), 01, date('Y'))) . "<br>";

                ## Setar o números de meses desejado
                $dateWithNumber = strtotime(date('Y-m') . "+" . $numberMonths . " month");

            elseif($numberMonths==12):

                ## Data início
                $dateCurrent = strtotime(date('Y-01-01'));

                ## Setar o números de meses desejado
                $dateWithNumber = strtotime(date('Y-12-31') . "+" . $numberMonths . " month");

            endif;
            
        else:

            ## Data início
            $dateCurrent = strtotime(date($yearSelected.'-m-01'));

            ## Setar o números de meses desejado
            $dateWithNumber = strtotime(date('Y-m') . "+" . $numberMonths . " month");
        endif;

        

        ## Chamar o INIT do presencial
        SimooLibInit::presential();

        ## Pega o id da categoria 'presencial'
        $presential = "/" . session('presential');

        ## Buscar os cursos com as variáveis solicitadas
        $courses = MoodleCourse::select('mdl_course.id as courseid', 'mdl_course.fullname', 'mdl_course.shortname', 'mdl_course.startdate')
            ->join('mdl_course_categories', 'mdl_course.category', '=', 'mdl_course_categories.id')
            ->where('mdl_course_categories.path', 'like', '/' . session()->get('presential') . '/%')
            ->where('mdl_course.startdate','>=',$dateCurrent)
            ->where('mdl_course.startdate','<=',$dateWithNumber)
            ->orderBy('mdl_course.startdate', 'ASC')
            ->orderBy('mdl_course.fullname','ASC')
            ->get();

        if($numberMonths == 12 AND count($courses)):
            $monthsWithCourses = [
                'Janeiro' => [],
                'Fevereiro' => [],
                'Março' => [],
                'Abril' => [],
                'Maio' => [],
                'Junho' => [],
                'Julho' => [],
                'Agosto' => [],
                'Setembro' => [],
                'Outubro' => [],
                'Novembro' => [],
                'Dezembro' => []
            ];
        endif;

        ## Varre os cursos dos meses inseridos e inseri na variavel 'monthsWithCourses'
        foreach($courses as $course):
            
            // Buscar os meses dos cursos
            $month = ToolsGeneral::monthToBrazil(date('F', $course['startdate']));

            // Buscar os alunos dos cursos
            $courseStudent = MoodleLibStudent::get([
                'courseid' => $course['courseid'],
                'options' => [
                    [
                        'name' => 'sortby',
                        'value' => 'firstname'
                    ]
                ]
            ]);
            
            // Buscar simoo table 'course'
            $courseSimoo = SimooCourse::select('id')->where('instanceid', $course['courseid'])->first();

            // Buscar o período e hora do curso
            $courseDates = SimooLibCourse::get_date($courseSimoo['id']);

            // Adiciona os meses na variável 'monthsWithCourses'
            if($numberMonths == 3):
                $monthsWithCourses += [$month => []];
            endif;

            // Adiciona as informações na variável 'monthsWithCourses'
            $monthsWithCourses[$month] += [
                $course['shortname'] => [
                    'courseid' => $course['courseid'],
                    'fullname' => $course['fullname'],
                    'shortname' => $course['shortname'],
                    'enrolled' => count($courseStudent['students']),
                    'dates' => $courseDates
                ]
            ];

        endforeach;
        
        return ($monthsWithCourses);
    }

    public static function Full(array $data)
    {
        $course = self::GetAll($data);

        ## Buscar simoo table 'config'
        $courseConfig = SimooCourseConfig::getConfig();

        ## Buscar simoo table 'course'
        $courseSimoo = SimooCourse::where('instanceid', $data['ids'][0])->first();

        ## Buscar simoo table 'course_room'
        $courseRoom = SimooRoom::where('id', $courseSimoo->location)->first();

        ## Buscar simoo table 'environment'
        $courseEnvironment = SimooEnvironment::where('id', $courseRoom->environmentid)->first();
        
        ## Buscar simoo table 'course_dates'
        $courseDates = SimooLibCourse::get_date($courseSimoo->id);

        ## Teacher
        $courseTeacher = MoodleLibTeacher::get($data['ids'][0])->toArray();

        ## Coordinator
        $courseCoordinator = MoodleLibCoordinator::get($data['ids'][0])->toArray();

        ## Student
        $courseStudent = MoodleLibStudent::get([
            'courseid' => $data['ids'][0],
            'options'  => [
                ['name'   => 'sortby',
                'value'  => 'firstname']
            ]
        ]);

        $course[0]['environment'] = $courseEnvironment->name;
        $course[0]['room'] = $courseRoom->name;
        $course[0]['dates'] = $courseDates;
        $course[0]['workload'] = $courseSimoo->workload;
        $course[0]['config'] = $courseSimoo->config;
        $course[0]['frequency'] = ($courseSimoo->frequency) ? $courseSimoo->frequency : $courseConfig['frequency'];
        $course[0]['grade'] = ($courseSimoo->grade) ? $courseSimoo->grade : $courseConfig['grade'];
        $course[0]['teacher'] = $courseTeacher;
        $course[0]['coordinator'] = $courseCoordinator;
        $course[0]['student'] = $courseStudent['students'];
        $course[0]['approved'] = $courseStudent['approved'];
        $course[0]['disapproved'] = $courseStudent['disapproved'];
        $course[0]['inprogress'] = $courseStudent['inprogress'];
        /*
            ALERTA:
            Trabalhar no item teacher, coodinator e student, pois da forma que se encontra, o mesmo está
                fazendo 3 requisições, ao invés, de uma só e organizá-los em um array.
        */

        return $course;
    }

    public static function create(array $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_course_create_courses';

        ## Inseri o parâmetro recebido
        $params = array('courses' => array($data));

        ## Faz a chamada com a API
        return RestCall::call($functionName, $params)[0];
    }

    public static function update(array $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_course_update_courses';

        ## Inseri o parâmetro recebido
        $params = array('courses' => array($data));

        ## Faz a chamada com a API
        RestCall::call($functionName, $params);
    }

    public static function delete(int $data)
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionName = 'core_course_delete_courses';

        ## Inseri o parâmetro recebido
        $params = array('courseids' => array($data));

        ## Faz a chamada com a API
        RestCall::call($functionName, $params);
    }
}