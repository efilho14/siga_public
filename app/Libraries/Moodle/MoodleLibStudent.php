<?php

namespace App\Libraries\Moodle;

## WebService
use App\Libraries\Moodle\WebService\RestCall;

## Models
use App\Model\Moodle\MoodleGradeItems;
use App\Model\Moodle\MoodleGradeGrades;
use App\Model\Moodle\MoodleEnrol;
use App\Model\Moodle\MoodleContext;
use App\Model\Moodle\MoodleRoleAssignments;
use App\Model\Moodle\MoodleUserEnrolments;
use App\Model\Simoo\SimooCourse;
use App\Model\Simoo\SimooCourseConfig;
use App\Model\Simoo\SimooStudentFrequency;

## Helpers
use App\Helpers\General\ToolsGeneral;

class MoodleLibStudent{

    public static function get(array $data, string $simple='N')
    {
        ## Informa qual a funcão será acessada pelo webservice
        $functionname = 'core_enrol_get_enrolled_users';

        ## Faz a chamada com a API
        $students = RestCall::call($functionname, $data);

        ## Buscar as informações adicionais no Simoo
        $courseSimoo = SimooCourse::select('id', 'workload', 'config', 'frequency', 'grade')
            ->where('instanceid', $data['courseid'])->first();

        ## Buscar as configurações iniciais do curso
        $courseConfig = SimooCourseConfig::select('*')->first();

        ## Variáveis de estudantes aprovados, reprovados e em damento
        $approved=0;
        $disapproved=0;
        $inprogress=0;

        ## Corre o array da API
        if($simple == 'N'):
            
            foreach($students as $key => $student):
                if($student['roles'][0]['shortname'] != 'student'):
                    unset($students[$key]);
                else:
                    // Busca a nota do aluno individualmente
                    $grade = MoodleGradeItems::select('mdl_grade_items.courseid', 'mdl_grade_items.id', 'mdl_grade_grades.finalgrade', 'mdl_grade_grades.userid')
                        ->join('mdl_grade_grades', 'mdl_grade_items.id', '=', 'mdl_grade_grades.itemid')
                        ->where('mdl_grade_items.courseid', $data['courseid'])
                        ->where('mdl_grade_grades.userid', $student['id'])
                        ->whereNotNull('mdl_grade_items.categoryid')->first();

                    // Busca a frequência do aluno individualmente
                    $frequency = SimooStudentFrequency::select('student_frequency.userid', 'student_frequency.frequency')
                        ->join('course', 'course.id', '=', 'student_frequency.courseid')
                        ->where('course.instanceid', $data['courseid'])
                        ->where('student_frequency.userid', $student['id'])->first();

                    // Calcula a frequência do usuário na turma
                    $frequencyTotal = round(((($courseSimoo['workload'] - ($courseSimoo['workload'] - $frequency['frequency'])) / $courseSimoo['workload']) * 100));

                    // Setar a nota
                    ($grade) ? $grade = $grade->finalgrade : $grade = '';

                    // Setar a frequência
                    ($frequency) ? $frequency = $frequency['frequency'] : $frequency = '';

                    // Se a turma selecionou as configurações padrões ao ser criada
                    if($courseSimoo->config == 1) :
                
                        if($frequencyTotal >= $courseConfig['frequency'] AND $grade >= $courseConfig['grade']):
                            $situation = 'Aprovado';
                        elseif($frequency == 0 AND $grade == ''):
                            $situation = 'Em andamento';
                        else:
                            $situation = 'Reprovado';
                        endif;

                    else:

                        if($frequencyTotal >= $courseSimoo['frequency'] AND $grade >= $courseSimoo['grade']):
                            $situation = 'Aprovado';
                        elseif($frequency == 0 AND $grade == ''):
                            $situation = 'Em andamento';
                        else:
                            $situation = 'Reprovado';
                        endif;
            
                    endif;

                    // Verifica se grade é nulo
                    if($grade==null):
                        //$grade = '-';
                    endif;

                    // Calcula a quantidade e estudantes aprovados, reprovados e em andamento
                    if($situation == "Aprovado") $approved++;
                    if($situation == "Reprovado") $disapproved++;
                    if($situation == "Em andamento") $inprogress++;

                    // Inseri as informações de nota, frequencia e situação
                    $students[$key]['grade'] = ToolsGeneral::truncate($grade,2);
                    $students[$key]['frequency'] = $frequency; 
                    $students[$key]['situation'] = $situation;
                endif;
            endforeach;

        endif;

        $students = array_values($students);

        $infos = [
            'students' => $students, 
            'approved'  => $approved,
            'disapproved' => $disapproved,
            'inprogress' => $inprogress
        ];

        return $infos;
    }

    public static function remove(int $userId, int $courseId)
    {   
        ## BUSCAR ENROL
        $enrol = MoodleEnrol::where('courseid',$courseId)
                            ->where('enrol','=','manual')->first();

        ## BUSCAR CONTEXT
        $contextId = MoodleContext::where('instanceid',$courseId)
                                  ->where('contextlevel','50')->first();

        ## REMOVER USUÁRIO NO ROLE_ASSIGNMENTS
        MoodleRoleAssignments::where('contextid',$contextId->id)
                             ->where('userid',$userId)->delete();

        ## REMOVER USUÁRIO NO USER_ENROLMENTS
        MoodleUserEnrolments::where('enrolid', $enrol->id)
                            ->where('userid', $userId)->delete();

        ## REMOVE GRADE
        $gradeItems = MoodleGradeItems::where('courseid', $courseId)->orderBy('id','desc')->get();
        if(count($gradeItems)):
            $items = [];

            //pegar o id do gradeItem e inclui em um array
            foreach($gradeItems as $itemId):
                array_push($items, $itemId->id);
            endforeach; 
            
            MoodleGradeGrades::where('userid', $userId)
                        ->whereIn('itemid', $items)->delete();
        endif;

        ## REMOVE FREQUENCY
        $course = SimooCourse::where('instanceid',$courseId)->first();
        if(count($course)):

            //remover frequencia do usuario
            SimooStudentFrequency::where('courseid', $course->id)
                        ->where('userid', $userId)->delete();
        endif;

        ## REMOVE AS LIÇÕES COMPLETADAS
    }
}