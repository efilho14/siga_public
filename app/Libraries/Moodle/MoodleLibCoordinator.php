<?php

namespace App\Libraries\Moodle;

//models
use App\Model\Moodle\MoodleContext;
use App\Model\Moodle\MoodleUser;

//helpers
use App\Helpers\General\ToolsGeneral;

class MoodleLibCoordinator
{
    public static function get(int $courseId)
    {
        ## buscar context
        $context = MoodleContext::where('instanceid',$courseId)
                                  ->where('contextlevel','50')->first();

        $user = MoodleUser::select('mdl_user.id','mdl_user.username','mdl_user.firstname','mdl_user.lastname','mdl_user.email','mdl_role.shortname')
                    ->join('mdl_user_enrolments', 'mdl_user.id', '=', 'mdl_user_enrolments.userid')
                    ->join('mdl_role_assignments', 'mdl_role_assignments.userid', '=', 'mdl_user.id')
                    ->join('mdl_role', 'mdl_role_assignments.roleid', '=', 'mdl_role.id')
                    ->join('mdl_enrol', 'mdl_user_enrolments.enrolid', '=', 'mdl_enrol.id')
                    ->where('mdl_user.confirmed', 1)
                    ->where('mdl_role.shortname', 'teacher')
                    ->where('mdl_enrol.courseid', $courseId)
                    ->where('mdl_role_assignments.contextid', $context->id)
                    ->orderBy('mdl_role.shortname', 'ASC')
                    ->orderBy('mdl_user.firstname', 'ASC')
                    ->orderBy('mdl_user.lastname', 'ASC')->get();
                    
        return $user;
    }

}