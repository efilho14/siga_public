<?php

namespace App\Libraries\Moodle;

//models
use App\Model\Moodle\MoodleContext;
use App\Model\Moodle\MoodleCourseModules;
use App\Model\Moodle\MoodleCourseSections;
use App\Model\Moodle\MoodleGradeCategories;
use App\Model\Moodle\MoodleGradeItems;
use App\Model\Moodle\MoodleQuiz;
use App\Model\Moodle\MoodleQuizFeedback;
use App\Model\Moodle\MoodleQuizSections;

//helpers
use App\Helpers\General\ToolsGeneral;

//libraries
use App\Libraries\Moodle\CourseCategories;

class MoodleLibQuiz
{    
    public static function create(int $courseId, string $name, int $visible, float $grade)
    {
        ## inseri o 'mdl_quiz'
        $quiz = MoodleQuiz::insertQuiz($courseId, $name, $grade, ToolsGeneral::strToTime());

        ## inseri o 'mdl_quiz_feedback'
        MoodleQuizFeedback::insertFeedback($quiz);

        ## inseri o 'mdl_quiz_sections'
        MoodleQuizSections::insertSections($quiz);

        ## inseri o 'mdl_grade_categories'
        $searchGradeCategories = MoodleGradeCategories::where('courseid', $courseId)->first();
        if(empty($searchGradeCategories)):
            MoodleGradeCategories::insertCategories($courseId);
        endif;

        ## inseri o 'mdl_grade_items'
        MoodleGradeItems::insertItems($courseId, $quiz, $name, $grade);

        ## inseri o 'mdl_course_modules'
        $courseModules = MoodleCourseModules::insertCourseModules($courseId, $quiz, 'quiz', 0, NULL, $visible);

        ## inseri ou atualiza o 'mdl_course_sections'
        $searchCourseSections = MoodleCourseSections::where('course', $courseId)->first();
        if(!empty($searchCourseSections)):
            MoodleCourseSections::updateCourseSections($courseId, 0, $courseModules);
        else:
            MoodleCourseSections::insertCourseSections($courseId, 0, $courseModules);
        endif;

        ## busca o 'mdl_context' da turma
        $contextCourse = MoodleContext::getInstanceId($courseId, 50);

        ## inseri o 'mdl_context'
        $context = MoodleContext::insertContext($courseModules, 70, '', 0);
        $contextUpdate = MoodleContext::updateContext($context, ($contextCourse->path.'/'.$context), ($contextCourse->depth+1));
    }
}