<?php

namespace App\Libraries\Moodle;

//models
use App\Model\Moodle\MoodleQuestionnaire;
use App\Model\Moodle\MoodleQuestionnaireSurvey;
use App\Model\Moodle\MoodleCourseModules;
use App\Model\Moodle\MoodleCourseSections;
use App\Model\Moodle\MoodleContext;

//helpers
use App\Helpers\General\ToolsGeneral;

class MoodleLibQuestionnaire
{
    public static function create(int $courseId, string $name)
    {
        ## inseri o 'mdl_questionnaire_survey'
        $survey = MoodleQuestionnaireSurvey::insertSurvey($courseId, $name);

        ## inseri o 'mdl_questionnaire'
        $questionnaire = MoodleQuestionnaire::insertQuestionnaire($courseId, $name, $survey, ToolsGeneral::strToTime());        

        ## inseri o 'mdl_course_modules'
        $courseModules = MoodleCourseModules::insertCourseModules($courseId, $questionnaire, 'questionnaire', 2);

        ## inseri ou atualiza o 'mdl_course_sections'
        $searchCourseSections = MoodleCourseSections::where('course', $courseId)->first();
        if(empty($searchCourseSections)):
            MoodleCourseSections::insertCourseSections($courseId, 0, $courseModules);
        else:
            MoodleCourseSections::updateCourseSections($courseId, 0, $courseModules);
        endif;

        ## busca o 'mdl_context' da turma
        $contextCourse = MoodleContext::getInstanceId($courseId, 50);

        ## inseri o 'mdl_context'
        $context = MoodleContext::insertContext($courseModules, 70, '', 0);
        $contextUpdate = MoodleContext::updateContext($context, ($contextCourse->path.'/'.$context), ($contextCourse->depth+1));
    }

    public static function delete(int $courseId)
    {

    }
}