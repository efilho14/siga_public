<?php

namespace App\Libraries\Moodle;

//WebService
use App\Libraries\Moodle\WebService\RestCall;

class MoodleLibCourseCategories
{
    public static function create(array $data)
    {
        //Informa qual a funcão será acessada pelo webservice
        $functionname = 'core_course_create_categories';

        //Inseri o parâmetro recebido
        $params = array('categories' => array($data));

        //Faz a chamada com a API
        $result = RestCall::call($functionname, $params);
    }

    public static function update(array $data)
    {
        //Informa qual a funcão será acessada pelo webservice
        $functionname = 'core_course_update_categories';

        //Inseri o parâmetro recebido
        $params = array('categories' => array($data));

        //Faz a chamada com a API
        $result = RestCall::call($functionname, $params);
        print_r($result);
    }

    public static function delete(array $data)
    {
        //Informa qual a funcão será acessada pelo webservice
        $functionname = 'core_course_delete_categories';

        //Inseri o parâmetro recebido
        $params = array('categories' => array($data));

        //Faz a chamada com a API
        RestCall::call($functionname, $params);
    }
}