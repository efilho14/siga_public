<?php

namespace App\Libraries\Moodle;

//models
use App\Model\Simoo\SimooEnvironment;
use App\Model\Simoo\SimooRoom;

//helpers
use App\Helpers\General\ToolsGeneral;

class MoodleLibEnvironment
{
    public static function union()
    {
        $environmentWithRoom = [];
        $environment = SimooEnvironment::getAll();
        $room = SimooRoom::getAll();
        foreach($environment as $keyEnvironment => $environments):
            array_push($environmentWithRoom, array(
                'environmentId' => $environments->id,
                'environmentName' => $environments->name,
                'rooms' => array()
            ));
            foreach($room as $keyRoom => $rooms):
                if($rooms->environmentid == $environments->id):
                    array_push($environmentWithRoom[$keyEnvironment]['rooms'], array(
                        'roomId' => $rooms->id,
                        'roomName' => $rooms->name
                    ));
                endif;
            endforeach;
        endforeach;

        return ($environmentWithRoom);
    }
}