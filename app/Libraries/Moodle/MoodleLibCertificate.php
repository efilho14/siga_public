<?php

namespace App\Libraries\Moodle;

//models
use App\Model\Moodle\MoodleCertificate;
use App\Model\Moodle\MoodleCourseModules;
use App\Model\Moodle\MoodleCourseSections;
use App\Model\Moodle\MoodleGradeItems;
use App\Model\Moodle\MoodleContext;

//helpers
use App\Helpers\General\ToolsGeneral;

class MoodleLibCertificate
{
    public static function create(int $courseId, string $name, int $workload, int $frequency, int $grade)
    {
        //inseri o 'mdl_certificate'
        $certificate = MoodleCertificate::insertCertificate($courseId, $name, ToolsGeneral::strToTime(), $workload);

        ## busca o gradeItems
        $gradeItems = MoodleGradeItems::where('courseid', $courseId)->whereNotNull('categoryid')->orderBy('itemname', 'asc')->get();

        ## busca o courseModules
        $courseModulesQuiz = MoodleCourseModules::where('course', $courseId)->where('completion', 2)->first();


        ## inseri o 'mdl_course_modules'
        $courseModules = MoodleCourseModules::insertCourseModules($courseId, $certificate, 'certificate', 0, '{"op":"&","c":[{"type":"completion","cm":' . $courseModulesQuiz['id'] . ',"e":1},{"type":"grade","id":' . $gradeItems[0]->id . ',"min":' . ($grade*10) . '}, {"type":"grade","id":' . $gradeItems[1]->id . ',"min":' . $frequency . '}],"showc":[true,false,false]}');

        ## inseri ou atualiza o 'mdl_course_sections'
        $searchCourseSections = MoodleCourseSections::where('course', $courseId)->first();
        if(empty($searchCourseSections)):
            MoodleCourseSections::insertCourseSections($courseId, 0, $courseModules);
        else:
            MoodleCourseSections::updateCourseSections($courseId, 0, $courseModules);
        endif;

        ## busca o 'mdl_context' da turma
        $contextCourse = MoodleContext::getInstanceId($courseId, 50);

        ## inseri o 'mdl_context'
        $context = MoodleContext::insertContext($courseModules, 70, '', 0);
        $contextUpdate = MoodleContext::updateContext($context, ($contextCourse->path.'/'.$context), ($contextCourse->depth+1));
    }
}