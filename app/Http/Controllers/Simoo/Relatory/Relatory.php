<?php

# Classe Relatory
#### Mostra os relatórios do Simoo

namespace App\Http\Controllers\Simoo\Relatory;

## Controller
use App\Http\Controllers\Controller;

## Libraries
use App\Libraries\Moodle\MoodleLibUser;


class Relatory extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('relatory.index');

        return view('simoo.relatory.index');
    }
}