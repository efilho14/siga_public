<?php

# Classe CalendarCourse
#### Mostra o calendário dos cursos

namespace App\Http\Controllers\Simoo\Calendar;

## Controller
use App\Http\Controllers\Controller;

## Libraries
use App\Libraries\Moodle\MoodleLibUser;


class CalendarCourse extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('calendar.index');

        return view('simoo.calendar.index');
    }
}