<?php

# Classe RoomController
#### Controla as salas de aula cadastradas no simoo

namespace App\Http\Controllers\Simoo\Environment;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Simoo\SimooEnvironment;
use App\Model\Simoo\SimooRoom;

## Others
use Validator;
use Input;
use Session;
use Notify;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        ## Permissão
        $this->authorize('room.create');

        $environment = SimooEnvironment::getAll();
        return view('simoo.salas.create')->withEnvironment($environment)->withIdEnvironment($id);
    }

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('room.create');

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome da Sala');	
        $rules = array('name'=> 'required');
	    $validator = Validator::make(Input::all(), $rules,$messsages);
        
        ## Validação
        if ($validator->fails()):
            return redirect('simoo/salas/create')
                        ->withErrors($validator)
                        ->withInput();
        endif;

        ## Inserir no DB 'Simoo' na tabela 'environment'
        $room = new SimooRoom();
        $room->name = mb_strtoupper($request->name, 'UTF-8');
        $room->environmentid = $request->environmentid;
        $room->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Sala', $options = []);

        return redirect()->route('simoo.ambientes.index')->with('id', $room->environmentid);
    }

    public function edit($id)
    {
        ## Permissão
        $this->authorize('room.edit');

        $room = SimooRoom::find($id);
        return view('simoo.salas.edit')->withRoom($room);
    }

    public function update(Request $request, $id)
    {
        ## Permissão
        $this->authorize('room.edit');

        ## Buscar o evento a ser atualizado pelo ID
        $room = SimooRoom::find($id);

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome da Sala');	
        $rules = array('name'=> 'required');
	    $validator = Validator::make(Input::all(), $rules,$messsages);
        
        ## Validação
        if ($validator->fails()):
            return redirect('simoo/salas/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        endif;

        ## Inserir as informações preenchidas no form
        $room->name = strtoupper($request->name);
        $room->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Sala', $options = []);

        return redirect()->route('simoo.ambientes.index')->with('id', $room->environmentid);
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('room.delete');

        ## Buscar o evento a ser atualizado pelo ID
        $room = SimooRoom::find($id);
        $room->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Sala', $options = []);

        return redirect()->route('simoo.ambientes.index')->with('id', $room->environmentid);
    }
}
