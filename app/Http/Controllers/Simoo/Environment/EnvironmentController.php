<?php

# Classe EnvironmentController
### Controla todos os ambientes cadastrados no simoo.

namespace App\Http\Controllers\Simoo\Environment;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Simoo\SimooEnvironment;
use App\Model\Simoo\SimooRoom;

## Others
use Validator;
use Input;
use Session;
use Notify;

class EnvironmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('environment.index');

        $environmentWithRoom = [];
        $environment = SimooEnvironment::getAll();
        $room = SimooRoom::getAll();
        foreach($environment as $keyEnvironment => $environment):

            array_push($environmentWithRoom, array(
                'environmentId' => $environment->id,
                'environmentName' => $environment->name,
                'rooms' => array()
            ));

            foreach($room as $keyRoom => $rooms):

                if($rooms->environmentid == $environment->id):

                    array_push($environmentWithRoom[$keyEnvironment]['rooms'], array(
                        'roomId' => $rooms->id,
                        'roomName' => $rooms->name
                    ));

                endif;
                
            endforeach;

        endforeach;

        return view('simoo.ambientes.index')->withEnvironment($environmentWithRoom);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('environment.create');

        return view('simoo.ambientes.create');
    }

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('environment.create');

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome do Ambiente');	
        $rules = array('name'=> 'required');
	    $validator = Validator::make(Input::all(), $rules,$messsages);
        
        ## Validação
        if ($validator->fails()):
            return redirect('simoo/ambientes/create')
                        ->withErrors($validator)
                        ->withInput();
        endif;

        ## Inserir no DB 'Simoo' na tabela 'environment'
        $environment = new SimooEnvironment();
        $environment->name = strtoupper($request->name);
        $environment->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Ambiente', $options = []);

        return redirect()->route('simoo.ambientes.index');
    }

    public function edit(int $id)
    {
        ## Permissão
        $this->authorize('environment.edit');

        $environment = SimooEnvironment::find($id);
        return view('simoo.ambientes.edit')->withEnvironment($environment);
    }

    public function update(Request $request, int $id)
    {
        ## Permissão
        $this->authorize('environment.edit');

        ## Buscar o evento a ser atualizado pelo ID
        $environment = SimooEnvironment::find($id);

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required'
        ));

        ## Inserir as informações preenchidas no form
        $environment->name = strtoupper($request->name);
        $environment->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Ambiente', $options = []);

        return redirect()->route('simoo.ambientes.index')->with('id',$id);
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('environment.delete');

        ## Buscar o evento a ser atualizado pelo ID
        $environment = SimooEnvironment::find($id);
        $environment->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Ambiente', $options = []);

        return redirect()->route('simoo.ambientes.index')->with('id',$id);
    }

}