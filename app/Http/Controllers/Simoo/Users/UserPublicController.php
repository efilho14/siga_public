<?php

# Classe UserPublicController
#### Controla o público dos usuários cadastrados no moodle.

namespace App\Http\Controllers\Simoo\Users;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Simoo\SimooUserPublic;
use App\Model\Simoo\SimooUserGroup;

## Others
use Validator;
use Input;
use Session;
use Notify;

class UserPublicController extends Controller
{
    public function create()
    {
        ## Permissão
        $this->authorize('userTargetPublic.create');

        ## View
        return view('simoo.usuarios.target.publicos.create');
    }

    public function store(request $request)
    {
        ## Permissão
        $this->authorize('userTargetPublic.create');

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome do Público');
        $rules = array('name' => 'required');
        $validator = Validator::make(Input::all(), $rules, $messsages);
        
        ## Validação
        if ($validator->fails()) :
            return redirect('simoo/usuarios/target')
            ->withErrors($validator)
            ->withInput();
        endif;

        ## Inserir no DB 'Simoo' na tabela 'user_public'
        $public_simoo = new SimooUserPublic();
        $public_simoo->name = $request->name;
        $public_simoo->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Público', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function edit(int $id)
    {
        ## Permissão
        $this->authorize('userTargetPublic.edit');

        ## Buscar o Público pelo $id
        $publics = SimooUserPublic::where('id', $id)->first();

        ## View
        return view('simoo.usuarios.target.publicos.edit')->withPublics($publics);
    }
    
    public function update(Request $request, $id)
    {
        ## Permissão
        $this->authorize('userTargetPublic.edit');

        ## Buscar o público a ser atualizado pelo ID
        $public = SimooUserPublic::find($id);

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome do Público');
        $rules = array('name' => 'required');
        $validator = Validator::make(Input::all(), $rules, $messsages);
        
        ## Validação
        if ($validator->fails()) :
            return redirect('simoo/usuarios/target')
            ->withErrors($validator)
            ->withInput();
        endif;

        ## Inserir as informações preenchidas no form
        $public->name = $request->name;
        $public->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Público', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('userTargetPublic.delete');

        ## Buscar o público a ser atualizado pelo ID
        $public = SimooUserPublic::find($id);
        $public->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Público', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function getName(request $request)
    {
        ## Buscar o public pelo NAME
        $public = SimooUserPublic::select('id', 'name')->where('name', $request->publicName)->first();

        ## Buscar o grupo pelo ID
        $group = SimooUserGroup::select('id', 'name')->where('publicid', $public->id)->get();
        
        return ($group);
    }
}