<?php

# Classe UserEnrolledController
#### Controla alunos, instrutores e coordenadores cadastrados no moodle e simoo

namespace App\Http\Controllers\Simoo\Users;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Model
use App\Model\Moodle\MoodleUser;
use App\Model\Moodle\MoodleEnrol;
use App\Model\Moodle\MoodleRole;
use App\Model\Moodle\MoodleContext;
use App\Model\Moodle\MoodleUserEnrolments;
use App\Model\Moodle\MoodleRoleAssignments;
use App\Model\Moodle\MoodleGradeItems;
use App\Model\Moodle\MoodleGradeGrades;
use App\Model\Simoo\SimooStudentFrequency;
use App\Model\Simoo\SimooCourse;

## Libraries
use App\Libraries\Moodle\MoodleLibStudent;
use App\Libraries\Moodle\MoodleLibTeacher;
use App\Libraries\Moodle\MoodleLibCoordinator;

## Helpers
use App\Helpers\General\ToolsGeneral;

class UserEnrolledController extends Controller
{
    public function all_students(Request $request)
    {
        ## Buscar os estudantes na Librarie
        $students = MoodleLibStudent::get([
            'courseid' => $request->courseId,
            'options' => [
                [
                    'name' => 'sortby',
                    'value' => 'firstname'
                ]
            ]
        ]);
        return $students['students'];
    }

    public function all_teachers(Request $request)
    {
        ## Buscar os professores na Librarie
        return MoodleLibTeacher::get($request->courseId);
    }

    public function all_coordinators(Request $request)
    {
        ## Buscar os coordenadores na Librarie
        return MoodleLibCoordinator::get($request->courseId);
    }

    public function insert_student(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.insertStudent');

        ## Buscar enrol do curso
        $enrol = MoodleEnrol::where('courseid',$request->courseId)
                            ->where('enrol','=','manual')->first();

        if(empty($enrol)):
            MoodleEnrol::insertEnrol($request->courseId, 5);

            $enrol = MoodleEnrol::where('courseid', $request->courseId)
                                ->where('enrol', '=', 'manual')->first();
        endif;

        foreach($request->userId as $user):
            $get_user_enrolments = MoodleUserEnrolments::select('mdl_user_enrolments.id', 'mdl_user_enrolments.userid' ,'mdl_enrol.enrol')
                                        ->join('mdl_enrol', 'mdl_user_enrolments.enrolid', '=', 'mdl_enrol.id')
                                        ->where('enrolid', $enrol->id)
                                        ->where('userid', $user)->first();
            
            if(empty($get_user_enrolments)):
                // Buscar context
                $contextId = MoodleContext::where('instanceid',$request->courseId)
                                          ->where('contextlevel','50')->first();

                // Inserir no mdl_user_enrolments
                MoodleUserEnrolments::insert([
                    'status' => 0, 
                    'enrolid' => $enrol->id,
                    'userid' => $user,
                    'timestart' => ToolsGeneral::strToTime(),
                    'timeend' => 0,
                    'modifierid' => 2,
                    'timecreated' => ToolsGeneral::strToTime(),
                    'timemodified' => ToolsGeneral::strToTime()
                ]);

                // Inserir no mdl_role_assignments
                MoodleRoleAssignments::insert([
                    'roleid' => 5, 
                    'contextid' => $contextId->id,
                    'userid' => $user,
                    'timemodified' => ToolsGeneral::strToTime(),
                    'modifierid' => 2,
                    'component' => '',
                    'itemid' => 0,
                    'sortorder' => 0
                ]);
            endif;                       
        endforeach;
    }

    public function insert_teacher(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.insertTeacher');

        ## Buscar enrol do curso
        $enrol = MoodleEnrol::where('courseid',$request->courseId)
                            ->where('enrol','=','manual')->first();

        ## Ver se o instrutor está matriculado no curso
        $get_user_enrolments = MoodleUserEnrolments::select('mdl_user_enrolments.id', 'mdl_user_enrolments.userid' ,'mdl_enrol.enrol')
                                        ->join('mdl_enrol', 'mdl_user_enrolments.enrolid', '=', 'mdl_enrol.id')
                                        ->where('enrolid', $enrol->id)
                                        ->where('userid', $request->userId[0])->first();

        if(empty($get_user_enrolments)):

            ## buscar context
            $contextId = MoodleContext::where('instanceid',$request->courseId)
                                        ->where('contextlevel','50')->first();

            ## buscar o role do curso
            $role = MoodleRole::where('shortname',$request->typeName)->first();

            ## inserir no mdl_user_enrolments
            MoodleUserEnrolments::insert([
                'status' => 0, 
                'enrolid' => $enrol->id,
                'userid' => $request->userId[0],
                'timestart' => ToolsGeneral::strToTime(),
                'timeend' => 0,
                'modifierid' => 2,
                'timecreated' => ToolsGeneral::strToTime(),
                'timemodified' => ToolsGeneral::strToTime()
            ]);

            ## inserir no mdl_role_assignments
            MoodleRoleAssignments::insert([
                'roleid' => $role->id, 
                'contextid' => $contextId->id,
                'userid' => $request->userId[0],
                'timemodified' => ToolsGeneral::strToTime(),
                'modifierid' => 2,
                'component' => '',
                'itemid' => 0,
                'sortorder' => 0
            ]);

        endif;
    }

    public function insert_coordinator(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.insertCoordinator');

        ## buscar enrol do curso
        $enrol = MoodleEnrol::where('courseid',$request->courseId)
                            ->where('enrol','=','manual')->first();

        ## ver se o instrutor está matriculado no curso
        $get_user_enrolments = MoodleUserEnrolments::select('mdl_user_enrolments.id', 'mdl_user_enrolments.userid' ,'mdl_enrol.enrol')
                                        ->join('mdl_enrol', 'mdl_user_enrolments.enrolid', '=', 'mdl_enrol.id')
                                        ->where('enrolid', $enrol->id)
                                        ->where('userid', $request->userId[0])->first();

        if(empty($get_user_enrolments)):

            ## buscar context
            $contextId = MoodleContext::where('instanceid',$request->courseId)
                                        ->where('contextlevel','50')->first();

            ## buscar o role do curso
            $role = MoodleRole::where('shortname','teacher')->first();

            ## inserir no mdl_user_enrolments
            MoodleUserEnrolments::insert([
                'status' => 0, 
                'enrolid' => $enrol->id,
                'userid' => $request->userId[0],
                'timestart' => ToolsGeneral::strToTime(),
                'timeend' => 0,
                'modifierid' => 2,
                'timecreated' => ToolsGeneral::strToTime(),
                'timemodified' => ToolsGeneral::strToTime()
            ]);

            ## inserir no mdl_role_assignments
            MoodleRoleAssignments::insert([
                'roleid' => $role->id, 
                'contextid' => $contextId->id,
                'userid' => $request->userId[0],
                'timemodified' => ToolsGeneral::strToTime(),
                'modifierid' => 2,
                'component' => '',
                'itemid' => 0,
                'sortorder' => 0
            ]);

        endif;
    }

    public function edit_teacher(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.editTeacher');

        $userOld = implode('',$request->userIdOld);
        $userNew = implode('',$request->userIdNew);

        ## buscar enrol do curso
        $enrol = MoodleEnrol::where('courseid',$request->courseId)
                            ->where('enrol','=','manual')->first();

        ## buscar context
        $contextId = MoodleContext::where('instanceid',$request->courseId)
                                  ->where('contextlevel','50')->first();

        ## pegar enrol - instrutor titular
        $role = MoodleRole::where('shortname', $request->roleShortname)->first();

        ## atualizar o MoodleRoleAssignments
        MoodleRoleAssignments::where('contextid',$contextId->id)
                            ->where('userid', $userOld)
                            ->where('roleid', $role->id)
                            ->update(['userid' => $userNew]);

        ## atualizar o MoodleUserEnrolments
        MoodleUserEnrolments::where('enrolid',$enrol->id)
                            ->where('userid', $userOld)
                            ->update(['userid' => $userNew]);
    }

    public function edit_coordinator(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.editCoordinator');

        $userOld = implode('',$request->userIdOld);
        $userNew = implode('',$request->userIdNew);

        ## buscar enrol do curso
        $enrol = MoodleEnrol::where('courseid',$request->courseId)
                            ->where('enrol','=','manual')->first();

        ## buscar context
        $contextId = MoodleContext::where('instanceid',$request->courseId)
                                  ->where('contextlevel','50')->first();

        ## pegar enrol - instrutor titular
        $role = MoodleRole::where('shortname', 'teacher')->first();

        ## atualizar o MoodleRoleAssignments
        MoodleRoleAssignments::where('contextid',$contextId->id)
                            ->where('userid', $userOld)
                            ->where('roleid', $role->id)
                            ->update(['userid' => $userNew]);

        ## atualizar o MoodleUserEnrolments
        MoodleUserEnrolments::where('enrolid',$enrol->id)
                            ->where('userid', $userOld)
                            ->update(['userid' => $userNew]);
    }

    public function remove_student(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.deleteStudent');

        ## chamada da librarie Student
        MoodleLibStudent::remove($request->userId, $request->courseId);
    }

    public function remove_teacher(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.deleteTeacher');

        ## buscar enrol do curso
        $enrol = MoodleEnrol::where('courseid',$request->courseId)
                            ->where('enrol','=','manual')->first();

        ## buscar context
        $contextId = MoodleContext::where('instanceid',$request->courseId)
                                  ->where('contextlevel','50')->first();

        ## remover usuario no role_assignments
        MoodleRoleAssignments::where('contextid',$contextId->id)
                             ->where('userid',$request->userId)->delete();

        ## remover usuario no user_enrolments
        MoodleUserEnrolments::where('enrolid', $enrol->id)
                                ->where('userid', $request->userId)->delete();


        ## retorna qual ou quais os instrutores que ainda estão matriculados
        $getTeachers = MoodleRoleAssignments::select('mdl_role.shortname', 'mdl_role_assignments.userid')
                                ->join('mdl_role', 'mdl_role_assignments.roleid','=','mdl_role.id')
                                ->where('mdl_role_assignments.contextid',$contextId->id)
                                ->whereIn('mdl_role.shortname',['editingteacherone','editingteachertwo'])->get();

        if(count($getTeachers)):
            ## pegar enrol - instrutor titular
            $role = MoodleRole::where('shortname','editingteacherone')->first();

            foreach($getTeachers as $teacher):
                MoodleRoleAssignments::where('userid',$teacher->userid)
                             ->update(['roleid' => $role->id]);
            endforeach;
        endif;
    }

    public function remove_coordinator(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.deleteCoordinator');

        ## buscar enrol do curso
        $enrol = MoodleEnrol::where('courseid',$request->courseId)
                            ->where('enrol','=','manual')->first();

        ## buscar context
        $contextId = MoodleContext::where('instanceid',$request->courseId)
                                  ->where('contextlevel','50')->first();

        ## remover usuario no role_assignments
        MoodleRoleAssignments::where('contextid',$contextId->id)
                             ->where('userid',$request->userId)->delete();

        ## remover usuario no user_enrolments
        MoodleUserEnrolments::where('enrolid', $enrol->id)
                                ->where('userid', $request->userId)->delete();       
    }

    /*
        O terceiro item de insertGrades não pode ser somente 2, pois será o id do 
        usuario que cadastrou a nota
    */
    public function manager_grades(Request $request)
    {
        ## Permissão
        $this->authorize('userEnrolled.gradeFrequency');

        /* GRADE */

        $items = [];
        $gradeItems = MoodleGradeItems::where('courseid', $request->courseId)
                                        ->orderBy('id','desc')->get();

        ## verifica se tem algum avaliação final cadastrada
        if(count($gradeItems)):

            ## pegar o id do gradeItem e inclui em um array
            foreach($gradeItems as $itemId):
                array_push($items, $itemId->id);
            endforeach;

            ## criar array para usuarios com nota
            foreach($request->manager[0] as $key => $userId):

                ## verifica se o valor da nota do aluno é NULL
                if($request->manager[1][$key] == NULL):

                    MoodleGradeGrades::where('userid', $userId)
                                     ->whereIn('itemid', $items)->delete();

                ## se tiver valor, o mesmo irá atualizar ou inserir
                else:

                    ## busca os alunos que tem notas cadastradas
                    $grades = MoodleGradeGrades::where('userid', $userId)
                                ->whereIn('itemid', $items)->get();

                    ## se o estudante tive nota, irá somente atualizar
                    if(count($grades)):
                        
                        foreach($grades as $grade):
                            MoodleGradeGrades::where('itemid', $grade->itemid)
                                            ->where('userid', $userId)
                                            ->update(['finalgrade' => $request->manager[1][$key]]);
                        endforeach;
                    
                    ## se o estudante não tiver nota, o sistema irá inserir
                    else:  
                        MoodleGradeGrades::insertGrades($userId, $items, 2, $request->manager[1][$key], ToolsGeneral::strToTime());
                    endif;

                endif;

            endforeach;
            
            echo 'Success: Nota inserida ou atualizada com sucesso.';
        else:
            echo 'Error: Não existe nenhum registro em "GradeItems" para este curso.';
        endif;

        /* FREQUENCY */

        $course = SimooCourse::where('instanceid',$request->courseId)->first();

        ## verifica se o curso está cadastrado
        if(count($course)):
            
            ## criar array para usuarios com nota
            foreach($request->manager[0] as $key => $userId):

                ## verifica se o valor da frequência do aluno é NULL
                if($request->manager[2][$key] == NULL):

                    SimooStudentFrequency::where('courseid', $course->id)
                        ->where('userid', $userId)->delete();
                
                ## se tiver valor, o mesmo irá atualizar ou inserir
                else:

                    ## verifica se tem alguma frequencia cadastrada
                    $frequency = SimooStudentFrequency::where('courseid', $course->id)
                                        ->where('userid',$userId)->first();

                    if(count($frequency)):
                    SimooStudentFrequency::where('courseid', $course->id)
                        ->where('userid', $userId)->update([
                            'frequency' => $request->manager[2][$key],
                            'updated_at' => date('Y-m-d h:i:s')
                        ]);
                    else:
                        SimooStudentFrequency::insert([
                            'courseid' => $course->id,
                            'userid' => $userId,
                            'frequency' => $request->manager[2][$key],
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    endif;

                endif;

            endforeach;

            echo 'Success: Frequência inserida ou atualizada com sucesso.';
        else:
            echo 'Error: Não existem nenhum curso cadastrado com este ID.';
        endif;
    }
}