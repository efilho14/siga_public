<?php

# Classe UserTypeController
#### Controla o tipo dos usuários cadastrados no moodle.

namespace App\Http\Controllers\Simoo\Users;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Simoo\SimooUserType;
use App\Model\Simoo\SimooUserGroup;

## Others
use Validator;
use Input;
use Session;
use Notify;

class UserTypeController extends Controller
{
    public function create()
    {
        ## Permissão
        $this->authorize('userTargetType.create');

        ## Busca os grupos criados
        $groups = SimooUserGroup::orderBy('name', 'asc')->get();

        ## View
        return view('simoo.usuarios.target.tipos.create')->withGroups($groups);
    }

    public function store(request $request)
    {
        ## Permissão
        $this->authorize('userTargetType.create');

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome do Tipo');
        $rules = array('name' => 'required');
        $validator = Validator::make(Input::all(), $rules, $messsages);
        
        ## Validação
        if ($validator->fails()) :
            return redirect('simoo/usuarios/target')
            ->withErrors($validator)
            ->withInput();
        endif;

        ## Inserir no DB 'Simoo' na tabela 'user_type'
        $type = new SimooUserType();
        $type->name = $request->name;
        $type->groupid = $request->groupid;
        $type->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Tipo', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function edit(int $id)
    {
        ## Permissão
        $this->authorize('userTargetType.edit');

        ## Buscar o Tipo pelo $id
        $type = SimooUserType::where('id', $id)->first();

        ## Busca todos os grupos
        $groups = SimooUserGroup::orderBy('name', 'asc')->get();

        ## View
        return view('simoo.usuarios.target.tipos.edit')->withGroups($groups)->withType($type);
    }

    public function update(Request $request, $id)
    {
        ## Permissão
        $this->authorize('userTargetType.edit');

        ## Buscar o tipo a ser atualizado pelo ID
        $type = SimooUserType::find($id);

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome do Tipo');
        $rules = array('name' => 'required');
        $validator = Validator::make(Input::all(), $rules, $messsages);
        
        ## Validação
        if ($validator->fails()) :
            return redirect('simoo/usuarios/target')
            ->withErrors($validator)
            ->withInput();
        endif;

        ## Inserir as informações preenchidas no form
        $type->name = $request->name;
        $type->groupid = $request->groupid;
        $type->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Tipo', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('userTargetType.delete');

        ## Buscar o público a ser atualizado pelo ID
        $type = SimooUserType::find($id);
        $type->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Tipo', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }
}