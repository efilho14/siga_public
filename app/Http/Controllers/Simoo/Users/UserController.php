<?php

# Classe UserController
#### Controla todos os usuários cadastrados no moodle.

namespace App\Http\Controllers\Simoo\Users;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Moodle\MoodleUser;
use App\Model\Simoo\SimooUserPublic;

## Libraries
use App\Libraries\Moodle\MoodleLibUser;
use App\Libraries\Simoo\SimooLibInit;

## Helpers
use App\Helpers\General\ToolsGeneral;

## Others
use Validator;
use Input;
use Session;
use Notify;
use App\Libraries\Moodle\MoodleLibCourse;

class UserController extends Controller
{

    public function index()
    {
        ## Permissão
        $this->authorize('user.index');

        ## Buscar os usuários
        $users = MoodleLibUser::GetAll();

        ## View
        return view('simoo.usuarios.index')->withUsers($users);
    }   

    public function show(int $userId)
    {
        ## Permissão
        $this->authorize('user.show');

        ## Chamar o INIT do presencial
        SimooLibInit::presential();

        ## Buscar usuário pelo ID
        $users = MoodleLibUser::GetUserIdCourse($userId);

        ## View
        return view('simoo.usuarios.show')->withUsers($users);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('user.create');

        ## Buscar públicos
        $publics = SimooUserPublic::orderBy('name', 'desc')->get();

        ## View
        return view('simoo.usuarios.create')->withPublics($publics);
    }

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('user.create');

        ## Validar os campos
        $this->validate($request, array(
            'firstname'   => 'required',
            'lastname' => 'required',
            'username' => 'required',
            'email' => 'required',
            'public' => 'required',
            'group' => 'required',
            'type' => 'required',
            'password' => 'required',
        ));

        ## Chamada da API - courseCategories => Create
        $result = MoodleLibUser::create([
            'username'  => ToolsGeneral::removeMask($request->username),
            'password'  => $request->password,
            'createpassword' => 1,
            'firstname' => mb_strtoupper($request->firstname),
            'lastname'  => mb_strtoupper($request->lastname),
            'email'     => $request->email,
            'customfields' => [
                [
                    'type' => 'registry',
                    'value' => $request->registry
                ],
                [
                    'type' => 'cellphone',
                    'value' => ToolsGeneral::removeMask($request->cellphone)
                ],
                [
                    'type' => 'public',
                    'value' => $request->public
                ],
                [
                    'type' => 'group',
                    'value' => $request->group
                ],
                [
                    'type' => 'type',
                    'value' => $request->type
                ],
            ]
        ]);

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Usuário', $options = []);

        return redirect()->route('simoo.usuarios.index');
    }

    public function edit(int $userId)
    {
        ## Permissão
        $this->authorize('user.edit');

        ## Buscar usuário pelo ID
        $users = MoodleLibUser::GetId(['key' => 'id', 'value' => $userId]);

        $publics = SimooUserPublic::orderBy('name', 'desc')->get();

        ## View
        return view('simoo.usuarios.edit')->withUsers($users)->withPublics($publics);
    }

    public function update(Request $request, int $userId)
    {
        ## Permissão
        $this->authorize('user.edit');

        ## Chamar a API de atualização
        $data = [
            'id' => $userId,
            'firstname' => mb_strtoupper($request->firstname),
            'lastname' => mb_strtoupper($request->lastname),
            'username' => ToolsGeneral::removeMask($request->username),
            'email' => $request->email,
            'customfields' => [
                [
                    'type' => 'registry',
                    'value' => $request->registry
                ],
                [
                    'type' => 'cellphone',
                    'value' => ToolsGeneral::removeMask($request->cellphone)
                ],
                [
                    'type' => 'public',
                    'value' => $request->public
                ],
                [
                    'type' => 'group',
                    'value' => $request->group
                ],
                [
                    'type' => 'type',
                    'value' => $request->type
                ],
            ]
        ];

        if(!is_null($request->password)):
            $data += ['password' => $request->password];
        endif;

        ## Atualizar usuário
        MoodleLibUser::Update($data);

        ## Atualizar info_data do usuário
        //MoodleLibUser::UpdateInfoData(['matricula'], [$userId], [$request->matricula]);

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Usuário', $options = []);

        return redirect()->route('simoo.usuarios.index');
    }

    public function destroy($id)
    {
        ## Permissão
        $this->authorize('user.delete');

        ## Chama a API para deletar o usuário
        MoodleLibUser::Delete($id);

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Usuário', $options = []);

        return redirect()->route('simoo.usuarios.index');
    }

    public function search(Request $request)
    {
        ## Permissão
        $this->authorize('user.search');

        ## Usuários que estão matriculados na turma
        $notUsers = [];

        foreach($request->users as $user):
            foreach($user[1] as $value):
                array_push($notUsers,$value);
            endforeach;
        endforeach;

        if(empty($request->nameUser)):

            $user = MoodleUser::select('id','username','firstname','lastname','email')
                    ->whereNotIn('username',['admin','guest'])
                    ->whereNotIn('id',$notUsers)
                    ->where('deleted', 0)
                    ->orderBy('firstname', 'ASC')
                    ->orderBy('lastname', 'ASC')->paginate(5);

        elseif(is_numeric($request->nameUser)):

            $user = MoodleUser::select('id','username','firstname','lastname','email')
                    ->where('username','like', trim($request->nameUser).'%')
                    ->whereNotIn('username',['admin','guest'])
                    ->whereNotIn('id',$notUsers)
                    ->where('confirmed','=',1)
                    ->where('deleted', 0)
                    ->orderBy('mdl_user.firstname', 'ASC')
                    ->orderBy('mdl_user.lastname', 'ASC')->paginate(5);
                    
        else:

            $user = MoodleUser::select('id','username','firstname','lastname','email')
                    ->where('firstname','like','%'.trim($request->nameUser).'%')
                    ->whereNotIn('username',['admin','guest'])
                    ->whereNotIn('id',$notUsers)
                    ->where('confirmed','=',1)
                    ->where('deleted', 0)
                    ->orderBy('mdl_user.firstname', 'ASC')
                    ->orderBy('mdl_user.lastname', 'ASC')->paginate(5);
        endif;

        return $user->toArray();
    }
}