<?php

# Classe UserGroupController
#### Controla o grupo dos usuários cadastrados no moodle.

namespace App\Http\Controllers\Simoo\Users;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Simoo\SimooUserPublic;
use App\Model\Simoo\SimooUserGroup;
use App\Model\Simoo\SimooUserType;

## Others
use Validator;
use Input;
use Session;
use Notify;

class UserGroupController extends Controller
{
    public function create()
    {
        ## Permissão
        $this->authorize('userTargetGroup.create');

        ## Busca os públicos criados
        $publics = SimooUserPublic::orderBy('name','asc')->get();

        ## View
        return view('simoo.usuarios.target.grupos.create')->withPublics($publics);
    }

    public function store(request $request)
    {
        ## Permissão
        $this->authorize('userTargetGroup.create');

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome do Grupo');
        $rules = array('name' => 'required');
        $validator = Validator::make(Input::all(), $rules, $messsages);
        
        ## Validação
        if ($validator->fails()) :
            return redirect('simoo/usuarios/target')
            ->withErrors($validator)
            ->withInput();
        endif;

        ## Inserir no DB 'Simoo' na tabela 'user_group'
        $group = new SimooUserGroup();
        $group->name = $request->name;
        $group->publicid = $request->publicid;
        $group->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Grupo', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function edit(int $id)
    {
        ## Permissão
        $this->authorize('userTargetGroup.edit');

        ## Buscar o Grupo pelo $id
        $group = SimooUserGroup::where('id', $id)->first();

        ## Busca todos os públicos
        $publics = SimooUserPublic::orderBy('name', 'asc')->get();

        ## View
        return view('simoo.usuarios.target.grupos.edit')->withPublics($publics)->withGroup($group);
    }

    public function update(Request $request, $id)
    {
        ## Permissão
        $this->authorize('userTargetGroup.edit');

        ## Buscar o grupo a ser atualizado pelo ID
        $group = SimooUserGroup::find($id);

        ## Variaveis iniciais
        $messsages = array('name.required' => 'Preencha o Nome do Público');
        $rules = array('name' => 'required');
        $validator = Validator::make(Input::all(), $rules, $messsages);
        
        ## Validação
        if ($validator->fails()) :
            return redirect('simoo/usuarios/target')
            ->withErrors($validator)
            ->withInput();
        endif;

        ## Inserir as informações preenchidas no form
        $group->name = $request->name;
        $group->publicid = $request->publicid;
        $group->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Grupo', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('userTargetGroup.delete');

        ## Buscar o grupo a ser atualizado pelo ID
        $group = SimooUserGroup::find($id);
        $group->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Grupo', $options = []);

        return redirect()->route('simoo.usuarios.target.index');
    }

    public function getName(request $request)
    {
        ## Buscar o group pelo NAME
        $group = SimooUserGroup::select('id', 'name')->where('name', $request->groupName)->first();

        ## Buscar o tipo pelo ID
        $type = SimooUserType::select('id', 'name')->where('groupid', $group->id)->get();
        
        return ($type);
    }
}