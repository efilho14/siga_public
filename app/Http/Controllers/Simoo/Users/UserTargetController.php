<?php

# Classe UserTargetController
#### Controla os públicos-alvo dos usuários cadastrados no moodle.

namespace App\Http\Controllers\Simoo\Users;

## Controller
use App\Http\Controllers\Controller;

## Libraries
use App\Libraries\Simoo\SimooLibTarget;


class UserTargetController extends Controller
{
    public function index()
    {
        ## Permissão
        $this->authorize('userTarget.index');

        ## Chamar a Librarie 'Target' para buscar a união de Pública, Grupo e Tipo
        $target = SimooLibTarget::Full();

        ## View
        return view('simoo.usuarios.target.index')->withTarget($target);
    }
}