<?php

# Classe UserController
#### Controla as categorias cadastradas no moodle.

namespace App\Http\Controllers\Simoo\Course;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Moodle\MoodleCourseCategory;
use App\Model\Moodle\MoodleContext;

## Libraries
use App\Libraries\Moodle\MoodleLibCourseCategories;

## Helpers
use App\Helpers\Moodle\ToolsMoodleCategory;
use App\Helpers\Moodle\ToolsMoodleContext;
use App\Helpers\General\ToolsGeneral;

## Others
use Session;
use Notify;

class CourseCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('courseCategory.index');

        $category = MoodleCourseCategory::allCategories();
        return view('simoo.eventos.index')->withCategory($category);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('courseCategory.create');

        $parents = ToolsMoodleCategory::orderCategoryLine();
        return view('simoo.eventos.create')->withParents($parents);
    }

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('course.config.create');

        ## Validar os campos
        $this->validate($request, array(
            'name'   => 'required',
            'parent' => 'required'
        ));

        ## Request visible
        if($request->visible == 1):
            $visible = $request->visible;
        elseif($request->visible == ''):
            $visible = 0;
        endif;

        ## Chamada da API - courseCategories => Create
        $result = MoodleLibCourseCategories::create([
            'name'              => $request->name,
            'parent'            => $request->parent,
            'description'       => $request->description,
            'descriptionformat' => 1
        ]);

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Evento', $options = []);

        return redirect()->route('simoo.eventos.index');
    }

    public function edit(int $id)
    {
        ## Permissão
        $this->authorize('courseCategory.edit');

        $category = MoodleCourseCategory::find($id);
        $parents = ToolsMoodleCategory::orderCategoryLine($category->id);
        return view('simoo.eventos.edit')->withCategory($category)->withParents($parents);
    }

    public function update(Request $request, int $id)
    {
        ## Permissão
        $this->authorize('courseCategory.edit');

        ## Buscar o evento a ser atualizado pelo ID
        $category = MoodleCourseCategory::find($id);

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required',
            'parent' => 'required',
        ));
        
        ## Chamada da API - courseCategories => Update
        MoodleLibCourseCategories::update([
            'id'                => $id,
            'name'              => $request->name,
            'parent'            => $request->parent,
            'description'       => $request->description,
            'descriptionformat' => 1
        ]);

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Evento', $options = []);

        return redirect()->route('simoo.eventos.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('courseCategory.destroy');

        ## Chamada da API - courseCategories => Delete
        MoodleLibCourseCategories::delete([
            'id'        => $id,
            'recursive' => 1
        ]);

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Evento', $options = []);

        return redirect()->route('simoo.eventos.index');
    }
}
