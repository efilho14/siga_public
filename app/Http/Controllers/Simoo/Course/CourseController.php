<?php

namespace App\Http\Controllers\Simoo\Course;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Moodle\MoodleCourseCategory;
use App\Model\Moodle\MoodleCourse;
use App\Model\Moodle\MoodleQuestionnaireSurvey;
use App\Model\Moodle\MoodleQuestionnaireQuestion;
use App\Model\Moodle\MoodleQuestionnaireQuestChoice;
use App\Model\Simoo\SimooCourseConfig;
use App\Model\Simoo\SimooCourse;
use App\Model\Simoo\SimooCourseDates;

## Helpers
use App\Helpers\Moodle\ToolsMoodleCategory;
use App\Helpers\General\ToolsGeneral;

## Libraries
use App\Libraries\Moodle\MoodleLibCourse;
use App\Libraries\Moodle\MoodleLibStudent;
use App\Libraries\Moodle\MoodleLibTeacher;
use App\Libraries\Moodle\MoodleLibQuiz;
use App\Libraries\Moodle\MoodleLibQuestionnaire;
use App\Libraries\Moodle\MoodleLibCertificate;
use App\Libraries\Moodle\MoodleLibEnvironment;
use App\Libraries\Simoo\SimooLibCourse;

## Others
use Validator;
use Input;
use Session;
use Notify;
use Gate;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($year=null, $qtyMouth=null)
    {
        ## Permissão
        $this->authorize('course.index');

        ## Setar o ano selecionado
        $year_selected = date('Y');
        $years = [
            '2018' => ['actual' => 'N'], 
            '2019' => ['actual' => 'N']
        ];

        ## Verifica se tem algum ano setado e busca os meses
        if($year):
            if(isset($years[$year])):
                $years[$year]['actual'] = 'Y';
                $year_selected = $year;

                if($year == date('Y') && $qtyMouth == null): //fazer algo aqui para detectar a quantidade (list/2018/12) de meses a mostrar
                    $courses = MoodleLibCourse::GetCourseMouth(3, $year_selected);
                else:
                    $courses = MoodleLibCourse::GetCourseMouth(12, $year_selected);
                endif;
            else:
                return redirect()->route('simoo.turmas.index', $year_selected);
            endif;
        else:
            if(isset($years[$year_selected])):
                $years[$year_selected]['actual'] = 'Y';
                $courses = MoodleLibCourse::GetCourseMouth(3, $year_selected);
            endif;
        endif;       

        return view('simoo.turmas.index')
                ->withCourses($courses)
                ->withYears($years)
                ->withYearCurrent($year_selected)
                ->withQtyMouth($qtyMouth);
    }

    public function show($id)
    {
        ## Permissão
        $this->authorize('course.show');

        ## Procurar o curso pelo $id solicitado
        $courseMoodle = MoodleCourse::find($id);
        $courseSimoo = SimooCourse::where('instanceid', $id)->first();

        ## Verifica se existe algum curso com o ID solicitado
        if (empty($courseMoodle))
            return redirect()->route('simoo.turmas.create');

        ## Frequência e Nota padrão para o curso
        if ($courseSimoo->config == 1) :
            $config = SimooCourseConfig::getConfig();
        else :
            $config = $courseSimoo;
        endif;

        ## Pesquisa a última data do curso
        $courseLastDate = SimooCourseDates::select('date')->where('courseid', $courseSimoo->id)->orderBy('date', 'desc')->limit(1)->get();

        ## Pesquisa se existe avaliação de reação
        $questionnaire = MoodleQuestionnaireQuestion::getJoinSurvey($courseMoodle->id);

        ## Redirect
        return view('simoo.turmas.show')->withCourseMoodle($courseMoodle)
            ->withCourseSimoo($courseSimoo)
            ->withCourseLastDate($courseLastDate[0])
            ->withQuestionnaire($questionnaire)
            ->withConfig($config);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('course.create');

        ## Chama o ordenamento da categoria em linha e as configurações padrões do Simoo
        $category = ToolsMoodleCategory::orderCategoryLine();
        $config = SimooCourseConfig::getConfig();

        if(empty($config))
            return redirect()->route('simoo.turmas.config');

        ## União da Tabela 'environment' e 'Room'
        $environment = MoodleLibEnvironment::union();

        return view('simoo.turmas.create')->withCategory($category)->withConfig($config)->withEnvironment($environment);
    }

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('course.create');

        ## Mensagens, regras e inputs
        $messsages = array(
            'idnumber.required'   => 'Preencha o Número da Turma', 
            'category.required'   => 'Selecione a Categoria da Turma',
            'workload.required'   => 'Preencha a Carga Horária', 
            'fe_default.required' => 'Selecione se Deseja Manter as Configurações Padrões',
            'dates.required'      => 'Preencha um Período', 
            'hourinit.required'   => 'Preencha o Horário de Início da Turma',
            'hourend.required'    => 'Preencha o Horário de Término da Turma',
        );	
        $rules = array(
            'idnumber'   => 'required', 
            'category'   => 'required',
            'workload'   => 'required',
            'fe_default' => 'required', 
            'dates'      => 'required',
            'hourinit'   => 'required',
            'hourend'    => 'required'
        );
	    $validator = Validator::make(Input::all(), $rules,$messsages);
        
        ## Validação
        if($validator->fails())
            return redirect('simoo/turmas/create')
                        ->withErrors($validator)
                        ->withInput();

        ## Chamar categoria
        $category = MoodleCourseCategory::where('id', $request->category)->first();

        ## Chamada da API - course => Create
        $course_moodle = MoodleLibCourse::create([
            'fullname'          => $category['name'],
            'shortname'         => $request->idnumber,
            'categoryid'        => $request->category,
            'idnumber'          => $request->idnumber,
            'summary'           => $request->summary,
            'summaryformat'     => 1,
            'format'            => 'topics',
            'showgrades'        => 1,
            'newsitems'         => 0,
            'startdate'         => ToolsGeneral::strToTimeExplode($request->dates),
            'enddate'           => 0,
            'maxbytes'          => 0,
            'showreports'       => 1,
            'visible'           => $request->visible,
            'groupmode'         => 0,
            'groupmodeforce'    => 0,
            'defaultgroupingid' => 0,
            'enablecompletion'  => 1,
            'completionnotify'  => 0
        ]);

        ## Cria a frequência
        MoodleLibQuiz::create($course_moodle['id'], 'Frequência', 0, 100.00000);

        ## Cria a avaliação final
        MoodleLibQuiz::create($course_moodle['id'], 'Avaliação Final', 1, 10.00000);

        ## Cria a avaliação de reação
        MoodleLibQuestionnaire::create($course_moodle['id'], 'Avaliação de Reação');
        
        ## Verifica a frequência e nota padrão ou customizada
        if($request->fe_default == 0):
            $frequency = $request->frequency;
            $grade = $request->grade;

            // Cria o certificado
            MoodleLibCertificate::create($course_moodle['id'], 'Emitir Certificado', $request->workload, $frequency, $grade);
        else:
            $frequency = null;
            $grade = null;

            $courseConfig = SimooCourseConfig::select('*')->first();

            // Cria o certificado
            MoodleLibCertificate::create($course_moodle['id'], 'Emitir Certificado', $request->workload, $courseConfig->frequency, $courseConfig->grade);
        endif;

        ## Cria o curso no db 'Simoo'
        SimooLibCourse::create([
            'instanceid' => $course_moodle['id'],
            'config'     => $request->fe_default,
            'workload'   => $request->workload,
            'location'   => $request->location,
            'frequency'  => $frequency,
            'grade'      => $grade,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ], $request->dates, $request->hourinit, $request->hourend, $request->block);
        
        ## Session e redirect
        Notify::success('Incluída com sucesso!', 'Turma', $options = []);

        ## Redirect
        return redirect()->route('simoo.turmas.show', $course_moodle['id']);
    }

    public function edit($id)
    {
        ## Permissão
        $this->authorize('course.edit');

        ## Procurar o curso pelo $id solicitado
        $courseMoodle = MoodleCourse::find($id);
        $courseSimoo = SimooCourse::where('instanceid', $id)->first();

        ## Buscar datas do curso
        $courseDates = SimooCourseDates::where('courseid', $courseSimoo->id)
                            ->orderBy('block','ASC')
                            ->orderBy('date','ASC')->get();

        ## Defini os blocos de períodos
        $dates = ['block' => []];
        foreach($courseDates as $key => $date):

            $dates['block'] += [
                $date->block => [
                    'keyblock' => $date->block,
                    'dateinit' => '',
                    'dateend'  => '',
                    'hourinit' => '',
                    'hourend'  => ''
                ]
            ];

            // Verifca se esta no bloco correto e inseri a data inicial
            if( $dates['block'][$date->block]['keyblock'] == $date->block AND
                empty($dates['block'][$date->block]['dateinit']) ):

                $dates['block'][$date->block]['dateinit'] = $date->date;
                $dates['block'][$date->block]['hourinit'] = $date->hourinit;
                $dates['block'][$date->block]['hourend']  = $date->hourend;
            endif;
            
            // Verifca se esta no bloco correto e inseri a data final
            if( $dates['block'][$date->block]['keyblock'] == $date->block AND
                empty($dates['block'][$date->block]['dateend']) ):

                $dates['block'][$date->block]['dateend'] = $date->date;
            endif;

            // Atualiza a data final
            if( $dates['block'][$date->block]['keyblock'] == $date->block AND
                $dates['block'][$date->block]['dateend'] ):

                $dates['block'][$date->block]['dateend'] = $date->date;
            endif;
 
        endforeach;        

        ## Buscar as categorias do curso
        $category = ToolsMoodleCategory::orderCategoryLine();

        ## Buscar as configurações do 
        $config = SimooCourseConfig::getConfig();

        ## União da Tabela 'environment' e 'Room'
        $environment = MoodleLibEnvironment::union();

        ## Redirect
        return view('simoo.turmas.edit')->withCourseMoodle($courseMoodle)
                        ->withCourseSimoo($courseSimoo)->withCourseDates($dates)
                        ->withEnvironment($environment)->withCategory($category)
                        ->withConfig($config);
    }
    
    public function update(Request $request, $id)
    {
        ## Permissão
        $this->authorize('course.edit');

        ## Procurar o curso pelo $id solicitado
        $courseMoodle = MoodleCourse::find($id);
        $courseSimoo = SimooCourse::where('instanceid', $id)->first();

        ## Mensagens, regras e inputs
        $messsages = array(
            'idnumber.required'   => 'Preencha o Número da Turma', 
            'category.required'   => 'Selecione a Categoria da Turma',
            'workload.required'   => 'Preencha a Carga Horária', 
            'fe_default.required' => 'Selecione se Deseja Manter as Configurações Padrões',
            'dates.required'      => 'Preencha um Período', 
            'hourinit.required'   => 'Preencha o Horário de Início da Turma',
            'hourend.required'    => 'Preencha o Horário de Término da Turma',
        );	
        $rules = array(
            'idnumber'   => 'required', 
            'category'   => 'required',
            'workload'   => 'required',
            'fe_default' => 'required', 
            'dates'      => 'required',
            'hourinit'   => 'required',
            'hourend'    => 'required',
        );
	    $validator = Validator::make(Input::all(), $rules,$messsages);
        
        ## Validação
        if ($validator->fails())
            return redirect('simoo/turmas/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();

        ## Chamar categoria
        $category = MoodleCourseCategory::where('id', $request->category)->first();

        ## Chamada da API - course => Update
        MoodleLibCourse::update([
            'id'         => $id,
            'fullname'   => $category['name'],
            'shortname'  => $request->idnumber,
            'categoryid' => $request->category,
            'idnumber'   => $request->idnumber,
            'summary'    => $request->summary,
            'startdate'  => ToolsGeneral::strToTimeExplode($request->dates),
            'visible'    => $request->visible
        ]);

        ## Chamada Librarie - course => Update
        if($request->fe_default == 0):
            $frequency = $request->frequency;
            $grade = $request->grade;
        else:
            $frequency = NULL;
            $grade = NULL;
        endif;

        SimooLibCourse::update([
            'config'    => $request->fe_default,
            'workload'  => $request->workload,
            'location'  => $request->location,
            'frequency' => $frequency,
            'grade'     => $grade,
        ], $courseSimoo->id, $request->dates, $request->hourinit, $request->hourend, $request->block);

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Turma', $options = []);

        return redirect()->route('simoo.turmas.index');
    }

    public function destroy($id)
    {
        ## Permissão
        $this->authorize('course.delete');

        ## Chamada Librarie - course => Delete
        MoodleLibCourse::delete($id);

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Turma', $options = []);

        return redirect()->route('simoo.turmas.index');
    }

    public function rate(Request $request, $courseId)
    {
        ## Pesquisa se existe avaliação de reação
        $questionnaire = MoodleQuestionnaireQuestion::getJoinSurvey($courseId);

        ## Buscar os alunos dos cursos
        $courseStudent = MoodleLibStudent::get([
            'courseid' => $courseId,
            'options' => [
                [
                    'name' => 'sortby',
                    'value' => 'firstname'
                ]
            ]
        ], 'Y');

        ## Buscar os instrutores dos cursos
        $courseTeacher = MoodleLibTeacher::get($courseId);

        ## Verifica se o formulário está vazio
        if(count($questionnaire)==0):

            // Pesquisa o id do survey
            $questSurvey = MoodleQuestionnaireSurvey::select('id')->where('courseid', $courseId)->first();

            // Inseri as questões da avaliação de reação
            MoodleQuestionnaireQuestion::insertQuestion($questSurvey['id'], $courseTeacher);

            // Inseri as opções de escolha das questões
            MoodleQuestionnaireQuestChoice::insertQuestChoice($courseId);

            // Session e redirect
            Notify::success('O registro da Avaliação de Reação e o e-mail para os alunos foram enviados com sucesso!', 'Registro e E-mail', $options = []);
        else:

            // Session e redirect
            Notify::success('Enviado com sucesso para os alunos!', 'E-mail - Avaliação de Reação', $options = []);
        endif;

        ## Envia o link da avaliação de reação para o aluno

        /* INSERIR A FUNÇÃO DE EMAIL AQUi  */



        return redirect()->route('simoo.turmas.show', $courseId);
    }

    public function frequency(int $id, int $numberPage=1)
    {
        ## Chama Librarie das informações dos cursos
        $course = MoodleLibCourse::Full(['ids' => [$id]])[0];

        ## Página que será chamada
        if($numberPage == 0):
            $page = 0;
            return redirect()->route('simoo.turmas.frequency', ['id' => $id, 'numberPage' => 1]);
        else:
            $page = ($numberPage - 1);
        endif;

        ## Verifica a quantidade de páginas da lista de frequência
        $qttPages = count($course['dates']['dateFull']);
        if($numberPage > $qttPages):
            $page = 0;
            return redirect()->route('simoo.turmas.frequency', ['id' => $id, 'numberPage' => 1]);
        endif;

        return view('simoo.turmas.relatorios.student_frequency')->withCourse($course)->withPage($page);
    }

    public function student_general(int $id)
    {
        ## Chama Librarie das informações dos cursos
        $course = MoodleLibCourse::Full(['ids' => [$id]])[0];

        return view('simoo.turmas.relatorios.student_general')->withCourse($course);
    }

    public function student_grade(int $id)
    {
        ## Chama Librarie das informações dos cursos
        $course = MoodleLibCourse::Full(['ids' => [$id]])[0];

        return view('simoo.turmas.relatorios.student_grade')->withCourse($course);
    }

    public function student_grade_frequency(int $id)
    {
        ## Chama Librarie das informações dos cursos
        $course = MoodleLibCourse::Full(['ids' => [$id]])[0];

        //print_r($course);

        return view('simoo.turmas.relatorios.student_grade_frequency')->withCourse($course);
    }
}
