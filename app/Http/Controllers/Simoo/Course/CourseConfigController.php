<?php

/*
    Classe CourseConfigController
    - Controla as configurações de um evento/turma
*/

namespace App\Http\Controllers\Simoo\Course;

//controller
use App\Http\Controllers\Controller;

//request
use Illuminate\Http\Request;

//model
use App\Model\Simoo\SimooCourseConfig;

//others
use Session;
use Notify;

class CourseConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('course.config.index');

        $config = SimooCourseConfig::getConfig();

        if(empty($config)):
            $config = new SimooCourseConfig;
            $config->frequency = 70;
            $config->grade = 6;
            $config->save();
        endif;

        $config = SimooCourseConfig::getConfig();
        return view('simoo.turmas.config')->withConfig($config);
    }

    public function update(Request $request, $idConfig)
    {
        ## Permissão
        $this->authorize('course.config.update');

        $config = SimooCourseConfig::find($idConfig);

        //validar os campos
        $this->validate($request, array(
            'frequency' => 'required',
            'grade' => 'required'
        ));

        //inserir as informações preenchidas no form
        $config->frequency = $request->frequency;
        $config->grade = $request->grade;
        $config->save();

        //session e redirect
        Notify::success('Editado com sucesso!', 'Configurações da Turma', $options = []);

        return redirect()->route('simoo.turmas.config');
    }
}
