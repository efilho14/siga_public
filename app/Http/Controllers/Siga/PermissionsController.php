<?php

# Classe PermissionsController
#### Controla as permissões do usuário sobre os módulos

namespace App\Http\Controllers\Siga;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Siga\SigaRoles;
use App\Model\Siga\SigaPermissions;

## Others
use Session;
use Notify;

class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('permission.index');

        ## Buscar todas as permissões
        $permission = SigaPermissions::orderBy('name', 'asc')->get();

        ## Criação do array
        $all = [];

        ## Inserir no array
        foreach($permission as $permissions):

            $all += [$permissions->roles->modules->name => []];
            $all[$permissions->roles->modules->name] += [$permissions->roles->name => []];
            
            array_push(
                $all[$permissions->roles->modules->name][$permissions->roles->name],
                [
                    'name' => $permissions->name,
                    'label' => $permissions->label
                ]
            );

        endforeach;

        ksort($all);

        return view('siga.permissions.index')->withPermission($all);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('permission.create');

        ## Buscar roles
        $roles = SigaRoles::orderBy('name', 'asc')->get();

        ## Inserir a view
        return view('siga.permissions.create')->withRole($roles);
    }

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('permission.create');

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required',
            'label' => 'required',
            'roleid' => 'required',
        ));

        ## Inserir no DB 'Simoo' na tabela 'permissions'
        $permission = new SigaPermissions();
        $permission->name = mb_strtoupper($request->name);
        $permission->label = mb_strtoupper($request->label);
        $permission->roleid = $request->roleid;
        $permission->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Permissão', $options = []);

        return redirect()->route('permissions.index');
    }

    public function edit($id)
    {
        ## Permissão
        $this->authorize('permission.edit');

        ## Buscar perfil de usuário
        $role = SigaRoles::orderBy('name', 'asc')->get();

        ## Buscar permissões
        $permission = SigaPermissions::find($id);
        return view('siga.permissions.edit')->withRole($role)->withPermission($permission);
    }

    public function update(Request $request, int $id)
    {
        ## Permissão
        $this->authorize('permission.edit');

        ## Buscar permissões a ser atualizado pelo ID
        $permission = SigaPermissions::find($id);

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required',
            'label' => 'required',
            'roleid' => 'required',
        ));

        ## Inserir as informações preenchidas no form
        $permission->name = mb_strtoupper($request->name);
        $permission->label = mb_strtoupper($request->label);
        $permission->roleid = $request->roleid;
        $permission->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Permissão', $options = []);

        return redirect()->route('permissions.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('permission.delete');

        ## Buscar a permissão a ser atualizado pelo ID
        $permission = SigaPermissions::find($id);
        $permission->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Permissão', $options = []);

        return redirect()->route('permissions.index')->with('id', $id);
    }
}
