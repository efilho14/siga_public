<?php

# Classe UsersController
#### Controla os usuários do sistema

namespace App\Http\Controllers\Siga;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Siga\SigaUsers;
use App\Model\Siga\SigaRoles;

## Others
use Session;
use Notify;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('user.index');

        ## Buscar todos os usuários
        $user = SigaUsers::orderBy('name','asc')->get();

        ## Criação do array
        $all = [];

        ## Inserir no array
        foreach($user as $users):
            if(!isset($users->roles->name)):
                $all += ['NÃO DEFINIDO' => []];

                $all['NÃO DEFINIDO'] += [$users->id => [
                    'username' => $users->username,
                    'name' => $users->name
                ]];
            else:
                $all += [$users->roles->name => []];

                $all[$users->roles->name] += [$users->id => [
                    'username' => $users->username,
                    'name' => $users->name
                ]];
            endif;
        endforeach;

        return view('siga.users.index')->withUser($all);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('user.create');

        ## Buscar perfil
        $role = SigaRoles::orderBy('name', 'asc')->get();

        ## Inserir a view
        return view('siga.users.create')->withRole($role);
    } 

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('user.create');

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'roleid' => 'required',
            'password' => 'required'
        ));

        ## Inserir no DB 'Siga' na tabela 'users'
        $user = new SigaUsers();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->roleid = $request->roleid;
        $user->password = Hash::make($request->password);
        $user->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Usuário', $options = []);

        return redirect()->route('users.index');
    }

    public function edit(int $id)
    {
        ## Permissão
        $this->authorize('user.edit');

        ## Buscar perfil
        $role = SigaRoles::orderBy('name', 'asc')->get();

        ## Buscar a permissão
        $user = SigaUsers::find($id);
        return view('siga.users.edit')->withUser($user)->withRole($role);
    }

    public function update(Request $request, int $id)
    {
        ## Permissão
        $this->authorize('user.edit');

        ## Buscar a permissão a ser atualizado pelo ID
        $user = SigaUsers::find($id);

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:11',
            'email' => 'required|string|email|max:255',
            'roleid' => 'required'
        ));

        ## Inserir as informações preenchidas no form
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->roleid = $request->roleid;
        if($request->password == ''):
            $user->password = $user->password;
        else:
            $user->password = Hash::make($request->password);
        endif;
        
        $user->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Usuário', $options = []);

        return redirect()->route('users.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('user.delete');

        ## Buscar o evento a ser atualizado pelo ID
        $user = SigaUsers::find($id);
        $user->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Usuário', $options = []);

        return redirect()->route('users.index')->with('id', $id);
    }
}