<?php

# Classe ModulesController
#### Controla os módulos do siga

namespace App\Http\Controllers\Siga;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Siga\SigaModules;

## Others
use Session;
use Notify;

class ModulesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('module.index');

        ## Buscar todos os módulos
        $module = SigaModules::orderBy('name','asc')->get();
        return view('siga.modules.index')->withModule($module);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('module.create');

        ## Inserir a view
        return view('siga.modules.create');
    } 

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('module.create');

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required'
        ));

        ## Inserir no DB 'Simoo' na tabela 'environment'
        $module = new SigaModules();
        $module->name = mb_strtoupper($request->name);
        $module->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Módulo', $options = []);

        return redirect()->route('modules.index');
    }

    public function edit(int $id)
    {
        ## Permissão
        $this->authorize('module.edit');

        ## Buscar o módulo
        $module = SigaModules::find($id);
        return view('siga.modules.edit')->withModule($module);
    }

    public function update(Request $request, int $id)
    {
        ## Permissão
        $this->authorize('module.edit');

        ## Buscar o módulo a ser atualizado pelo ID
        $module = SigaModules::find($id);

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required'
        ));

        ## Inserir as informações preenchidas no form
        $module->name = strtoupper($request->name);
        $module->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Módulo', $options = []);

        return redirect()->route('modules.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('module.delete');

        ## Buscar o evento a ser atualizado pelo ID
        $module = SigaModules::find($id);
        $module->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Módulo', $options = []);

        return redirect()->route('modules.index')->with('id', $id);
    }
}