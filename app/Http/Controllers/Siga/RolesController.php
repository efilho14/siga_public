<?php

# Classe RolesController
#### Controla os tipos de usuário do siga

namespace App\Http\Controllers\Siga;

## Controller
use App\Http\Controllers\Controller;

## Request
use Illuminate\Http\Request;

## Models
use App\Model\Siga\SigaRoles;
use App\Model\Siga\SigaModules;

## Others
use Session;
use Notify;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        ## Permissão
        $this->authorize('role.index');

        ## Buscar todos os tipos de usuário
        $role = SigaRoles::orderBy('name', 'asc')->get();
        return view('siga.roles.index')->withRole($role);
    }

    public function create()
    {
        ## Permissão
        $this->authorize('role.create');

        ## Buscar módulos
        $module = SigaModules::orderBy('name', 'asc')->get();

        ## Inserir a view
        return view('siga.roles.create')->withModule($module);
    }

    public function store(Request $request)
    {
        ## Permissão
        $this->authorize('role.create');

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required',
            'label' => 'required',
            'moduleid' => 'required',
        ));

        ## Inserir no DB 'Simoo' na tabela 'environment'
        $role = new SigaRoles();
        $role->name = mb_strtoupper($request->name);
        $role->label = mb_strtoupper($request->label);
        $role->moduleid = $request->moduleid;
        $role->save();

        ## Session e redirect
        Notify::success('Incluído com sucesso!', 'Tipos de Usuário', $options = []);

        return redirect()->route('roles.index');
    }

    public function edit($id)
    {
        ## Permissão
        $this->authorize('role.edit');

        ## Buscar módulos
        $module = SigaModules::orderBy('name', 'asc')->get();

        ## Buscar o tipo de usuário
        //$role = SigaRoles::find($id);
        $role = SigaRoles::find($id);
        return view('siga.roles.edit')->withRole($role)->withModule($module);
    }

    public function update(Request $request, int $id)
    {
        ## Permissão
        $this->authorize('role.edit');
        
        ## Buscar o tipo de usuário a ser atualizado pelo ID
        $role = SigaRoles::find($id);

        ## Validar os campos
        $this->validate($request, array(
            'name' => 'required',
            'label' => 'required',
            'moduleid' => 'required',
        ));

        ## Inserir as informações preenchidas no form
        $role->name = strtoupper($request->name);
        $role->label = strtoupper($request->label);
        $role->moduleid = strtoupper($request->moduleid);
        $role->save();

        ## Session e redirect
        Notify::success('Editado com sucesso!', 'Tipos de Usuário', $options = []);

        return redirect()->route('roles.index');
    }

    public function destroy(int $id)
    {
        ## Permissão
        $this->authorize('role.delete');

        ## Buscar o evento a ser atualizado pelo ID
        $role = SigaRoles::find($id);
        $role->delete();

        ## Session e redirect
        Notify::success('Excluído com sucesso!', 'Tipos de Usuário', $options = []);

        return redirect()->route('roles.index')->with('id', $id);
    }
}
