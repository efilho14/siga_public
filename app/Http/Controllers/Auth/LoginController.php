<?php

namespace App\Http\Controllers\Auth;

## Controller
use App\Http\Controllers\Controller;

## Auth
use Illuminate\Foundation\Auth\AuthenticatesUsers;

## Model
use App\Model\Siga\SigaUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $connection = 'siga';
    public $timestamps = true;
    protected $redirectTo = '';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $user = SigaUsers::All();
        return view('auth.login')->withUser($user);
    }

    public function username()
    {
        return 'username';
    }

    protected function redirectTo()
    {
        return route('simoo.turmas.index');
    }
}
