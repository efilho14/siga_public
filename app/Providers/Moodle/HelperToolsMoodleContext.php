<?php

namespace App\Providers\Moodle;

use Illuminate\Support\ServiceProvider;

class HelperToolsMoodleContext extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/Moodle/ToolsMoodleContext.php';
    }
}
