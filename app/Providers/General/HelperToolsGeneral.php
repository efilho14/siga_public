<?php

namespace App\Providers\General;

use Illuminate\Support\ServiceProvider;

class HelperToolsGeneral extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/General/ToolsGeneral.php';
    }
}
