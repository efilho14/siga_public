<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model\Siga\SigaPermissions' => 'App\Policies\CoursePolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /* SIMOO */

        ## Modules
        Gate::resource('moduleSystem', 'App\Policies\ModulesPolicy', [
            'simoo' => 'simoo',
            'admin' => 'admin'
        ]);

        ## Dashboard
        Gate::resource('dashboard', 'App\Policies\Simoo\DashboardPolicy', [
            'index' => 'index'
        ]);

        ## Course
        Gate::resource('course', 'App\Policies\Simoo\CoursePolicy', [
            'index' => 'index', 
            'show' => 'show', 
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete'
        ]);

        ## Course - UserEnrolled
        Gate::resource('userEnrolled', 'App\Policies\Simoo\UserEnrolledPolicy', [
            'gradeFrequency' => 'gradeFrequency',
            'insertStudent' => 'insertStudent', 'deleteStudent' => 'deleteStudent',
            'insertTeacher' => 'insertTeacher', 'editTeacher' => 'editTeacher', 'deleteTeacher' => 'deleteTeacher',
            'insertCoordinator' => 'insertCoordinator', 'editCoordinator' => 'editCoordinator', 'deleteCoordinator' => 'deleteCoordinator'
        ]);

        ## Course - Config
        Gate::resource('course.config', 'App\Policies\Simoo\CourseConfigPolicy', [
            'index' => 'index', 
            'edit' => 'edit',
        ]);

        ## Course Category
        Gate::resource('courseCategory', 'App\Policies\Simoo\CourseCategoryPolicy', [
            'index' => 'index', 
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete'
        ]);

        ## User
        Gate::resource('user', 'App\Policies\Simoo\UserPolicy', [
            'index' => 'index', 
            'show' => 'show', 
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete',
            'search' => 'search'
        ]);

        ## User Target
        Gate::resource('userTarget', 'App\Policies\Simoo\UserTargetPolicy', [
            'index' => 'index',
        ]);

        ## User Target - Public
        Gate::resource('userTargetPublic', 'App\Policies\Simoo\UserTargetPublicPolicy', [
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete'
        ]);

        ## User Target - Group
        Gate::resource('userTargetGroup', 'App\Policies\Simoo\UserTargetGroupPolicy', [
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete'
        ]);

        ## User Target - Type
        Gate::resource('userTargetType', 'App\Policies\Simoo\UserTargetTypePolicy', [
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete'
        ]);

        ## Calendar
        Gate::resource('calendar', 'App\Policies\Simoo\CalendarPolicy', [
            'index' => 'index',
        ]);

        ## Environment
        Gate::resource('environment', 'App\Policies\Simoo\EnvironmentPolicy', [
            'index' => 'index', 
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete'
        ]);

        ## Room
        Gate::resource('room', 'App\Policies\Simoo\RoomPolicy', [
            'create' => 'create', 
            'edit' => 'edit', 
            'delete' => 'delete'
        ]);

        ## Relatory
        Gate::resource('relatory', 'App\Policies\Simoo\RelatoryPolicy', [
            'index' => 'index'
        ]);


        /* ADMINISTRATOR */

        ## Permission
        Gate::resource('permission', 'App\Policies\Siga\PermissionPolicy', [
            'index' => 'index',
            'create' => 'create',
            'edit' => 'edit',
            'delete' => 'delete'
        ]);

        ## Modules
        Gate::resource('module', 'App\Policies\Siga\ModulePolicy', [
            'index' => 'index',
            'create' => 'create',
            'edit' => 'edit',
            'delete' => 'delete'
        ]);

        ## User
        Gate::resource('user', 'App\Policies\Siga\UserPolicy', [
            'index' => 'index',
            'create' => 'create',
            'edit' => 'edit',
            'delete' => 'delete'
        ]);

        ## User
        Gate::resource('role', 'App\Policies\Siga\RolePolicy', [
            'index' => 'index',
            'create' => 'create',
            'edit' => 'edit',
            'delete' => 'delete'
        ]);
    }
}
