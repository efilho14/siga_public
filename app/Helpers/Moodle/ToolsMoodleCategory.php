<?php

namespace App\Helpers\Moodle;

use App\Model\Moodle\MoodleCourseCategory;

class ToolsMoodleCategory{

    /*
        Cria uma margem sobre os sub-itens
    */
    public static function marginOrderEvents(int $num)
    {
        if($num==1):
            $result = null;
        else:
            $result = 'padding-left:'.($num * 12).'px';
        endif;

        return $result;
    }

    /*
        Ordena as categorias do curso em linha, exemplo:
        "Cursos a distância / Comunicação Eficaz"
    */
    public static function orderCategoryLine(int $idCategory=null)
    {
        $category = MoodleCourseCategory::allCategories();
        $parent = array();

        foreach($category as $categories):

            $name = null;
            $paths = array_filter(explode('/',$categories->categorypath));

            if(empty($idCategory)):

                foreach($paths as $key => $path):
                    $name .= MoodleCourseCategory::getName($path)->name;
                    $name .= ($key < count($paths)) ? ' / ' : ''; 
                endforeach;

                array_push($parent,array(
                    'id' => $categories->categoryid, 
                    'name' => $name
                ));
            
            elseif($idCategory != $categories->categoryid):               

                foreach($paths as $key => $path):
                    $name .= MoodleCourseCategory::getName($path)->name;
                    $name .= ($key < count($paths)) ? ' / ' : ''; 
                endforeach;

                array_push($parent,array(
                    'id' => $categories->categoryid, 
                    'name' => $name
                ));

            endif;

            
        endforeach;

        return $parent;
    }

    /*
        Ordena novamente o SortOrder do item acionado e dos outros que estão abaixo dele
    */
    public static function updateSortOrderCategory(int $parent, int $idCategory=null)
    {
        $allCategory = MoodleCourseCategory::allCategories();

        $category = MoodleCourseCategory::parentCategories($parent);

        if($category):
            $newSortOrder = $category->sortorder + 10000;
            MoodleCourseCategory::where('sortorder','>=',$newSortOrder)->increment('sortorder',10000);
        else:
            $filter_category = MoodleCourseCategory::getLastSortOrder()->sortorder;
            $newSortOrder = $filter_category + 10000;
        endif;
        return $newSortOrder;
    }

    public static function order(int $parent, int $idCategory)
    {
        $allCategory = MoodleCourseCategory::allCategories();
        $count = 20000;
        foreach($allCategory as $categories):
            MoodleCourseCategory::where('id','=',$categories->categoryid)->update(['sortorder' => $count]);
            $count+=10000;
        endforeach;
    }

    public static function getPathCategory(int $parent, int $id)
    {
        $category = MoodleCourseCategory::parentCategories($parent);

        if($category):
            $path = $category->categorypath.'/'.$id;
        else:
            $path = '/'.$id;
        endif;
        return $path;
    }

    public static function getDepth(int $parent)
    {
        $category = MoodleCourseCategory::parentCategories($parent);
        if($category):
            $depth = count(array_filter(explode('/',$category->categorypath)))+1;
        else:
            $depth = 1;
        endif;
        return $depth;
    }
}
