<?php

namespace App\Helpers\Moodle;

//Models
use App\Model\Moodle\MoodleCourse;

class ToolsMoodleCourse{

    public static function sortOrder(int $idCategory)
    {
        $course = MoodleCourse::where('category',$idCategory)->orderBy('sortorder', 'DESC')->limit(1)->get();
        
        if(count($course)):
            $sortOrder = $course->first()->sortorder + 1;
        else:
            $sortOrder = 50001;
        endif;
        
        return $sortOrder;
    }

    public static function updateSortOrder(int $idCategory)
    {
        
    }
}