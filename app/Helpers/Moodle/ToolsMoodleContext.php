<?php

namespace App\Helpers\Moodle;

use App\Model\Moodle\MoodleContext;
use App\Model\Moodle\MoodleCourseCategory;

class ToolsMoodleContext
{
    public static function getPathContext(int $parent,int $id)
    {
        $context = MoodleContext::where('instanceid',$parent)->where('contextlevel',40)->first();
        if($context):
            $path = $context->path.'/'.$id;
        else:
            $path = '1/'.$id;
        endif;
        return $path;
    }

    public static function getDepth(int $parent, int $contextLevel=40)
    {
        $context = MoodleContext::where('instanceid',$parent)->where('contextlevel',$contextLevel)->first();
        if($context):
            $depth = count(array_filter(explode('/',$context->path)))+1;
        else:
            $depth = 1;
        endif;
        return $depth;
    }

    /*public static function getDepth(int $parent)
    {
        $context = MoodleCourseCategory::parentCategories($parent);
        if($context):
            $depth = count(array_filter(explode('/',$context->categorypath)))+1;
        else:
            $depth = 1;
        endif;
        return $depth;
    }*/
    
}