<?php

namespace App\Helpers\Simoo;

use App\Helpers\General\ToolsGeneral;
use DateTime;

class ToolsSimooCourse
{
    public static function Period(int $courseid, array $date, array $hourInit, array $hourEnd, array $block)
    {
        $new_date = array();

        for($m=0;$m<count($date);$m++):
            //explodir as datas cadastradas
            $explode = explode('-',$date[$m]);
            
            //converter a data do Brasil para EUA
            $convert_date1 = ToolsGeneral::convertDatePtToEn(trim($explode[0]));
            $convert_date2 = ToolsGeneral::convertDatePtToEn(trim($explode[1]));

            //converter em DateTime
            $date1 = new DateTime($convert_date1);
            $date2 = new DateTime($convert_date2);
            $diference = $date1->diff($date2)->d;

            //adicionar no array
            array_push($new_date,array(
                'courseid' => $courseid, 
                'date' => $convert_date1, 
                'hourinit' => $hourInit[$m], 
                'hourend' => $hourEnd[$m], 
                'block' => $block[$m])
            );
            
            $incremet_date = $convert_date1;
            
            for($i=1;$i<$diference;$i++):
                $incremet_date = date('Y/m/d', strtotime("+1 day",strtotime($incremet_date)));
                array_push($new_date,array(
                    'courseid' => $courseid, 
                    'date' => $incremet_date, 
                    'hourinit' => $hourInit[$m], 
                    'hourend' => $hourEnd[$m], 
                    'block' => $block[$m])
                );
            endfor;
            
            if(!empty($diference)):
                array_push($new_date,array(
                    'courseid' => $courseid, 
                    'date' => $convert_date2, 
                    'hourinit' => $hourInit[$m], 
                    'hourend' => $hourEnd[$m], 
                    'block' => $block[$m])
                );
            endif;
        endfor;

        return $new_date;
    }
}