<?php

//Helper
namespace App\Helpers\General;

class ToolsGeneral
{
    public static function visible(bool $num)
    {
        if($num):
            return 'Sim';
        else:
            return 'Não';
        endif;
    }

    public static function strToTime()
    {
        $strToTime = strtotime(date('Y/m/d h:i:s'));
        return ($strToTime);
    }

    public static function strToTimeExplode($date)
    {   
        $explode = explode('-',$date[0]);
        $trim = trim($explode[0]);
        $convert = self::convertDatePtToEn($trim);
        $strToTime = self::convertDateToStrToTime($convert);
        return ($strToTime);
    }

    public static function convertDateToStrToTime($date)
    {
        $strToTime = strtotime(date("$date 03:01:00"));
        return ($strToTime);
    }

    public static function convertDatePtToEn(string $datePt)
    {
        $explode = self::multiexplode(array("/","-"), $datePt);
        return ($explode);
    }

    public static function multiexplode($delimiters, $string)
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        $result = trim($launch[2]) . "/" . trim($launch[1]) . "/" . trim($launch[0]);
        return ($result);
    }

    public static function mask($val, $mask)
    {
        #mask($cnpj,'##.###.###/####-##');
        #mask($cpf,'###.###.###-##');
        #mask($cep,'#####-###');
        #mask($data,'##/##/####');

        $maskared = '';
        $k = 0;

        for($i = 0; $i<=strlen($mask)-1; $i++):
            if($mask[$i] == '#'):
                if(isset($val[$k])):
                    $maskared .= $val[$k++];
                endif;
            else:
                if(isset($mask[$i])):
                    $maskared .= $mask[$i];
                endif;
            endif;
        endfor;
        
        return ($maskared);
    }

    public static function removeMask($val)
    {
        $mask = preg_replace("/\D+/", "", $val);
        return ($mask);
    }

    public static function monthToBrazil(string $date)
    {
        $month = [
            'January' => 'Janeiro',
            'February' => 'Fevereiro',
            'March' => 'Março',
            'April' => 'Abril',
            'May' => 'Maio',
            'June' => 'Junho',
            'July' => 'Julho',
            'August' => 'Agosto',
            'September' => 'Setembro',
            'October' => 'Outubro',
            'November' => 'Novembro',
            'December' => 'Dezembro',
        ];

        return ($month[$date]);
    }

    public static function week(string $date)
    {
        $week = [
            'Sun' => 'Dom',
            'Mon' => 'Seg',
            'Tue' => 'Ter',
            'Wed' => 'Qua',
            'Thu' => 'Qui',
            'Fri' => 'Sex',
            'Sat' => 'Sáb'
        ];

        $day = date("D", strtotime($date));

        return ($week[$day]);
    }

    public static function truncate($val, $f="0")
    {
        if(($p = strpos($val, '.')) !== false):
            $val = floatval(substr($val, 0, $p + 1 + $f));
        endif;
        return ($val);
    }

    public static function twoPoints($val)
    {
        $explode = explode(':',$val);
        print_r($explode);
        return ($explode);
    }
}