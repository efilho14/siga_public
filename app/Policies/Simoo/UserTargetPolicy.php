<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class UserTargetPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_PUBLICO_ALVO');
    }
}
