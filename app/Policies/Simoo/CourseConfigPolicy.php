<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class CourseConfigPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CONFIG_TURMAS_INDEX');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CONFIG_TURMAS_UPDATE');
    }
}
