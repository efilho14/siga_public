<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class CoursePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_TURMAS');
    }

    public function show(User $user)
    {
        return SigaPermissions::hasAccess($user, 'SHOW_TURMAS');
    }

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_TURMAS');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_TURMAS');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_TURMAS');
    }
}
