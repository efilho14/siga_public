<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class EnvironmentPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_AMBIENTES');
    }

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_AMBIENTES');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_AMBIENTES');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_AMBIENTES');
    }
}
