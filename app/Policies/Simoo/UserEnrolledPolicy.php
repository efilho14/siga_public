<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class UserEnrolledPolicy
{
    use HandlesAuthorization;

    ## Student
    public function insertStudent(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INSERT_ALUNO');
    }

    public function deleteStudent(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_ALUNO');
    }

    ## Teacher
    public function insertTeacher(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INSERT_INSTRUTOR');
    }

    public function editTeacher(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_INSTRUTOR');
    }

    public function deleteTeacher(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_INSTRUTOR');
    }

    ## Coordinator
    public function insertCoordinator(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INSERT_COORDENADOR');
    }

    public function editCoordinator(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_COORDENADOR');
    }

    public function deleteCoordinator(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_COORDENADOR');
    }

    ## Others
    public function gradeFrequency(User $user)
    {
        return SigaPermissions::hasAccess($user, 'GRADE_FREQUENCY_COURSE');
    }
}
