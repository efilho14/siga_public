<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class CourseCategoryPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_EVENTOS');
    }

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_EVENTOS');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_EVENTOS');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_EVENTOS');
    }
}
