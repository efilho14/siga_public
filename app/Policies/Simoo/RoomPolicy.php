<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class RoomPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_SALAS');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_SALAS');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_SALAS');
    }
}
