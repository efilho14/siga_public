<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class UserTargetGroupPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_PUBLICO_ALVO_GRUPO');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_PUBLICO_ALVO_GRUPO');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_PUBLICO_ALVO_GRUPO');
    }
}
