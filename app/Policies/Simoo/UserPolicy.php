<?php

namespace App\Policies\Simoo;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class UserPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_USUARIOS');
    }

    public function show(User $user)
    {
        return SigaPermissions::hasAccess($user, 'SHOW_USUARIOS');
    }

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_USUARIOS');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_USUARIOS');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_USUARIOS');
    }

    public function search(User $user)
    {
        return SigaPermissions::hasAccess($user, 'SEARCH_USUARIOS');
    }
}
