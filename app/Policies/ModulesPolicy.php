<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class ModulesPolicy
{
    use HandlesAuthorization;

    public function simoo(User $user)
    {
        return SigaPermissions::hasAccess($user, '_SIMOO');
    }

    public function admin(User $user)
    {
        return SigaPermissions::hasAccess($user, '_ADMIN');
    }
}
