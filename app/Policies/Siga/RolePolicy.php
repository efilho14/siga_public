<?php

namespace App\Policies\Siga;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class RolePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_PERFIL_USUARIO');
    }

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_PERFIL_USUARIO');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_PERFIL_USUARIO');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_PERFIL_USUARIO');
    }
}
