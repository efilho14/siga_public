<?php

namespace App\Policies\Siga;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_PERMISSAO');
    }

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_PERMISSAO');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_PERMISSAO');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_PERMISSAO');
    }
}
