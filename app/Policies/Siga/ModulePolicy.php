<?php

namespace App\Policies\Siga;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

## Models
use App\Model\Siga\SigaPermissions;

class ModulePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return SigaPermissions::hasAccess($user, 'INDEX_MODULOS_SISTEMA');
    }

    public function create(User $user)
    {
        return SigaPermissions::hasAccess($user, 'CREATE_MODULOS_SISTEMA');
    }

    public function edit(User $user)
    {
        return SigaPermissions::hasAccess($user, 'EDIT_MODULOS_SISTEMA');
    }

    public function delete(User $user)
    {
        return SigaPermissions::hasAccess($user, 'DELETE_MODULOS_SISTEMA');
    }
}
