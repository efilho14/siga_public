<?php

namespace App\Model\Moodle;

## Models
use Illuminate\Database\Eloquent\Model;

class MoodleQuestionnaireQuestion extends Model
{
    protected $table = 'mdl_questionnaire_question';
    public $timestamps = false;

    public static function getJoinSurvey(int $courseId)
    {
        return self::select('mdl_questionnaire_question.id as questionId', 'mdl_questionnaire_question.length')
                ->join('mdl_questionnaire_survey', 'mdl_questionnaire_question.survey_id' , 'mdl_questionnaire_survey.id')
                ->where('mdl_questionnaire_survey.courseid',$courseId)
                ->get();
    }

    public static function insertQuestion(int $surveyId, $teachers)
    {
        $first = self::insert([
            ['survey_id' => $surveyId, 'name' => 'Local', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => 1, 'content' => '<p>Local</p>', 'required' => 'y', 'deleted' => 'n'],
            ['survey_id' => $surveyId, 'name' => 'Horário', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => 2, 'content' => '<p>Horário</p>', 'required' => 'y', 'deleted' => 'n'],
            ['survey_id' => $surveyId, 'name' => 'Duração / Carga Horária', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => 3, 'content' => '<p>Duração / Carga Horária</p>', 'required' => 'y', 'deleted' => 'n'],
            ['survey_id' => $surveyId, 'name' => 'Aplicabilidade no Trabalho', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => 4, 'content' => '<p>Aplicabilidade no Trabalho</p>', 'required' => 'y', 'deleted' => 'n'],
            ['survey_id' => $surveyId, 'name' => 'Material Didático', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => (count($teachers)+5), 'content' => '<p>Material Didático</p>', 'required' => 'y', 'deleted' => 'n'],
            ['survey_id' => $surveyId, 'name' => 'Coordenação', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => (count($teachers) + 6), 'content' => '<p>Coordenação</p>', 'required' => 'y', 'deleted' => 'n'],
            ['survey_id' => $surveyId, 'name' => 'Avaliação Geral do Evento', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => (count($teachers) + 7), 'content' => '<p>Avaliação Geral do Evento</p>', 'required' => 'y', 'deleted' => 'n'],
            ['survey_id' => $surveyId, 'name' => 'Observações / Sugestões', 'type_id' => 3, 'result_id' => null, 'length' => 5, 'precise' => 1, 'position' => (count($teachers) + 8), 'content' => '<p>Observações / Sugestões</p>', 'required' => 'y', 'deleted' => 'n']
        ]);

        foreach ($teachers as $key => $teacher):
            $name = explode(' ',$teacher['firstname']);
            self::insert(['survey_id' => $surveyId, 'name' => 'Instrutor ('.$name[0].' '.$name[1].')', 'type_id' => 4, 'result_id' => null, 'length' => 0, 'precise' => 0, 'position' => (4+($key+1)), 'content' => '<p>Instrutor ('. $name[0].' '.$name[1].')</p>', 'required' => 'y', 'deleted' => 'n']);
        endforeach;
    }
}
