<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

//helpers
use App\Helpers\General\ToolsGeneral;

//moodle
use App\Model\Moodle\MoodleGradeCategories;

class MoodleGradeItems extends Model
{
    protected $table = 'mdl_grade_items';
    public $timestamps = false;

    public static function insertItems(int $courseId, int $itemInstance, string $itemName, float $gradeMax)
    {
        $categories = MoodleGradeCategories::where('courseid',$courseId)
                        ->orderBy('id','desc')->first();

        $searchGradeItems = self::where('courseid', $courseId)->where('categoryid', NULL)->first();

        if(empty($searchGradeItems)):
            self::insert([
                'courseid' => $courseId, 
                'categoryid' => NULL, 
                'itemname' => NULL, 
                'itemtype' => 'course', 
                'itemmodule' => NULL, 
                'iteminstance' => $categories->id, 
                'itemnumber' => NULL, 
                'iteminfo' => NULL, 
                'idnumber' => '', 
                'calculation' => NULL, 
                'gradetype' => 1, 
                'grademax' => 110.00000, 
                'grademin' => 0.00000, 
                'scaleid' => NULL, 
                'outcomeid' => NULL, 
                'gradepass' => 0.00000, 
                'multfactor' => 1.00000, 
                'plusfactor' => 0.00000, 
                'aggregationcoef' => 0.00000, 
                'aggregationcoef2' => 0.00000, 
                'sortorder' => 1, 
                'display' => 0, 
                'decimals' => NULL, 
                'hidden' => 0, 
                'locked' => 0, 
                'locktime' => 0, 
                'needsupdate' => 0, 
                'weightoverride' => 0, 
                'timecreated' => ToolsGeneral::strToTime(), 
                'timemodified' => ToolsGeneral::strToTime(), 
            ]);
        endif;
        
        $gradeItems = self::where('courseid', $courseId)->orderBy('sortorder','desc')->first();

        $items = self::insert([
            'courseid' => $courseId, 
            'categoryid' => $categories->id, 
            'itemname' => $itemName, 
            'itemtype' => 'mod', 
            'itemmodule' => 'quiz', 
            'iteminstance' => $itemInstance, 
            'itemnumber' => 0, 
            'iteminfo' => NULL, 
            'idnumber' => '', 
            'calculation' => NULL, 
            'gradetype' => 1, 
            'grademax' => $gradeMax, 
            'grademin' => 0.00000, 
            'scaleid' => NULL, 
            'outcomeid' => NULL, 
            'gradepass' => 0.00000, 
            'multfactor' => 1.00000, 
            'plusfactor' => 0.00000, 
            'aggregationcoef' => 0.00000, 
            'aggregationcoef2' => 1.00000, 
            'sortorder' => ($gradeItems->sortorder+1), 
            'display' => 0, 
            'decimals' => NULL, 
            'hidden' => 0, 
            'locked' => 0, 
            'locktime' => 0, 
            'needsupdate' => 0, 
            'weightoverride' => 0, 
            'timecreated' => ToolsGeneral::strToTime(), 
            'timemodified' => ToolsGeneral::strToTime(), 
        ]);
    }
}