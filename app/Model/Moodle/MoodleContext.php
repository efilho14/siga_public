<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MoodleContext extends Model
{
    protected $table = 'mdl_context';
    public $timestamps = false;

    /*
        Contextlevel - 30 - Para usuarios
        Contextlevel - 40 - Para categorias
        Contextlevel - 50 - Para turmas
        Contextlevel - 70 - Para quiz
    */

    public static function getInstanceId(int $instanceId, int $contextLevel = 40)
    {
        return $context = self::where('instanceid',$instanceId)->where('contextlevel',$contextLevel)->first();
    }

    public static function insertContext(int $instanceId, int $contextLevel, string $path, int $depth)
    {
        return $context = self::insertGetId([
            'contextlevel' => $contextLevel, 
            'instanceid' => $instanceId, 
            'path' => $path, 
            'depth' => $depth
        ]);
    }

    public static function updateContext(int $id, string $path, int $depth)
    {
        self::where('id',$id)->update([
            'path' => $path,
            'depth' => $depth
        ]);
    }
}
