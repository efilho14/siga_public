<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

class MoodleQuiz extends Model
{
    protected $table = 'mdl_quiz';
    public $timestamps = false;

    public static function insertQuiz(int $courseId, string $name, float $grade, string $date)
    {       
        return self::insertGetId([
            'course' => $courseId, 
            'name' => $name, 
            'intro' => '', 
            'introformat' => 1, 
            'timeopen' => 0, 
            'timeclose' => 0, 
            'timelimit' => 0, 
            'overduehandling' => 'autosubmit', 
            'graceperiod' => 0, 
            'preferredbehaviour' => 'deferredfeedback', 
            'canredoquestions' => 0, 
            'attempts' => 0, 
            'attemptonlast' => 0, 
            'grademethod' => 1, 
            'decimalpoints' => 1, 
            'questiondecimalpoints' => -1, 
            'reviewattempt' => 69632, 
            'reviewcorrectness' => 4096, 
            'reviewmarks' => 4352, 
            'reviewspecificfeedback' => 0, 
            'reviewgeneralfeedback' => 0, 
            'reviewrightanswer' => 0, 
            'reviewoverallfeedback' => 0, 
            'questionsperpage' => 0, 
            'navmethod' => 'free', 
            'shuffleanswers' => 0, 
            'sumgrades' => 0.00000, 
            'grade' => $grade, 
            'timecreated' => 0, 
            'timemodified' => $date, 
            'password' => '', 
            'subnet' => '', 
            'browsersecurity' => '-', 
            'delay1' => 0, 
            'delay2' => 0, 
            'showuserpicture' => 0, 
            'showblocks' => 0, 
            'completionattemptsexhausted' => 0, 
            'completionpass' => 0, 
            'allowofflineattempts' => 0, 
        ]);
    }
}
