<?php

namespace App\Model\Moodle;

## Models
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

## Libraries
use App\Libraries\Simoo\SimooLibInit;

class MoodleCourseCategory extends Model
{
    protected $table = 'mdl_course_categories';
    public $timestamps = false;

    public static function allCategories()
    {
        ## Chamar o INIT do presencial
        SimooLibInit::presential();

        ## Pega o id da categoria 'presencial'
        $presential = "/".session('presential');

        ## Seleciona as categorias no 'Moodle'
        $category = DB::select(
            "SELECT category.id as categoryid, category.name, category.description, category.parent, category.sortorder, 
                category.visible, category.depth, category.path as categorypath, context.id as contextid, context.instanceid,
                context.path as contextpath
            FROM mdl_course_categories as category 
            INNER JOIN mdl_context as context
                ON category.id = context.instanceid 
            WHERE category.id <> 1 AND context.contextlevel = 40
                AND category.path LIKE '$presential%'
            ORDER BY category.sortorder ASC"
            );
        return $category;
    }

    public static function parentCategories(int $id)
    {
        $category = collect(\DB::select(
            "SELECT category.id as categoryid, category.name, category.description, category.parent, category.sortorder, 
                category.visible, category.depth, category.path as categorypath, context.id as contextid, context.instanceid,
                context.path as contextpath
            FROM mdl_course_categories as category 
            INNER JOIN mdl_context as context
                ON category.id = context.instanceid 
            WHERE category.id <> 1 AND context.contextlevel = 40 AND category.id = $id
            ORDER BY category.sortorder ASC"
            ))->first();
        return ($category);
    }

    public static function GetIdNumber(string $value)
    {
        return $category = self::select('*')->where('idnumber', $value)->first();
    }

    public static function getLastId()
    {
        return $lastId = self::select('id')->orderBy('id','desc')->first();
    }

    public static function getLastSortOrder()
    {
        return $lastOrder = self::select('id','sortorder')->orderBy('sortorder','desc')->first();
    }

    public static function getName($id)
    {
        return $name = self::find($id);
    }
    
}
