<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleUser extends Model
{
    protected $table = 'mdl_user';
    public $timestamps = false;
}
