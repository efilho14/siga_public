<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleUserInfoData extends Model
{
    protected $table = 'mdl_user_info_data';
    public $timestamps = false;
}
