<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

//helpers
use App\Helpers\General\ToolsGeneral;

class MoodleGradeCategories extends Model
{
    protected $table = 'mdl_grade_categories';
    public $timestamps = false;

    public static function insertCategories(int $courseId)
    {
        $categories = self::orderBy('id','desc')->first();

        $gradeCategories = self::insertGetId([
            'courseid' => $courseId, 
            'parent' => NULL, 
            'depth' => 1, 
            'path' => '',
            'fullname' => '',
            'aggregation' => 13,
            'keephigh' => 0,
            'droplow' => 0,
            'aggregateonlygraded' => 1,
            'aggregateoutcomes' => 0,
            'timecreated' => ToolsGeneral::strToTime(),
            'timemodified' => ToolsGeneral::strToTime(),
            'hidden' => 0,
        ]);

        self::where('courseid', $courseId)
        ->where('id',$gradeCategories)
        ->update(['path' => ('/'.$gradeCategories.'/')]);
    }
}