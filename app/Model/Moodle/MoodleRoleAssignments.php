<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleRoleAssignments extends Model
{
    protected $table = 'mdl_role_assignments';
    public $timestamps = false;
}
