<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

class MoodleQuizFeedback extends Model
{
    protected $table = 'mdl_quiz_feedback';
    public $timestamps = false;

    public static function insertFeedback(int $quizId)
    {   
         $quiz = self::insert([
            'quizid' => $quizId, 
            'feedbacktext' => '', 
            'feedbacktextformat' => 1, 
            'mingrade' => 0.00000, 
            'maxgrade' => 11.00000, 
        ]);
    }
}