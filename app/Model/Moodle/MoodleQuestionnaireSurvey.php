<?php

namespace App\Model\Moodle;

## Models
use Illuminate\Database\Eloquent\Model;

class MoodleQuestionnaireSurvey extends Model
{
    protected $table = 'mdl_questionnaire_survey';
    public $timestamps = false;

    public static function insertSurvey(int $courseId, string $name)
    {   
        return self::insertGetId([
            'name' => $name, 
            'courseid' => $courseId, 
            'realm' => 'private', 
            'status' => 0, 
            'title' => $name, 
            'email' => '', 
            'subtitle' => '', 
            'info' => '', 
            'theme' => '', 
            'thanks_page' => '', 
            'thank_head' => '', 
            'thank_body' => '', 
            'feedbacksections' => 0, 
            'feedbacknotes' => '', 
            'feedbackscores' => 0, 
            'chart_type' => NULL
        ]);
    }
}