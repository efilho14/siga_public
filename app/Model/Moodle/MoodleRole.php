<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

class MoodleRole extends Model
{
    protected $table = 'mdl_role';
    public $timestamps = false;
}
