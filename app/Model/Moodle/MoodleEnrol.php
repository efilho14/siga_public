<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

//Helper
use App\Helpers\General\ToolsGeneral;

class MoodleEnrol extends Model
{
    protected $table = 'mdl_enrol';
    public $timestamps = false;

    public static function insertEnrol(int $courseId, int $roleId)
    {
        $enrol = self::insert([
            ['enrol' => 'manual', 'status' => 0, 'courseid' => $courseId, 'sortorder' => 0, 'name' => NULL, 'enrolperiod' => 0, 'enrolstartdate' => 0, 'expirynotify' => 0, 'expirythreshold' => 86400, 'notifyall' => 0, 'password' => NULL, 'cost' => NULL, 'currency' => NULL, 'roleid' => $roleId, 'customint1' => NULL, 'customint2' => NULL, 'customint3' => NULL, 'customint4' => NULL, 'customint5' => NULL, 'customint6' => NULL, 'customint7' => NULL, 'customint8' => NULL, 'customchar1' => NULL, 'customchar2' => NULL, 'customchar3' => NULL, 'customdec1' => NULL, 'customdec2' => NULL, 'customtext1' => NULL, 'customtext2' => NULL, 'customtext3' => NULL, 'customtext4' => NULL, 'timecreated' => ToolsGeneral::strToTime(), 'timemodified' => ToolsGeneral::strToTime()],
            ['enrol' => 'self', 'status' => 0, 'courseid' => $courseId, 'sortorder' => 1, 'name' => NULL, 'enrolperiod' => 0, 'enrolstartdate' => 0, 'expirynotify' => 0, 'expirythreshold' => 86400, 'notifyall' => 0, 'password' => NULL, 'cost' => NULL, 'currency' => NULL, 'roleid' => $roleId, 'customint1' => 0, 'customint2' => 0, 'customint3' => 0, 'customint4' => 1, 'customint5' => 0, 'customint6' => 1, 'customint7' => NULL, 'customint8' => NULL, 'customchar1' => NULL, 'customchar2' => NULL, 'customchar3' => NULL, 'customdec1' => NULL, 'customdec2' => NULL, 'customtext1' => NULL, 'customtext2' => NULL, 'customtext3' => NULL, 'customtext4' => NULL, 'timecreated' => ToolsGeneral::strToTime(), 'timemodified' => ToolsGeneral::strToTime()],
        ]);
    }
}
