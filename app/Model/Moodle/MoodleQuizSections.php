<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

class MoodleQuizSections extends Model
{
    protected $table = 'mdl_quiz_sections';
    public $timestamps = false;

    public static function insertSections(int $quizId)
    {   
         $quiz = self::insert([
            'quizid' => $quizId, 
            'firstslot' => 1, 
            'heading' => '', 
            'shufflequestions' => 0
        ]);
    }
}