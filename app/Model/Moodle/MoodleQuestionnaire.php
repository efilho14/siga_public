<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

class MoodleQuestionnaire extends Model
{
    protected $table = 'mdl_questionnaire';
    public $timestamps = false;

    public static function insertQuestionnaire(int $courseId, string $name, int $surveyId, string $date)
    {   
        return self::insertGetId([
            'course' => $courseId, 
            'name' => $name, 
            'intro' => '', 
            'introformat' => 1, 
            'qtype' => 1, 
            'respondenttype' => 'fullname', 
            'resp_eligible' => 'all', 
            'resp_view' => 1, 
            'notifications' => 0, 
            'opendate' => 0, 
            'closedate' => 0, 
            'resume' => 0, 
            'navigate' => 0, 
            'grade' => 0, 
            'sid' => $surveyId, 
            'timemodified' => $date, 
            'completionsubmit' => 0, 
            'autonum' => 3
        ]);
    }
}