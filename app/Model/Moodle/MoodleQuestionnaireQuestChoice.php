<?php

namespace App\Model\Moodle;

## Models
use Illuminate\Database\Eloquent\Model;
use App\Model\Moodle\MoodleQuestionnaireQuestion;

class MoodleQuestionnaireQuestChoice extends Model
{
    protected $table = 'mdl_questionnaire_quest_choice';
    public $timestamps = false;

    public static function insertQuestChoice(int $courseId)
    {   
        $questions = MoodleQuestionnaireQuestion::getJoinSurvey($courseId);

        foreach($questions as $question):
            if($question->length != '5'):
                self::insert([
                    ['question_id' => $question->questionId, 'content' => 'Excelente', 'value' => NULL],
                    ['question_id' => $question->questionId, 'content' => 'Bom', 'value' => NULL],
                    ['question_id' => $question->questionId, 'content' => 'Regular', 'value' => NULL],
                    ['question_id' => $question->questionId, 'content' => 'Fraco', 'value' => NULL]
                ]);
            endif;
        endforeach;
    }
}