<?php

namespace App\Model\Moodle;

//model
use Illuminate\Database\Eloquent\Model;

class MoodleGradeGrades extends Model
{
    protected $table = 'mdl_grade_grades';
    public $timestamps = false;

    public static function insertGrades(int $userId, array $itemId, int $userModified, int $grade, string $data)
    {
        self::insert([
            [
                'itemid' => $itemId[0], 
                'userid' => $userId, 
                'rawgrade' => NULL, 
                'rawgrademax' => 10.00000, 
                'rawgrademin' => 0.00000, 
                'rawscaleid' => NULL, 
                'usermodified' => $userModified, 
                'finalgrade' => $grade, 
                'hidden' => 0, 
                'locked' => 0, 
                'locktime' => 0, 
                'exported' => 0, 
                'overridden' => $data, 
                'excluded' => 0, 
                'feedback' => NULL, 
                'feedbackformat' => 0, 
                'information' => NULL, 
                'informationformat' => 0, 
                'timecreated' => NULL, 
                'timemodified' => $data, 
                'aggregationstatus' => 'used', 
                'aggregationweight' => 1.00000
            ],
            
            [
                'itemid' => $itemId[1], 
                'userid' => $userId, 
                'rawgrade' => NULL, 
                'rawgrademax' => 10.00000, 
                'rawgrademin' => 0.00000, 
                'rawscaleid' => NULL, 
                'usermodified' => NULL, 
                'finalgrade' => $grade, 
                'hidden' => 0, 
                'locked' => 0, 
                'locktime' => 0, 
                'exported' => 0, 
                'overridden' => 0, 
                'excluded' => 0, 
                'feedback' => NULL, 
                'feedbackformat' => 0, 
                'information' => NULL, 
                'informationformat' => 0, 
                'timecreated' => NULL, 
                'timemodified' => $data, 
                'aggregationstatus' => 'unknown', 
                'aggregationweight' => NULL
            ]               
        ]);
    }
}