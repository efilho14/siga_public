<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

//helper
use App\Helpers\General\ToolsGeneral;

//models
use App\Model\Moodle\MoodleModules;
use App\Model\Moodle\MoodleCourseSections;

class MoodleCourseModules extends Model
{
    protected $table = 'mdl_course_modules';
    public $timestamps = false;

    public static function insertCourseModules(int $courseId, int $quizId, string $moduleName, int $restriction=0, string $availability=NULL, int $visible=1)
    {
        ## 'mdl_modules'
        $module = MoodleModules::where('name',$moduleName)->first();

        ## 'mdl_course_sections'
        $section = MoodleCourseSections::where('course',$courseId)->first();

        return $courseModules = self::insertGetId([
            'course' => $courseId, 
            'module' => $module->id, 
            'instance' => $quizId, 
            'section' => $section->id,
            'idnumber' => '',
            'added' => ToolsGeneral::strToTime(),
            'score' => 0,
            'indent' => 0,
            'visible' => $visible,
            'visibleoncoursepage' => 1,
            'visibleold' => 1,
            'groupmode' => 0,
            'groupingid' => 0,
            'completion' => $restriction,
            'completiongradeitemnumber' => NULL,
            'completionview' => 0,
            'completionexpected' => 0,
            'showdescription' => 0,
            'availability' => $availability,
            'deletioninprogress' => 0
        ]);
    }
}
