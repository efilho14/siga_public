<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleCourse extends Model
{
    protected $table = 'mdl_course';
    public $timestamps = false;
}
