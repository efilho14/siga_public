<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleUserInfoField extends Model
{
    protected $table = 'mdl_user_info_field';
    public $timestamps = false;
}
