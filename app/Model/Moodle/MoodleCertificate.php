<?php

namespace App\Model\Moodle;

//Model
use Illuminate\Database\Eloquent\Model;

class MoodleCertificate extends Model
{
    protected $table = 'mdl_certificate';
    public $timestamps = false;

    public static function insertCertificate(int $courseId, string $name, string $date, int $workload)
    {   
        return self::insertGetId([
            'course' => $courseId, 
            'name' => $name, 
            'intro' => '', 
            'introformat' => 1, 
            'emailteachers' => 0, 
            'emailothers' => '', 
            'savecert' => 0, 
            'reportcert' => 0, 
            'delivery' => 0, 
            'requiredtime' => 0, 
            'certificatetype' => 'A4_embedded', 
            'orientation' => 'L', 
            'borderstyle' => 'esafaz.jpg', 
            'bordercolor' => 0, 
            'printwmark' => 0, 
            'printdate' => 1, 
            'datefmt' => 3, 
            'printnumber' => 1,
            'printgrade' => 0,
            'gradefmt' => 2,
            'printoutcome' => 0,
            'printhours' => $workload,
            'printteacher' => 0,
            'customtext' => '',
            'printsignature' => 0,
            'printseal' => 0,
            'timecreated' => $date,
            'timemodified' => $date,
        ]);
    }
}