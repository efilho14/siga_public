<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleUserEnrolments extends Model
{
    protected $table = 'mdl_user_enrolments';
    public $timestamps = false;
}
