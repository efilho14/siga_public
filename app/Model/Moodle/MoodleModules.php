<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleModules extends Model
{
    protected $table = 'mdl_modules';
    public $timestamps = false;
}
