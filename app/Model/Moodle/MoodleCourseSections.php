<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

//helper
use App\Helpers\General\ToolsGeneral;

class MoodleCourseSections extends Model
{
    protected $table = 'mdl_course_sections';
    public $timestamps = false;

    public static function insertCourseSections(int $idCourse, int $section = 0, int $courseModule = NULL)
    {
        self::insert([
            'course' => $idCourse, 
            'section' => $section, 
            'name' => NULL, 
            'summary' => '',
            'summaryformat' => 1,
            'sequence' => $courseModule,
            'visible' => 1,
            'availability' => NULL,
            'timemodified' => ToolsGeneral::strToTime(),
        ]);
    }

    public static function updateCourseSections(int $idCourse, int $section = 0, int $courseModule = NULL)
    {
        $courseSections = self::where('course', $idCourse)->first();

        self::where('course',$idCourse)
            ->update([
            'section' => $section,
            'sequence' => $courseSections->sequence . ',' . $courseModule
        ]);
    }
}
