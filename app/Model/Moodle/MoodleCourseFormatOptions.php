<?php

namespace App\Model\Moodle;

use Illuminate\Database\Eloquent\Model;

class MoodleCourseFormatOptions extends Model
{
    protected $table = 'mdl_course_format_options';
    public $timestamps = false;

    public static function insertFormatOptions(int $idCourse)
    {
        $options = self::insert([
            ['courseid' => $idCourse, 'format' => 'topics', 'sectionid' => 0, 'name' => 'hiddensections', 'value' => 0],
            ['courseid' => $idCourse, 'format' => 'topics', 'sectionid' => 0, 'name' => 'coursedisplay', 'value' => 0]
        ]);
    }
}



