<?php

namespace App\Model\Siga;

//Model
use Illuminate\Database\Eloquent\Model;

class SigaPermissions extends Model
{
    protected $connection = 'siga';
    protected $table = 'permissions';
    public $timestamps = true;

    public function roles()
    {
        return $this->belongsTo('App\Model\Siga\SigaRoles', 'roleid');
    }

    public static function hasAccess($user, $name)
    {
        $permission = SigaPermissions::join('roles', 'permissions.roleid','=','roles.id')
                        ->join('users', 'roles.id','=','users.roleid')
                        ->where('users.id', $user->id)
                        ->where('permissions.name',$name)->first();

        return ($permission) ? true : false;
    }
}