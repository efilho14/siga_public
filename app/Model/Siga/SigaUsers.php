<?php

namespace App\Model\Siga;

//Model
use Illuminate\Database\Eloquent\Model;

class SigaUsers extends Model
{
    protected $connection = 'siga';
    protected $table = 'users';
    public $timestamps = true;

    public function roles()
    {
        return $this->belongsTo('App\Model\Siga\SigaRoles', 'roleid');
    }
}