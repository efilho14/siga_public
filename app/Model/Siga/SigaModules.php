<?php

namespace App\Model\Siga;

//Model
use Illuminate\Database\Eloquent\Model;

class SigaModules extends Model
{
    protected $connection = 'siga';
    protected $table = 'modules';
    public $timestamps = true;
}