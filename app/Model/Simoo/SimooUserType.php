<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;

class SimooUserType extends Model
{
    protected $connection = 'simoo';
    protected $table = 'user_type';
}
