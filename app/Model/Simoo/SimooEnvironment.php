<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SimooEnvironment extends Model
{
    protected $connection = 'simoo';
    protected $table = 'environment';

    public static function getAll()
    {
        return DB::connection('simoo')
                    ->table('environment')
                    ->select('id','name')
                    ->orderBy('name','asc')
                    ->get();
    }

}
