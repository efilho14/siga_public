<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;

class SimooCourseConfig extends Model
{
    protected $connection = 'simoo';
    protected $table = 'course_config';

    public static function getConfig()
    {
        return $config = self::first();
    }
}
