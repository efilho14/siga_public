<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;

class SimooCourseDates extends Model
{
    protected $connection = 'simoo';
    protected $table = 'course_dates';
    public $timestamps = false;

}
