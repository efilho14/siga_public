<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;

class SimooCourse extends Model
{
    protected $connection = 'simoo';
    protected $table = 'course';
    public $timestamps = true;
}
