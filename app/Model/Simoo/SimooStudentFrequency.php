<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;

class SimooStudentFrequency extends Model
{
    protected $connection = 'simoo';
    protected $table = 'student_frequency';
}
