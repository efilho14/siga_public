<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;

class SimooUserGroup extends Model
{
    protected $connection = 'simoo';
    protected $table = 'user_group';
}
