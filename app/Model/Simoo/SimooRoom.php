<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SimooRoom extends Model
{
    protected $connection = 'simoo';
    protected $table = 'room';

    public static function getAll()
    {
        return DB::connection('simoo')
                    ->table('room')
                    ->select('id', 'environmentid','name')
                    ->orderBy('environmentid','asc')
                    ->orderBy('name','asc')
                    ->get();
    }
}
