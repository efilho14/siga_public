<?php

namespace App\Model\Simoo;

use Illuminate\Database\Eloquent\Model;

class SimooUserPublic extends Model
{
    protected $connection = 'simoo';
    protected $table = 'user_public';
}
